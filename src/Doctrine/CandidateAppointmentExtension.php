<?php


namespace App\Doctrine;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Extension\QueryCollectionExtensionInterface;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Extension\QueryItemExtensionInterface;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Util\QueryNameGeneratorInterface;
use App\Entity\Candidate;
use App\Entity\CandidateAppointment;
use App\Entity\Requisition;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\Security\Core\Security;


class CandidateAppointmentExtension implements QueryCollectionExtensionInterface, QueryItemExtensionInterface
{
    private $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    public function applyToCollection(QueryBuilder $queryBuilder, QueryNameGeneratorInterface $queryNameGenerator, string $resourceClass, string $operationName = null)
    {
        $this->addWhere($queryBuilder, $resourceClass);
    }

    public function applyToItem(QueryBuilder $queryBuilder, QueryNameGeneratorInterface $queryNameGenerator, string $resourceClass, array $identifiers, string $operationName = null, array $context = [])
    {
        $this->addWhere($queryBuilder, $resourceClass);
    }

    private function addWhere(QueryBuilder $queryBuilder, string $resourceClass): void
    {
        if (CandidateAppointment::class !== $resourceClass || null === $user = $this->security->getUser()) {
            return;
        }

        /*
        $rootAlias = $queryBuilder->getRootAliases()[0];
        $queryBuilder->leftJoin(sprintf('%s.candidate',$rootAlias),'ca');
        $queryBuilder->leftJoin(sprintf('%s.operators','ca'),'op');
        $queryBuilder->andWhere(sprintf('%s.id IN (:current_user)', 'op'));
        $queryBuilder->setParameter('current_user', array($user));
        */
    }
}