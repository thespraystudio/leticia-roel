<?php


namespace App\Utils;


class ImagesList
{
    public static $images = [
        "adam-vradenburg-TK5I5L5JGxY-unsplash.jpg",
        "claudio-schwarz-purzlbaum-pN684G33h_M-unsplash.jpg",
        "erik-eastman-4HG5hlhmZg8-unsplash.jpg",
        "jay-wennington-Y_aNcALhqZw-unsplash.jpg",
        "jezael-melgoza-7H77FWkK_x4-unsplash.jpg",
        "jezael-melgoza-layMbSJ3YOE-unsplash.jpg",
        "julius-drost-whiqaCAYytA-unsplash.jpg",
        "nathan-dumlao-ewGMqs2tmJI-unsplash.jpg",
        "polaroidville--NV9zA0NqRM-unsplash.jpg",
        "soroush-karimi-crjPrExvShc-unsplash.jpg",
        "stefan-widua-iPOZf3tQfHA-unsplash.jpg",
        "vasily-koloda-8CqDvPuo_kI-unsplash.jpg",
        "yoav-aziz-tKCd-IWc4gI-unsplash.jpg",
    ];

    public static $flags = [
        "001-ethiopia.png",
        "002-oman.png",
        "003-tanzania.png",
        "004-slovenia.png",
        "005-puerto-rico.png",
        "006-mozambique.png",
        "007-iraq.png",
        "008-lebanon.png",
        "009-uganda.png",
        "010-nigeria.png",
        "011-italy.png",
        "012-malta.png",
        "013-tunisia.png",
        "014-nicaragua.png",
        "015-el-salvador.png",
        "016-zambia.png",
        "017-wales.png"
    ];
}