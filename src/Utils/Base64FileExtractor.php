<?php


namespace App\Utils;


class Base64FileExtractor
{


    public function extractBase64String(string $base64Content)
    {

        $data = explode(';', $base64Content);
        return $data[1];

    }

    public function extractImageName(string $base64Content)
    {

        $data = explode(';', $base64Content);
        return $data[0];

    }

    public function isValidToExtract(string $base64Content):bool
    {
        $toExplode = explode(';', $base64Content);
        return count($toExplode) > 0;
    }
}