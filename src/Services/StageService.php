<?php

namespace App\Services;

use App\Repository\StageRepository;
use App\Entity\School;
use App\Entity\Candidate;

class StageService
{
    private $stageRepository;

    public function __construct(StageRepository $stageRepository)
    {
        $this->stageRepository = $stageRepository;
    }


    public function calculateApprovedStagesPercentage($school, $candidate): array
    {
        $allStages = $this->stageRepository->findBy(['school' => $school, 'candidate' => $candidate]);

        $totalStagesCount = count($allStages);
        $approvedStagesCount = 0;

        foreach ($allStages as $stage) {
            if ($stage->getApproved() === true) {
                $approvedStagesCount++;
            }
        }

        if ($totalStagesCount === 0) {
            return [
                'approvedStagesCount' => 0,
                'approvedStagesPercentage' => 0.0
            ];
        }

        $approvedStagesPercentage = ($approvedStagesCount / $totalStagesCount) * 100;

        return [
            'approvedStagesCount' => $approvedStagesCount,
            'approvedStagesPercentage' => $approvedStagesPercentage,
            'stagesCount'=>$totalStagesCount
        ];
    }
}
