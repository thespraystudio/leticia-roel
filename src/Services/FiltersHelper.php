<?php


namespace App\Services;

use App\Entity\Activity;
use App\Entity\Country;
use App\Entity\Currency;
use App\Entity\Level;
use App\Entity\Sport;
use App\Interfaces\SSFilterInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class FiltersHelper
{
    /**
     * @var SessionInterface
     */
    private $session;

    public $type;
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(RequestStack $requestStack, EntityManagerInterface $entityManager)
    {
        $this->session = $requestStack->getCurrentRequest()->getSession();
        $this->entityManager = $entityManager;
    }


    public function setParameters($data, $type)
    {
        if ($data && $data != null) {
            $this->session->set($type, $data);
        } else {
            $this->session->set($type, null);
        }
    }

    public function getParameters($type)
    {
        $object = $this->session->get($type);
        return $this->completeParams($object);
    }

    protected function completeParams($object)
    {
        if (!$object) {
            return null;
        }


        if (method_exists($object, 'getActivities') && $object->getActivities()) {
            $activities = new ArrayCollection();
            foreach ($object->getActivities() as $activity) {
                $activities->add($this->entityManager->getRepository(Activity::class)->find($activity->getId()));
            }
            $object->setActivities($activities);
        }

        if (method_exists($object, 'getLevels') && $object->getLevels()) {
            $levels = new ArrayCollection();
            foreach ($object->getLevels() as $level) {
                $levels->add($this->entityManager->getRepository(Level::class)->find($level->getId()));
            }
            $object->setLevels($levels);
        }

        if (method_exists($object, 'getSports') && $object->getSports()) {
            $sports = new ArrayCollection();
            foreach ($object->getSports() as $sport) {
                $sports->add($this->entityManager->getRepository(Sport::class)->find($sport->getId()));
            }
            $object->setSports($sports);
        }


        if (method_exists($object, 'getCountry') && $object->getCountry()) {
            $countries = new ArrayCollection();
            foreach ($object->getCountry() as $country) {
                $countries->add($this->entityManager->getRepository(Country::class)->find($country->getId()));
            }
            $object->setCountry($countries);
        }

        return $object;
    }
}
