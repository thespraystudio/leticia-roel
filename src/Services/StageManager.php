<?php


namespace App\Services;


use App\Entity\Candidate;
use App\Entity\Process;
use App\Entity\ProcessResult;
use App\Entity\Stage;
use App\Entity\CandidateStage;
use App\Entity\Document;
use App\Entity\ProcessField;
use App\Repository\StageRepository;
use App\Repository\CandidateStageRepository;
use Doctrine\ORM\EntityManagerInterface;

class StageManager
{

    private EntityManagerInterface $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function createInitialStage(Candidate $candidate, Stage $templateStage)
    {
        
        $stage = clone $templateStage;
        $stage->setTemplate(false);
        $stage->setCandidate($candidate);
        $stage->setSchool($templateStage->getSchool());
        $this->em->persist($stage);
        foreach ($stage->getProcesses() as $process) {
            $processClon = clone $process;
            $processClon->setCandidate($candidate);
            $stage->addProcess($processClon);
            foreach ($processClon->getChildren() as $children) {
                $childrenClon = clone $children;
                $processClon->addChild($childrenClon);
                foreach ($childrenClon->getDocuments() as $document) {
                    $documentClon = clone $document;
                    $childrenClon->addDocument($documentClon);
                    $this->em->persist($documentClon);
                }
                foreach ($childrenClon->getFields() as $field) {
                    $fieldClon = clone $field;
                    $childrenClon->addField($fieldClon);
                    $this->em->persist($fieldClon);
                }
            }
        }

        $this->em->flush();
    }


    public function updateStages(Candidate $candidate) 
    {
        $schools = $candidate->getSchool();
        if ($schools->isEmpty()) {
            return;
        }
        $stages = $this->em->getRepository(Stage::class)->findStagesByCandidateSchools($schools);
        foreach ($stages as $templateStage) {
            $candidateId = $candidate->getId();
            $stageId = $templateStage->getId();
            $schoolId = $templateStage->getSchool()->getId();
            if (!$this->em->getRepository(CandidateStage::class)->findOneBy([
                'candidate' => $candidateId,
                'stage' => $stageId,
                'school' => $schoolId,
            ])) {
                $candidateStage = new CandidateStage();
                $candidateStage->setCandidate($candidate);
                $candidateStage->setStage($templateStage);
                $candidateStage->setSchool($templateStage->getSchool());
                $this->em->persist($candidateStage);
                $this->createInitialStage($candidate, $templateStage);
            }
        }
    }


    public function configurateCandidate(Candidate $candidate)
    {
        $stages = $this->em->getRepository(Stage::class)->findBy(array('template' => true));
        if ($stages) {
            foreach ($stages as $stage) {
                foreach ($stage->getProcesses() as $process) {
                    foreach ($process->getChildren() as $sub) {
                        $this->addFields($candidate, $sub);
                        $this->addDocuments($candidate, $sub);
                    }
                }
            }
            $this->em->flush();
        }
    }

    protected function addResult(Candidate $candidate, Process $process)
    {
        $result = new ProcessResult();
        $result->setCandidate($candidate);
        $result->setProcess($process);
        $result->setStatus(false);
        return $result;
    }

    protected function addFields(Candidate $candidate, Process $process)
    {
        foreach ($process->getFields() as $field) {
            $result = $this->addResult($candidate, $process);
            $result->setField($field);
            $this->em->persist($result);
        }
    }

    protected function addDocuments(Candidate $candidate, Process $process)
    {
        foreach ($process->getDocuments() as $document) {
            $result = $this->addResult($candidate, $process);
            $result->setDocument($document);
            $this->em->persist($result);
        }
    }
}
