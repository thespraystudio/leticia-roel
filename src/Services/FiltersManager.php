<?php


namespace App\Services;

use App\Interfaces\SSFilterInterface;

class FiltersManager
{

    /**
     * @var SSFilterInterface[]
     */
    private $filterProviders;

    public function __construct(iterable $filterProviders)
    {
        $this->filterProviders = iterator_to_array($filterProviders);
    }

    public function createFilter($name): SSFilterInterface
    {
        foreach ($this->filterProviders as $filterProvider) {
            if ($filterProvider->getName() == $name) {
                return $filterProvider;
            }
        }
        throw new \Exception(sprintf('No existe el filtro para %s', $name));
    }


}