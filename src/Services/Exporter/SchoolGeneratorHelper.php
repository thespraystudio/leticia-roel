<?php

namespace App\Services\Exporter;

use App\Entity\Income;
use App\Entity\School;
use App\Services\XLSHelper;

class SchoolGeneratorHelper
{
    private XLSHelper $XLSHelper;

    public function __construct(XLSHelper $XLSHelper)
    {

        $this->XLSHelper = $XLSHelper;
    }

    public function generate($entities)
    {
        $results = [];
        foreach ($entities as $entity) {
            /** @var  School $entity */

            $types = [];
            $capacity = [];
            $roomsCapacity = [];
            $mexicans = [];
            $closestCity = [];
            $airport = [];
            $period = [];
            $dressCode = [];
            $founded = [];
            foreach ($entity->getDescriptions() as $description) {
                $types[] = $description->getTypeSchool();
                $capacity[] = $description->getCapacity();
                $roomsCapacity[] = $description->getDormitoriesCapacity();
                $mexicans[] = $description->getTotalMexicans();
                $closestCity[] = $description->getClosestCity();
                $airport[] = $description->getAirport();
                $period[] = $description->getPeriod() . ': ' . $description->getCost();
                $dressCode[] = $description->getDresscode();
                $founded[] = $description->getFounded();
            }

            $levels = [];
            foreach ($entity->getLevels() as $level) {
                $levels[] = $level;
            }


            $schoolType = implode(' - ', $types);
            $levelsType = implode(' - ', $levels);
            $capacityType = implode(' - ', $capacity);
            $roomsCapacityType = implode(' - ', $roomsCapacity);
            $mexicansType = implode('-', $mexicans);
            $closestCityType = implode('-', $closestCity);
            $airportType = implode('-', $airport);
            $periodType = implode('-', $period);
            $dressCodeType = implode('-', $dressCode);
            $foundedType = implode('-',$founded);
            $results[] = ['ID' => $entity->getId(), 'Nombre' => $entity->getName(), 'Dirección' => $entity->getAddress(), 'WEB' => $entity->getWebsite(), 'Tipo de colegio' => $schoolType, 'Grados' => $levelsType, 'Número de estudiantes' => $capacityType, 'Número de estudiantes internos' => $roomsCapacityType, 'Número de Mexicanos' => $mexicansType, 'Ciudad más cercana' => $closestCityType, 'Aeropuerto internacional más cercano' => $airportType, 'Costos' => $periodType, 'Dresscode' => $dressCodeType, 'Fundada en' => $foundedType];
        }
        return $this->XLSHelper->export($results, 'Schools', 'Schools', []);
    }
}