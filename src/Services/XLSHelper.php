<?php

namespace App\Services;

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Symfony\Component\HttpFoundation\Response;

class XLSHelper
{
    public function export($results, $reportName, $entityName,$currencySelectors = [])
    {
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $columnsMap = [];
        $lineIndex = 2;
        foreach ($results as $line) {
            foreach ($line as $columnName => $columnValue) {
                if (is_int($columnIndex = array_search($columnName, $columnsMap))) {
                    $columnIndex++;
                } else {
                    $columnsMap[] = $columnName;
                    $columnIndex = count($columnsMap);
                }
                $sheet->getCellByColumnAndRow($columnIndex, $lineIndex)->setValue($columnValue);
            }
            $lineIndex++;
        }
        foreach ($columnsMap as $columnMapId => $columnTitle) {
            $sheet->getCellByColumnAndRow($columnMapId + 1, 1)->setValue($columnTitle);
        }

        foreach ($spreadsheet->getWorksheetIterator() as $worksheet) {
            $spreadsheet->setActiveSheetIndex($spreadsheet->getIndex($worksheet));
            $sheet = $spreadsheet->getActiveSheet();
            $cellIterator = $sheet->getRowIterator()->current()->getCellIterator();
            $cellIterator->setIterateOnlyExistingCells(true);
            /** @var PHPExcel_Cell $cell */
            foreach ($cellIterator as $cell) {
                $sheet->getColumnDimension($cell->getColumn())->setAutoSize(true);
            }
        }

        if (count($currencySelectors) > 0) {
            foreach ($currencySelectors as $selector) {
                $formatCode = NumberFormat::FORMAT_ACCOUNTING_USD;
                $spreadsheet->getActiveSheet()
                    ->getStyle($selector)
                    ->getNumberFormat()
                    ->setFormatCode($formatCode);
            }
        }

        $writer = new Xlsx($spreadsheet);
        ob_start();
        $writer->save('php://output');
        $excelOutput = ob_get_clean();

        $exportName = sprintf($entityName . '_%s_%s.xlsx', $reportName, time());

        return new Response(
            $excelOutput,
            200,
            [
                'content-type' => 'text/x-csv; charset=windows-1251',
                'Content-Disposition' => 'attachment; filename="' . $exportName . '"'
            ]
        );
    }
}