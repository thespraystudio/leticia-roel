<?php


namespace App\Filters;


use App\Entity\Filter\ExpenseEntityFilter;
use App\Entity\Filter\IncomeEntityFilter;
use App\Entity\Filter\InventarioFilter;
use App\Entity\Filter\ReceivableEntityFilter;
use App\Entity\Filter\SchoolFilter as SchoolFilterEntity;
use App\Entity\Receivable;
use App\Form\Filter\ExpenseFilterType;
use App\Form\Filter\IncomeFilterType;
use App\Form\Filter\InventarioFilterType;
use App\Form\Filter\ReceivableFilterType;
use App\Form\Filter\SchoolFilterType;
use App\Interfaces\MSFilterInterface;
use App\Interfaces\SSFilterInterface;
use App\Repository\MovementRepository;
use Symfony\Component\HttpKernel\KernelInterface;


class SchoolFilter extends MainFilter implements SSFilterInterface
{

    public function __construct(KernelInterface $kernel)
    {
        parent::__construct($kernel);
    }

    public function getFormClass(): string
    {
        return SchoolFilterType::class;
    }

    public function getFilterEntity()
    {
        return new SchoolFilterEntity();
    }

    public function getName(): string
    {
        return 'school';
    }

}