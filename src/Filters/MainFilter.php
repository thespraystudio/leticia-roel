<?php


namespace App\Filters;


use Symfony\Component\HttpKernel\KernelInterface;

class MainFilter
{
    public $max_results = 500;
    private $devMaxResults = 20;
    /**
     * @var KernelInterface
     */
    private $kernel;


    public function __construct(KernelInterface $kernel)
    {
        $this->kernel = $kernel;
    }

    public function getMaxResults()
    {
        return $this->kernel->getEnvironment() == "dev" ? $this->devMaxResults : $this->max_results;
    }

}