<?php

namespace App\Form;

use App\Entity\SchoolInformation;
use App\Entity\Currency;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class InformationType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options): void {
        $builder
                ->add('period', null, [
                    'label' => 'Período',
                    'required' => true,
                ])
                ->add('typeSchool', ChoiceType::class, [
                    'label' => 'Tipo de escuela',
                    'choices' => [
                        'All-girls' => 'All-girls',
                        'Co-ed' => 'Co-ed',
                        'All-boys' => 'All-boys',
                    ],
                    'attr' => [
                    ]
                ])
                ->add('cost', null, [
                    'label' => 'Costos/Tuition/Colegiatura',
                    'required' => true,
                ])
                ->add('capacity', null, [
                    'label' => 'Capacidad',
                    'required' => true,
                ])
                ->add('tourVirtual', null, [
                    'label' => 'Campus Tour Virtual Link',
                    'attr' => [
                    ]
                ])
                ->add('airport', null, [
                    'label' => 'Aeropuerto Internacional más cercano',
                    'attr' => [
                    ]
                ])
                ->add('founded', null, [
                    'label' => 'Fundada en',
                    'attr' => [
                    ]
                ])
                ->add('totalMexicans', null, [
                    'label' => 'Número de Mexicanos:',
                    'attr' => [
                    ]
                ])
                ->add('closestCity', null, [
                    'label' => 'Ciudad más cercana:',
                    'attr' => [
                    ]
                ])
                ->add('dresscode', ChoiceType::class, [
                    'label' => 'Dresscode',
                    'choices' => [
                        'Uniforme' => 'Uniforme',
                        'Formal' => 'Formal',
                        'Casual' => 'Casual',
                    ],
                    'attr' => [
                    ]
                ])
                ->add('military', CheckboxType::class, [
                    'label' => 'Militar',
                    'required' => false,
                ])
                ->add('esl', CheckboxType::class, [
                    'label' => 'ESL (English Second Language)',
                    'required' => false,
                ])
                ->add('advancesClasses', CheckboxType::class, [
                    'label' => 'AP/Gifted/Advances classes',
                    'required' => false,
                ])
                ->add('religion', CheckboxType::class, [
                    'label' => 'Religión',
                    'required' => false,
                ])
                ->add('learningDifferences', CheckboxType::class, [
                    'label' => 'Learning Diferences Support',
                    'required' => false,
                ])                
                
                ->add('ib', CheckboxType::class, [
                    'label' => 'IB Prog (International Baccalaureate)',
                    'required' => false,
                ])
                ->add('dormitoriesCapacity', null, [
                    'label' => 'Capacidad de dormitorios',
                    'required' => true,
                ])
                // ->add('currency', null, [
                //     'label' => 'Divisa',
                //     'required' => true,
                // ])
                ->add('saturdayClasses', CheckboxType::class, [
                    'label' => 'Saturday Classes',
                    'required' => false,
                ])
                ->add('iGSCE', CheckboxType::class, [
                    'label' => 'iGSCE o GSCE',
                    'required' => false,
                ])
                ->add('aLevel', CheckboxType::class, [
                    'label' => 'A Level',
                    'required' => false,
                ])
                ->add('requiereGuardian', CheckboxType::class, [
                    'label' => 'Requiere Guardian',
                    'required' => false,
                ])
                ->add('applicationDeadline', CheckboxType::class, [
                    'label' => 'Application Deadline',
                    'required' => false,
                ])
                ->add('rollingAdmission', CheckboxType::class, [
                    'label' => 'Rolling Admission',
                    'required' => false,
                ])


        ;
    }

    public function configureOptions(OptionsResolver $resolver): void {
        $resolver->setDefaults([
            'data_class' => SchoolInformation::class,
        ]);
    }

}
