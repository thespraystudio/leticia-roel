<?php

namespace App\Form;

use App\Entity\Process;
use App\Entity\ProcessField;
use App\Form\StageType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class SubProcessType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $data = $builder->getData(); // data passed to the form
        $builder
            ->add('name', null, [
                'label' => 'Nombre del subproceso'
            ])
            ->add('enviar', SubmitType::class, ['label' => 'enviar', 'attr' => ['class' => '']]);;
            
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Process::class,
        ]);
    }
}
