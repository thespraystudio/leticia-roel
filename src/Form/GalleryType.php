<?php

namespace App\Form;

use App\Entity\Gallery;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Vich\UploaderBundle\Form\Type\VichImageType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class GalleryType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name', null, [
                'label' => 'Nombre'
            ])           
            ->add('imageFile', VichImageType::class, [
                'label' => '',
                'download_uri' => false,
                'required' => false,
                'imagine_pattern' => 'square_thumbnail',
                'attr' => [
                    'class' => 'file-tr',
                ]
            ])
            ->add('url',TextType::class,['label' => 'URL', 'required' => false,])
            ->add('type', ChoiceType::class, [
                'label' => 'Tipo',
                'choices' => [
                    'Foto' => 'image',
                    'Youtube' => 'video',
                ],
            ])
            
        ;
    } 

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Gallery::class,
        ]);
    }
}
