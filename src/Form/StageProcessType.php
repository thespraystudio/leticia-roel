<?php

namespace App\Form;

use App\Entity\Process;
use App\Form\StageType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class StageProcessType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $data = $builder->getData(); // data passed to the form
        // dump($data);
        $builder
            ->add('name',null,[
                'label' => 'Nombre del proceso',
                'required' => true,
                // 'data' => $data ? $data->getName() : ''
            ])
            ->add('position',TextType::class,[
                'label' => 'Posición',
                'empty_data' => 1,
                
            ])
            ->add('type',TextType::class,[
                'label' => 'Tipo',
                'empty_data' => 1
            ])
            ->add('documents',null,[
                'label' => 'Documentos'
            ])
            ->add('stage', null,[
                'required' => true,
                'mapped' => true,
                'label' => 'Stage',
            ])
            ->add('children', null,[
                'required' => false,
                'label' => 'Stage',
                'attr' => [
                    'readonly' => true,
                    'disabled' => true
                ]
            ])
            // ->add('candidate')
            // ->add('school')
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Process::class,
        ]);
    }
}
