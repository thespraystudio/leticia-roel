<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class User2Type extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('username')
            ->add('name')
            ->add('lastname')
            ->add('email')
            ->add('phoneNumber')
            ->add('password')
            ->add('image')
            ->add('created')
            ->add('updated')
            ->add('playerId')
            ->add('roles')
            ->add('birthday')
            ->add('status')
            ->add('notification')
            ->add('operatorCandidates')
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
