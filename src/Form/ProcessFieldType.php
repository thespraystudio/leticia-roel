<?php

namespace App\Form;

use App\Entity\ProcessField;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class ProcessFieldType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options): void {
        // $data = $builder->getData(); // data passed to the form
        $builder
                ->add('name', null, [
                    'label' => 'Nombre'
                ])
                ->add('description', TextAreaType::class, [
                    'label' => 'Descripción'
                ])
                ->add('type', ChoiceType::class, [
                    'label' => 'Tipo de campo',
                    'choices' => [
                        
                        'Check Box' => 2,
                    ],
                    'attr' => [
                    ]
                ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void {
        $resolver->setDefaults([
            'data_class' => ProcessField::class,
        ]);
    }

}
