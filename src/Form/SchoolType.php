<?php

namespace App\Form;

use App\Entity\School;
use App\Entity\Candidate;
use App\Entity\Level;
use App\Entity\Activity;
use App\Entity\Sport;
use App\Entity\SchoolSkill;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Vich\UploaderBundle\Form\Type\VichImageType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use App\Form\StageType;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use App\Entity\Stage;
use Doctrine\ORM\EntityManagerInterface;


class SchoolType extends AbstractType
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {

        $data = $builder->getData();
        $queryBuilder = function (EntityRepository $er) {
            $subquery = $er->createQueryBuilder('sub')
                ->select('IDENTITY(candidate.candidate)')
                ->from('App\Entity\Candidate', 'candidate')
                ->where('candidate.candidate IS NOT NULL')
                ->getDQL();

            return $er->createQueryBuilder('c')
                ->andWhere('c.status != :status')
                ->setParameter('status', 2)
                ->andWhere('c.id NOT IN (' . $subquery . ')');
        };

        $builder
            ->add('imageFile', VichImageType::class, [
                'label' => '',
                'download_uri' => false,
                'required' => false,
                'imagine_pattern' => 'square_thumbnail',
                'attr' => [
                    'class' => 'file-tr',
                ]
            ])
            ->add('name', null, [
                'label' => 'Nombre',
                'attr' => []
            ])
            ->add('levels', EntityType::class, [
                'label' => 'Grado Estudios',
                'required' => false,
                'by_reference' => false,
                'multiple' => true,
                'class' => Level::class,
                'attr' => [
                    'class' => 'js-example-basic-single',

                ],
            ])
            ->add('country', null, [
                'label' => 'País',
                'attr' => []
            ])
            ->add('state', null, [
                'label' => 'Estado/Provincia',
                'attr' => []
            ])
            ->add('city', null, [
                'label' => 'Ciudad',
                'attr' => []
            ])
            ->add('description', null, [
                'label' => 'Descripción',
                'attr' => []
            ])
            ->add('latitud', null, [
                'label' => 'Latitud',
                'attr' => []
            ])
            ->add('longitud', null, [
                'label' => 'Longitud',
                'attr' => []
            ])
            ->add('address', null, [
                'label' => 'Domicilio',
                'attr' => []
            ])
            ->add('website', null, [
                'label' => 'Página web',
                'attr' => []
            ])
            ->add('academics', null, [
                'label' => 'Academics',
                'attr' => []
            ])
            ->add('deportes', null, [
                'label' => 'Deportes',
                'attr' => []
            ])
            ->add('artes', null, [
                'label' => 'Artes',
                'attr' => []
            ])
            ->add('boarding', null, [
                'label' => 'Boarding Life',
                'attr' => []
            ])
            ->add('activities', EntityType::class, [
                'label' => 'Clubs & Activities',
                'required' => false,
                'by_reference' => false,
                'multiple' => true,
                'class' => Activity::class,
                'attr' => [
                    'class' => 'js-example-basic-single',
                ],
            ])
            ->add('sports', EntityType::class, [
                'label' => 'Deportes',
                'required' => false,
                'by_reference' => false,
                'multiple' => true,
                'class' => Sport::class,
                'attr' => [
                    'class' => 'js-example-basic-single',
                ],
            ])
            ->add('activities', EntityType::class, [
                'label' => 'Actividades',
                'required' => false,
                'by_reference' => false,
                'multiple' => true,
                'class' => Activity::class,
                'attr' => [
                    'class' => 'js-example-basic-single',
                ],
            ])
            ->add('descriptions', CollectionType::class, [
                'entry_type' => InformationType::class,
                'allow_add' => false,
                'allow_delete' => false,
                'by_reference' => false,
            ])

            ->add('skill', EntityType::class, [
                'required' => false,
                'by_reference' => false,
                'multiple' => true,
                'class' => SchoolSkill::class,
                'attr' => [
                    'class' => 'js-example-basic-single',

                ],
            ])
            ->add('gallery', CollectionType::class, [
                'entry_type' => GalleryType::class,
                'allow_add' => true,
                'allow_delete' => true,
                'by_reference' => false,
            ])
            ->add('contacts', CollectionType::class, [
                'entry_type' => SchoolContactType::class,
                'allow_add' => true,
                'allow_delete' => true,
                'by_reference' => false,
            ])
            ->add('canditates', EntityType::class, [
                'required' => false,
                'by_reference' => false,
                'multiple' => true,
                'class' => Candidate::class,
                'attr' => [
                    'class' => 'js-example-basic-single',

                ],
                'query_builder' => $queryBuilder
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => School::class,
            'school_id' => null,

        ]);
        $resolver->setRequired('entity_manager');
        $resolver->setAllowedTypes('entity_manager', EntityManagerInterface::class);
    }
}
