<?php

namespace App\Form\Filter;

use App\Entity\Activity;
use App\Entity\Country;
use App\Entity\Filter\SchoolFilter;
use App\Entity\Level;
use App\Entity\Sport;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SchoolFilterType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('country', EntityType::class, [
                'class' => Country::class,
                'multiple' => true,
                'expanded' => true,
                'required' => false,
            ])

            ->add('sports', EntityType::class, [
                'class' => Sport::class,
                'multiple' => true,
                'expanded' => true,
                'required' => false,
            ])

            ->add('activities', EntityType::class, [
                'class' => Activity::class,
                'multiple' => true,
                'expanded' => true,
                'required' => false
            ])
            ->add('religion', ChoiceType::class, [
                'multiple' => false,
                'expanded' => true,
                'required' => false,
                'placeholder' => false,
                'choices' => ['Sin afiliación' => 0, 'Con religión' => 1]
            ])
            ->add('military', ChoiceType::class, [
                'multiple' => false,
                'expanded' => true,
                'required' => false,
                'placeholder' => false,
                'choices' => ['No' => 0, 'Si' => 1]
            ])
            ->add('dresscode', ChoiceType::class, [
                'multiple' => false,
                'expanded' => true,
                'required' => false,
                'placeholder' => false,
                'choices' => ['Casual' => 'Casual', 'Formal' => 'Formal', 'Uniforme' => 'Uniforme']
            ])
            ->add('typeSchool', ChoiceType::class, [
                'multiple' => true,
                'expanded' => true,
                'required' => false,
                'placeholder' => false,
                'choices' => [
                    'All-girls' => 'All-girls',
                    'Co-ed' => 'Co-ed',
                    'All-boys' => 'All-boys',
                ]
            ])
            ->add('levels', EntityType::class, [
                'class' => Level::class,
                'multiple' => true,
                'expanded' => true,
                'required' => false
            ])
            ->add('others', ChoiceType::class, [
                'multiple' => true,
                'required' => false,
                'expanded' => true,
                'choices' => [
                    'AP/Gifed/Advances clases' => 1,
                    'Learning Diferences Support' => 2,
                    'Requiere Guardian' => 3,
                    'A Level' => 4,
                    'IB Diproma (International Baccalaureate)' => 5,
                    'Saturday Classes' => 6,
                    'Rolling Admission' => 7,
                    'Application Deadline' => 8
                ]
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => SchoolFilter::class,
        ]);
    }
}
