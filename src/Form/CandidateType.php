<?php

namespace App\Form;

use App\Entity\Candidate;
use App\Entity\School;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Vich\UploaderBundle\Form\Type\VichImageType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use App\Repository\CandidateRepository;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\NotBlank;


class CandidateType extends AbstractType
{
    private $candidateRepository;

    public function __construct(CandidateRepository $candidateRepository)
    {
        $this->candidateRepository = $candidateRepository;
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {

        $queryBuilder = function (EntityRepository $er) {


            return $er->createQueryBuilder('c')
                ->andWhere('c.status != :status')
                ->setParameter('status', 2)
                ->andWhere('c.type = :type')
                ->setParameter('type', 2);;
        };

        $builder
            ->add('imageFile', VichImageType::class, [
                'label' => '',
                'download_uri' => false,
                'required' => false,
                'imagine_pattern' => 'square_thumbnail',
                'attr' => [
                    'class' => 'file-tr',
                ]
            ])
            ->add('name', null, [
                'label' => 'Nombre',
                'required' => true,
                'attr' => []
            ])
            ->add('lastname', null, [
                'label' => 'Apellidos',
                'required' => true,
                'attr' => []
            ])
            ->add(
                'birthday',
                DateType::class,
                [
                    'widget' => 'single_text',
                    'html5' => true,
                    'label' => 'Fecha de nacimiento',
                    'required' => true,
                    'empty_data' => '',
                    'attr' => [
                        'min' => (new \DateTime('-100 years'))->format('Y-m-d'),
                        'max' => (new \DateTime('-15 years'))->format('Y-m-d')
                    ]
                ]
            )
            ->add('street', null, [
                'label' => 'Calle',
                'required' => true,
                'attr' => []
            ])
            ->add('inNumber', null, [
                'label' => 'Número interior',
                'required' => false,
                'attr' => []
            ])
            ->add('outNumber', null, [
                'label' => 'Número exterior',
                'required' => true,
                'attr' => []
            ])
            ->add('state', null, [
                'label' => 'Estado',
                'required' => true,
                'attr' => []
            ])
            ->add('currentSchool', null, [
                'label' => 'Escuela actual',
                'required' => true,
                'attr' => []
            ])
            ->add('currentGrade', null, [
                'label' => 'Grado actual',
                'required' => true,
                'attr' => []
            ])
            ->add('phoneNumber', null, [
                'label' => 'Teléfono',
                'required' => true,
                'attr' => []
            ])
            ->add('email', null, [
                'label' => 'Email',
                'required' => true,
                'attr' => [],
                'constraints' => [

                    new Email([
                        'message' => 'El formato del correo electrónico no es válido.'
                    ])
                ]
            ])
            ->add('newCandidate', TutorType::class, [
                'label' => false,
                'mapped' => false,
            ])
            ->add('school', EntityType::class, [
                'required' => false,
                'by_reference' => false,
                'multiple' => true,
                'class' => School::class,
                'attr' => [
                    'class' => 'js-example-basic-single',

                ],
            ]);
        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) use ($queryBuilder) {
            $form = $event->getForm();
            $candidate = $event->getData();

            if ($candidate && $candidate->getId()) {
                // $form->add('candidate', EntityType::class, [
                //     'required' => false,
                //     'by_reference' => false,
                //     'multiple' => false,
                //     'class' => Candidate::class,
                //     'attr' => [
                //         'class' => 'js-example-basic-single',
                //     ],
                //     'query_builder' => $queryBuilder, 
                // ]);
            } else {
                $form
                    ->add('candidate', EntityType::class, [
                        'required' => false,
                        'by_reference' => false,
                        'multiple' => false,
                        'label' => 'Tutor',
                        'class' => Candidate::class,
                        'attr' => [
                            'class' => 'js-example-basic-single',
                        ],
                        'query_builder' => $queryBuilder,
                    ])
                    ->add('isNewCandidate', CheckboxType::class, [
                        'label' => '¿Es un tutor nuevo?',
                        'attr' => [
                            'class' => '',
                        ],
                        'required' => false,
                        'mapped' => false,
                    ]);
            }
        });
    }


    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Candidate::class,

        ]);
    }
}
