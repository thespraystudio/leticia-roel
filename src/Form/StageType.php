<?php

namespace App\Form;

use App\Entity\Process;
use App\Entity\Stage;
use App\Form\SchoolType;
use App\Form\StageProcessType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
class StageType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $data = $builder->getData();

        $builder
            ->add('name', null, [
                'label' => 'Etapa'
            ])
            ->add('color', null, [
                'label' => 'Color',
                'attr' => [
                    'data-jscolor' => "{ format: 'rgba', required: false, palette: '#C00 #0C0 #00C', previewPosition: 'right' }",
                ]
            ])
            ->add('school', HiddenType::class, [
                'mapped' => false, 
            ])
            ->add('enviar', SubmitType::class, ['label' => 'enviar', 'attr' => ['class' => '']]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Stage::class,

        ]);
    }
}
