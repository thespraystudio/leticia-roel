<?php

namespace App\Form;

use App\Entity\User;
use App\Entity\Candidate;
use App\Form\CandidateType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\File;
use Vich\UploaderBundle\Form\Type\VichImageType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Doctrine\ORM\EntityRepository;

class UserType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $object = $options['data'];
        $queryBuilder = function (EntityRepository $er) {
            $subquery = $er->createQueryBuilder('sub')
                ->select('IDENTITY(candidate.candidate)')
                ->from('App\Entity\Candidate', 'candidate')
                ->where('candidate.candidate IS NOT NULL')
                ->getDQL();

            return $er->createQueryBuilder('c')
                ->andWhere('c.status != :status')
                ->setParameter('status', 2)
                ->andWhere('c.id NOT IN (' . $subquery . ')');
        };


        $builder
            ->add('imageFile', VichImageType::class, [
                'label' => '',
                'download_uri' => false,
                'required' => false,
                'imagine_pattern' => 'square_thumbnail',
                'attr' => [
                    'class' => 'file-tr',
                ]
            ])
            ->add('name', null, [
                'label' => 'Nombre',
                'required' => true,
                'attr' => [
                    'readonly' => $options['readonly']
                ]
            ])
            ->add('lastname', null, [
                'label' => 'Apellidos',
                'attr' => [
                    'readonly' => $options['readonly']
                ]
            ])
            ->add(
                'birthday',
                DateType::class,
                [
                    'widget' => 'single_text',
                    'html5' => true,
                    'label' => 'Fecha de nacimiento',
                    'required' => false,
                    'empty_data' => '',
                    'attr' => [
                        'readonly' => $options['readonly'],
                        'min' => (new \DateTime('-100 years'))->format('Y-m-d'),
                        'max' => (new \DateTime('-15 years'))->format('Y-m-d')
                    ]
                ]

            )
            ->add('phoneNumber', null, [
                'label' => 'No. Teléfono',
                'attr' => [
                    'readonly' => $options['readonly']
                ]
            ])
            ->add('email', null, [
                'label' => 'Correo electrónico',
                'attr' => [
                    'readonly' => $options['readonly']
                ]
            ])
            ->add('status', null, [
                'label' => 'Correo electrónico',
                'attr' => [
                    'readonly' => $options['readonly']
                ]
            ])
            ->add('plainPassword', PasswordType::class, [
                'label' => 'Contraseña',
                'required' => !$object || !$object->getId(),
                'attr' => [
                    'readonly' => $options['readonly']
                ]
            ])
            ->add('operatorCandidates', EntityType::class, [
                'required' => false,
                'label' => 'Aspirantes',
                'by_reference' => false,
                'multiple' => true,
                'class' => Candidate::class,
                'attr' => [
                    'class' => 'js-example-basic-single',
                    'readonly' => $options['readonly']
                ],
                'query_builder' => $queryBuilder,
            ]);

        // $builder->add('birthday', CollectionType::class,[
        //     'entry_type' => CandidateType::class,
        // ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => User::class,
            'cascade_validation' => true,
            'readonly' => false,
        ]);
    }
}
