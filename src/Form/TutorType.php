<?php

namespace App\Form;

use App\Entity\Candidate;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Component\Form\Extension\Core\Type\DateType;
use Vich\UploaderBundle\Form\Type\VichImageType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;

class TutorType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {

        $builder
            ->add('imageFile', VichImageType::class, [
                'label' => '',
                'download_uri' => false,
                'required' => false,
                'attr' => [
                    'class' => 'file-tr',

                ]
            ])
            ->add('name', null, [
                'label' => 'Nombre',
                'required' => false,
            ])
            ->add('lastname', null, [
                'label' => 'Apellidos',
                'required' => false,
            ])
            ->add(
                'birthday',
                DateType::class,
                [
                    'widget' => 'single_text',
                    'html5' => true,
                    'required' => false,
                    'label' => 'Fecha de nacimiento',
                    'empty_data' => '',
                    'attr' => [
                        'min' => (new \DateTime('-100 years'))->format('Y-m-d'),
                        'max' => (new \DateTime('-15 years'))->format('Y-m-d')
                    ]
                ]
            )
            ->add('street', null, [
                'label' => 'Calle',
                'required' => false,
            ])
            ->add('inNumber', null, [
                'label' => 'Número interior',
                'required' => false,
            ])
            ->add('outNumber', null, [
                'label' => 'Número exterior',
                'required' => false,
            ])
            ->add('state', null, [
                'label' => 'Estado',
                'required' => false,
            ])
            ->add('phoneNumber', null, [
                'label' => 'Teléfono',
                'required' => false,
            ])
            ->add('email', null, [
                'label' => 'Email',
                'required' => false,
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Candidate::class,
            'tutor_id' => null,
        ]);
    }
}
