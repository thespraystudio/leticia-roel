<?php


namespace App\DataFixtures;


use App\Entity\Level;
use App\Entity\State;
use App\Utils\ImagesList;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class StateFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {

        $states = [
            '1' => 'Aguascalientes',
            '2' => 'Baja California',
            '3' => 'Baja California Sur',
            '4' => 'Campeche',
            '5' => 'Chiapas',
            '6' => 'Chihuahua',
            '7' => 'Coahuila de Zaragoza',
            '8' => 'Colima',
            '9' => 'Ciudad de México',
            '10' => 'Durango',
            '11' => 'Guanajuato',
            '12' => 'Guerrero',
            '13' => 'Hidalgo',
            '14' => 'Jalisco',
            '15' => 'Mexico',
            '16' => 'Michoacan de Ocampo',
            '17' => 'Morelos',
            '18' => 'Nayarit',
            '19' => 'Nuevo Leon',
            '20' => 'Oaxaca',
            '21' => 'Puebla',
            '22' => 'Queretaro de Arteaga',
            '23' => 'Quintana Roo',
            '24' => 'San Luis Potosi',
            '25' => 'Sinaloa',
            '26' => 'Sonora',
            '27' => 'Tabasco',
            '28' => 'Tamaulipas',
            '29' => 'Tlaxcala',
            '30' => 'Veracruz-Llave',
            '31' => 'Yucatan',
            '32' => 'Zacatecas',
        ];

        foreach ($states as $state) {
            $object = new State();
            $object->setName($state);
            $manager->persist($object);
        }

        $manager->flush();
    }
}