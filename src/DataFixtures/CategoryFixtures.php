<?php


namespace App\DataFixtures;


use App\Entity\Category;
use App\Utils\ImagesList;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class CategoryFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $faker = Factory::create();
        $parents = [];

        for ($i = 0; $i < 19; $i++) {
            $category = new Category();
            $category->setName($faker->word());
            $category->setImage($faker->randomElement(ImagesList::$images));
            $category->setType('choice');
            $manager->persist($category);
            $parents[] = $category;
        }

        $manager->flush();


        foreach ($parents as $parent) {
            for($i=0;$i<rand(1,5);$i++){
                $category = new Category();
                $category->setName($faker->word());
                $category->setImage($faker->randomElement(ImagesList::$images));
                $category->setType('choice');
                $category->setCategory($parent);
                $manager->persist($category);
            }
        }

        $manager->flush();
    }


}