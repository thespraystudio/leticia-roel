<?php


namespace App\DataFixtures;


use App\Entity\Level;
use App\Utils\ImagesList;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class LevelFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $levels = ["High School", "University","Junior Boarding School"];
        $faker = Factory::create();

        foreach ($levels as $level) {
            $object = new Level();
            $object->setName($level);
            $object->setImage($faker->randomElement(ImagesList::$images));
            $manager->persist($object);
        }

        $manager->flush();
    }
}