<?php


namespace App\DataFixtures;


use App\Entity\Activity;
use App\Entity\Country;
use App\Entity\Currency;
use App\Entity\Gallery;
use App\Entity\Level;
use App\Entity\Outstanding;
use App\Entity\School;
use App\Entity\SchoolContact;
use App\Entity\SchoolInformation;
use App\Utils\ImagesList;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class CardiganFixture extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {

        $faker = Factory::create();

        $activities = $manager->getRepository(Activity::class)->findAll();
        $usa = $manager->getRepository(Country::class)->findOneBy(['name' => 'Estados Unidos']);
        $usd = $manager->getRepository(Currency::class)->findOneBy(['code' => 'USD']);
        $level = $manager->getRepository(Level::class)->findOneBy(['name' => 'Junior Boarding School']);
        $outstandings = [['1945','Founded'],['225','Students'],['25','States'],['17','Countries'],['73','Faculty Members'],['4:1','Studen:Teacher Ratio'],['525','Acre Campus'],['39','Athletic Teams in 17 Sports']];
        $galleries = $manager->getRepository(Gallery::class)->findAll();

        $school = new School();
        $school->setName('Cardigan Mountain School');
        $school->setDescription('Colegio con 214 estudiantes de los cuales aproximadamente 186 son internos.');
        $school->setLatitud(43.028801);
        $school->setLongitud(-71.757637);
        $school->setAddress('62 Alumni Drive, Canaan, New Hampshire 03741');
        $school->setWebsite('http://www.cardigan.org');
        $school->setCountry($usa);
        $school->addLevel($level);

        $information = new SchoolInformation();
        $information->setStatus(true);
        $information->setCost(74000);
        $information->setPeriod('2020/2021');
        $information->setCapacity(214);
        $information->setDormitoriesCapacity(186);
        $information->setSchool($school);
        $information->setCurrency($usd);
        $manager->persist($information);
        $school->addDescription($information);

        $contacts = [
            ['Courtney Mckahan','cmckahan@cardigan.org','Assistant to Admissions for Administration & Marketing','6035233748','6035233566',null],
            ['Jessica Bayreuther','jebah@cardigan.org','Associate Director of Adminssions','6035233548',null,'cmsadmissions'],
            ['John Bayreuther','jbayreuther@cardigan.org','Associate Director of Adminssions','6035233544',null,null],
        ];

        foreach ($contacts as $contact){
            $schoolContact = new SchoolContact();
            $schoolContact->setName($contact[0]);
            $schoolContact->setEmail($contact[1]);
            $schoolContact->setPosition($contact[2]);
            $schoolContact->setPhoneNumber($contact[3]);
            $schoolContact->setMobilePhoneNumber($contact[4]);
            $schoolContact->setSkype($contact[5]);
            $schoolContact->setSchool($school);
            $manager->persist($schoolContact);
            $school->addContact($schoolContact);
        }

        foreach ($activities as $activity) {
            $school->addActivity($activity);
        }

        foreach ($outstandings as $outstanding){
            $object = new Outstanding();
            $object->setName($outstanding[1]);
            $object->setValue($outstanding[0]);
            $object->setFile($faker->randomElement(ImagesList::$images));
            $school->addOutstanding($object);
            $manager->persist($object);
        }

        $pgalleries = $faker->randomElements($galleries, rand(1, count($galleries)));
        foreach ($pgalleries as $pgallary) {
            $school->addGallery($pgallary);
        }

        $manager->persist($school);
        $manager->flush();

    }

    public function getDependencies()
    {
        return array(
            ActivityFixtures::class,
            GalleryFixtures::class,
            OutstandingFixtures::class,
            LevelFixtures::class,
            CurrencyFixtures::class
        );
    }
}