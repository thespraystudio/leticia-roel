<?php


namespace App\DataFixtures;


use App\Entity\Currency;
use App\Entity\Level;
use App\Utils\ImagesList;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class CurrencyFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $currencies = [
            ['Dólar','$','USD'],
            ['Dólar Canadiense','$','CAD'],
            ['Euro','€','EUR'],
            ['Franco Suizo','CHF','CHF']
        ];
        $faker = Factory::create();

        foreach ($currencies as $currency) {
            $object = new Currency();
            $object->setName($currency[0]);
            $object->setSymbol($currency[1]);
            $object->setCode($currency[2]);
            $manager->persist($object);
        }

        $manager->flush();
    }
}