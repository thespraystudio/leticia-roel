<?php


namespace App\DataFixtures;


use App\Entity\Activity;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class ActivityFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $activities = ["Fútbol", "Fútbol Americano", "Tenis", "Lucha", "Béisbol", "Hockey", "Lacrosse", "Sky"];

        foreach ($activities as $activity) {
            $object = new Activity();
            $object->setName($activity);
            $manager->persist($object);
        }

        $manager->flush();
    }
}