<?php


namespace App\DataFixtures;


use App\Entity\Outstanding;
use App\Utils\ImagesList;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class OutstandingFixtures extends Fixture
{

    public function load(ObjectManager $manager)
    {
        $faker = Factory::create();
        for ($i = 0; $i < rand(5, 30); $i++) {
            $outstanding = new Outstanding();
            $outstanding->setName($faker->name());
            $outstanding->setFile($faker->randomElement(ImagesList::$images));
            $outstanding->setValue(rand(5, 30));
            $manager->persist($outstanding);
        }

        $manager->flush();
    }
}