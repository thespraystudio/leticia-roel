<?php


namespace App\DataFixtures;


use App\Entity\Continent;
use App\Entity\Country;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class CountryFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $continents = ['América' => [array('Estados Unidos','153-united-states-of-america.png'),array('Canada','206-canada.png')],'Europa' => [array('Francia','077-france.png'),array('España','044-spain.png'),array('Italia','011-italy.png'),array('Suiza','097-switzerland.png')]];

        foreach ($continents as $continent => $countries){
            $object = new Continent();
            $object->setName($continent);

            $manager->persist($object);

            foreach ($countries as $country){
                $oc = new Country();
                $oc->setName($country[0]);
                $oc->setContinent($object);
                $oc->setFlag(sprintf('flags/%s',$country[1]));
                $manager->persist($oc);
            }
        }

        $manager->flush();
    }
}