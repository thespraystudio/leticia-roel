<?php

namespace App\DataFixtures;

use App\Entity\Activity;
use App\Entity\Continent;
use App\Entity\Country;
use App\Entity\Gallery;
use App\Entity\Level;
use App\Entity\Outstanding;
use App\Entity\School;
use App\Entity\SchoolSkill;
use App\Utils\ImagesList;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class SchoolFixtures extends Fixture implements DependentFixtureInterface
{

    public function load(ObjectManager $manager)
    {

        /*

        $galleries = $manager->getRepository(Gallery::class)->findAll();
        $outstandings = $manager->getRepository(Outstanding::class)->findAll();

        $faker = Factory::create();
        $continents = [];
        $countries = [];
        $activities = [];
        $skills = [];
        $levels = [];

        for ($i = 0; $i < rand(1, 45); $i++) {
            $skill = new SchoolSkill();
            $skill->setName($faker->word);
            $skill->setTags(implode(',', $faker->words()));
            $skills[] = $skill;
            $manager->persist($skill);
        }

        for ($i = 0; $i < rand(1, 45); $i++) {
            $activity = new Activity();
            $activity->setName($faker->word);
            $activity->setImage($faker->randomElement(ImagesList::$images));
            $activities[] = $activity;
            $manager->persist($activity);
        }

        for ($i = 0; $i < rand(1, 5); $i++) {
            $continent = new Continent();
            $continent->setName($faker->word);
            $continents[] = $continent;
            $manager->persist($continent);
        }

        for ($i = 0; $i < rand(3, 5); $i++) {
            $level = new Level();
            $level->setName($faker->word);
            $level->setImage($faker->randomElement(ImagesList::$images));
            $levels[] = $level;
            $manager->persist($level);
        }

        for ($i = 0; $i < rand(5, 10); $i++) {
            $country = new Country();
            $country->setName($faker->country);
            $country->setContinent($faker->randomElement($continents));
            $country->setFlag('flags/' . $faker->randomElement(ImagesList::$flags));
            $manager->persist($country);
            $countries[] = $country;
        }

        for ($i = 0; $i < rand(100, 500); $i++) {
            $school = new School();
            $school->setAddress($faker->address);
            $school->setName($faker->name);
            $school->setDescription($faker->paragraphs(3, true));
            $school->setLatitud($faker->latitude);
            $school->setLongitud($faker->longitude);
            $school->setCountry($faker->randomElement($countries));

            $pactivities = $faker->randomElements($activities, rand(1, count($activities)));
            foreach ($pactivities as $pactivity) {
                $school->addActivity($pactivity);
            }

            $pskills = $faker->randomElements($skills, rand(1, count($skills)));
            foreach ($pskills as $pskill) {
                $school->addSkill($pskill);
            }

            $plevels = $faker->randomElements($levels, rand(1, count($levels)));
            foreach ($plevels as $plevel) {
                $school->addLevel($plevel);
            }

            $pgalleries = $faker->randomElements($galleries, rand(1, count($galleries)));
            foreach ($pgalleries as $pgallary) {
                $school->addGallery($pgallary);
            }
            $poutstandings = $faker->randomElements($outstandings, rand(1, count($outstandings)));
            foreach ($poutstandings as $poutstanding) {
                $school->addOutstanding($poutstanding);
            }

            $manager->persist($school);
        }

        $manager->flush();

        */

    }

    public function getDependencies()
    {
        return array(
            GalleryFixtures::class,
            OutstandingFixtures::class
        );
    }
}
