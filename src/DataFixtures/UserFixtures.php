<?php

namespace App\DataFixtures;

use App\Entity\User;
use App\Utils\ImagesList;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserFixtures extends Fixture
{

    protected $encoder;
    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->encoder = $passwordEncoder;
    }

    public function load(ObjectManager $manager)
    {
        $faker = Factory::create();
        $user = new User();
        $user->setName('Guillermo');
        $user->setPhoneNumber('5569769905');
        $user->setLastname('Zeleny');
        $user->setEmail('guillermo@spraystudio.com.mx');
        $user->setPlayerId($faker->uuid);
        $password = $this->encoder->encodePassword($user, '56fubu124817');
        $user->setPassword($password);
        $user->setRoles(['ROLE_ADMIN']);
        $user->setImage($faker->randomElement(ImagesList::$images));
        $manager->persist($user);


        $faker = Factory::create();
        $user2 = new User();
        $user2->setName('Jose');
        $user2->setPhoneNumber('999999999');
        $user2->setLastname('Bernabe');
        $user2->setEmail('jose@spraystudio.com.mx');
        $user2->setPlayerId($faker->uuid);
        $password = $this->encoder->encodePassword($user2, '1234');
        $user2->setPassword($password);
        $user2->setRoles(['ROLE_ADMIN']);
        $user2->setImage($faker->randomElement(ImagesList::$images));
        $manager->persist($user2);

        $manager->flush();
    }
}
