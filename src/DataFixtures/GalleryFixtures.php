<?php

namespace App\DataFixtures;
use App\Entity\Gallery;
use App\Utils\ImagesList;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class GalleryFixtures extends Fixture
{

    public function load(ObjectManager $manager)
    {
        $faker = Factory::create();

        foreach (ImagesList::$images as $image){
            $gallery = new Gallery();
            $gallery->setFile($image);
            $gallery->setType('image');
            $gallery->setName($faker->words(3,true));
            $manager->persist($gallery);
        }

        $manager->flush();
    }
}