<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\SerializedName;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use App\Controller\Api\DocumentController;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Core\Annotation\ApiFilter;

/**
 * @ApiResource(
 * collectionOperations={
 *     "get",
 *      "post"={
 *             "controller"=DocumentController::class,
 *             "deserialize"=false,
 *             "validation_groups"={"Default", "write"},
 *         },
 * },
 *     itemOperations={
 *     "get",
 *     "put",
 *     "delete"
 *     },
 *     normalizationContext={"groups"={"read"}},
 *     denormalizationContext={"groups"={"write"}},
 * ))
 * @ORM\Entity(repositoryClass="App\Repository\CanidateDocumentRepository")
 * @Vich\Uploadable
 * @ApiFilter(SearchFilter::class, properties={"candidate": "exact"})
 */
class CandidateDocument
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"read"})
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Candidate", inversedBy="documents")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"read","write"})
     */
    private $candidate;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Document", inversedBy="candidateDocuments" , cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false, onDelete="CASCADE")
     * @Groups({"read","write"})
     */
    private $document;


    /**
     * @ORM\Column(type="string", length=255,nullable=true)
     * @SerializedName("file")
     * @Groups({"read","write"})
     */
    private $file;

    /**
     * @Vich\UploadableField(mapping="media", fileNameProperty="file")
     * 
     * @var File
     */
    private $documentFile;

    use TimestampableEntity;


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCandidate(): ?Candidate
    {
        return $this->candidate;
    }

    public function setCandidate(?Candidate $candidate): self
    {
        $this->candidate = $candidate;

        return $this;
    }

   

    public function getFile(): ?string
    {
        return $this->file;
    }

    public function setFile(?string $file): self
    {
        $this->file = $file;

        return $this;
    }

    public function setDocumentFile(File $image = null)
    {
        $this->documentFile = $image;

        if ($image) {
            $this->updatedAt = new \DateTime('now');
        }
    }

    public function getDocumentFile()
    {
        return $this->documentFile;
    }

    public function __toString()
    {
        return sprintf('%s-%s',$this->getCandidate()->getName(),$this->getFile());
    }

    public function getDocument(): ?Document
    {
        return $this->document;
    }

    public function setDocument(?Document $document): self
    {
        $this->document = $document;

        return $this;
    }

}
