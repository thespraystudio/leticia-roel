<?php

namespace App\Entity;

use App\Repository\CandidateStageRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;


/**
 * @ORM\Entity(repositoryClass="App\Repository\CandidateStageRepository")
 * @UniqueEntity(fields={"candidate"}, errorPath="candidate", message="Este candidato ya está asociado a una etapa.")
 * @UniqueEntity(fields={"stage"}, errorPath="stage", message="Esta etapa ya está asociada a un candidato.")
 */
class CandidateStage
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Candidate", inversedBy="candidateStages")
     * @ORM\JoinColumn(nullable=false)
     */
    private $candidate;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Stage", inversedBy="candidateStages")
     * @ORM\JoinColumn(nullable=false)
     */
    private $stage;

    

    /**
     * @ORM\ManyToOne(targetEntity=School::class, inversedBy="candidateStages")
     */
    private $school;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCandidate(): ?Candidate
    {
        return $this->candidate;
    }

    public function setCandidate(?Candidate $candidate): self
    {
        $this->candidate = $candidate;

        return $this;
    }

    public function getStage(): ?Stage
    {
        return $this->stage;
    }

    public function setStage(?Stage $stage): self
    {
        $this->stage = $stage;

        return $this;
    }

    public function getPercent(): ?int
    {
        return $this->percent;
    }

    public function setPercent(?int $percent): self
    {
        $this->percent = $percent;

        return $this;
    }

    public function getSchool(): ?School
    {
        return $this->school;
    }

    public function setSchool(?School $school): self
    {
        $this->school = $school;

        return $this;
    }
}
