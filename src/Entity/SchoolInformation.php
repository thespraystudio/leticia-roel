<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Behavior\Timestampable;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ApiResource(
 *     collectionOperations={
 *     "get",
 * },
 *     itemOperations={
 *     "get",
 *     },
 *     normalizationContext={"groups"={"read"}},
 *     denormalizationContext={"groups"={"write"}},
 * )
 * @ORM\Entity(repositoryClass="App\Repository\SchoolInformationRepository")
 */
class SchoolInformation
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"read"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100)
     * @Groups({"read"})
     */
    private $period;

    /**
     * @ORM\Column(type="string", length=150)
     * @Groups({"read"})
     */
    private $cost;

    

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\School", inversedBy="descriptions")
     * @ORM\JoinColumn(nullable=false)
     */
    private $school;

    /**
     * @ORM\Column(type="boolean")
     * @Groups({"read"})
     */
    private $status = 1;

    /**
     * @ORM\Column(type="integer")
     * @Groups({"read"})
     */
    private $capacity;
    
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"read"})
     */
    private $typeSchool=0;

    /**
     * @ORM\Column(type="boolean")
     * @Groups({"read"}) 
     */
    private $religion;

    

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"read"})
     */
    private $tourVirtual;

    /**
     * @ORM\Column(type="string", length=40, nullable=true)
     * @Groups({"read"}) 
     */
    private $founded;

    /**
     * @ORM\Column(type="string", length=40, nullable=true)
     * @Groups({"read"}) 
     */
    private $dresscode;

    /**
     * @ORM\Column(type="string", length=80, nullable=true)
     */
    private $contactEmail;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     * @Groups({"read"}) 
     */
    private $esl;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     * @Groups({"read"}) 
     */
    private $military;
    
    /**
     * @ORM\Column(type="boolean", nullable=true)
     * @Groups({"read"})    
     */
    private $advancesClasses;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     * @Groups({"read"}) 
     */
    private $learningDifferences;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $interscholasticsports;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $extracurricularActivities;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $postGrad;

    /**
     * @ORM\Column(type="integer")
     * @Groups({"read"})
     */
    private $totalMexicans = 0;

    /**
     * @ORM\Column(type="string", length=120, nullable=true)
     * @Groups({"read"})
     */
    private $closestCity;

    /**
     * @ORM\Column(type="integer")
     * @Groups({"read"})
     */
    private $dormitoriesCapacity;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"read"})
     */
    private $airport;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $addAdhd;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     * @Groups({"read"}) 
     */
    private $saturdayClasses;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     * @Groups({"read"}) 
     */
    private $iGSCE;
    /**
     * @ORM\Column(type="boolean", nullable=true)
     * @Groups({"read"}) 
     */
    private $aLevel;
    /**
     * @ORM\Column(type="boolean", nullable=true)
     * @Groups({"read"}) 
     */
    private $requiereGuardian;
    /**
     * @ORM\Column(type="boolean", nullable=true)
     * @Groups({"read"}) 
     */
    private $applicationDeadline;
    /**
     * @ORM\Column(type="boolean", nullable=true)
     * @Groups({"read"}) 
     */
    private $rollingAdmission;
    /**
     * @ORM\Column(type="boolean", nullable=true)
     * @Groups({"read"}) 
     */
    private $ib;

    use Timestampable;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPeriod(): ?string
    {
        return $this->period;
    }

    public function setPeriod(string $period): self
    {
        $this->period = $period;

        return $this;
    }

    public function getCost(): ?string
    {
        return $this->cost;
    }

    public function setCost(string $cost): self
    {
        $this->cost = $cost;

        return $this;
    }

    

    public function getSchool(): ?School
    {
        return $this->school;
    }

    public function setSchool(?School $school): self
    {
        $this->school = $school;

        return $this;
    }

    public function getStatus(): ?bool
    {
        return $this->status;
    }

    public function setStatus(bool $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getCapacity(): ?int
    {
        return $this->capacity;
    }

    public function setCapacity(int $capacity): self
    {
        $this->capacity = $capacity;

        return $this;
    }

    public function getDormitoriesCapacity(): ?int
    {
        return $this->dormitoriesCapacity;
    }

    public function setDormitoriesCapacity(int $dormitoriesCapacity): self
    {
        $this->dormitoriesCapacity = $dormitoriesCapacity;

        return $this;
    }
    public function __toString()
    {
        return $this->getSchool()->getName().' ' .  $this->getPeriod();
    }

    public function getTypeSchool(): ?string
    {
        return $this->typeSchool;
    }

    public function setTypeSchool(?string $typeSchool): self
    {
        $this->typeSchool = $typeSchool;

        return $this;
    }

    public function getReligion(): ?bool
    {
        return $this->religion;
    }

    public function setReligion(?bool $religion): self
    {
        $this->religion = $religion;

        return $this;
    }

   

    public function getTourVirtual(): ?string
    {
        return $this->tourVirtual;
    }

    public function setTourVirtual(?string $tourVirtual): self
    {
        $this->tourVirtual = $tourVirtual;

        return $this;
    }

    public function getFounded(): ?string
    {
        return $this->founded;
    }

    public function setFounded(?string $founded): self
    {
        $this->founded = $founded;

        return $this;
    }

    public function getDresscode(): ?string
    {
        return $this->dresscode;
    }

    public function setDresscode(?string $dresscode): self
    {
        $this->dresscode = $dresscode;

        return $this;
    }

    public function getContactEmail(): ?string
    {
        return $this->contactEmail;
    }

    public function setContactEmail(?string $contactEmail): self
    {
        $this->contactEmail = $contactEmail;

        return $this;
    }
    
     public function getMilitary(): ?bool
    {
        return $this->military;
    }

    public function setMilitary(?bool $military): self
    {
        $this->military = $military;

        return $this;
    }

    public function getEsl(): ?bool
    {
        return $this->esl;
    }

    public function setEsl(?bool $esl): self
    {
        $this->esl = $esl;

        return $this;
    }

    public function getAdvancesClasses(): ?bool
    {
        return $this->advancesClasses;
    }

    public function setAdvancesClasses(?bool $advancesClasses): self
    {
        $this->advancesClasses = $advancesClasses;

        return $this;
    }

    public function getLearningDifferences(): ?bool
    {
        return $this->learningDifferences;
    }

    public function setLearningDifferences(?bool $learningDifferences): self
    {
        $this->learningDifferences = $learningDifferences;

        return $this;
    }

    public function getInterscholasticsports(): ?bool
    {
        return $this->interscholasticsports;
    }

    public function setInterscholasticsports(?bool $interscholasticsports): self
    {
        $this->interscholasticsports = $interscholasticsports;

        return $this;
    }

    public function getExtracurricularActivities(): ?bool
    {
        return $this->extracurricularActivities;
    }

    public function setExtracurricularActivities(?bool $extracurricularActivities): self
    {
        $this->extracurricularActivities = $extracurricularActivities;

        return $this;
    }

    public function getPostGrad(): ?bool
    {
        return $this->postGrad;
    }

    public function setPostGrad(?bool $postGrad): self
    {
        $this->postGrad = $postGrad;

        return $this;
    }

    public function getAirport(): ?string
    {
        return $this->airport;
    }

    public function setAirport(?string $airport): self
    {
        $this->airport = $airport;

        return $this;
    }

    public function getAddAdhd(): ?bool
    {
        return $this->addAdhd;
    }

    public function setAddAdhd(?bool $addAdhd): self
    {
        $this->addAdhd = $addAdhd;

        return $this;
    }

    public function getIb(): ?bool
    {
        return $this->ib;
    }

    public function setIb(?bool $ib): self
    {
        $this->ib = $ib;

        return $this;
    }

    public function getSaturdayClasses(): ?bool
    {
        return $this->saturdayClasses;
    }

    public function setSaturdayClasses(?bool $saturdayClasses): self
    {
        $this->saturdayClasses = $saturdayClasses;

        return $this;
    }

    public function getIGSCE(): ?bool
    {
        return $this->iGSCE;
    }

    public function setIGSCE(?bool $iGSCE): self
    {
        $this->iGSCE = $iGSCE;

        return $this;
    }

    public function getALevel(): ?bool
    {
        return $this->aLevel;
    }

    public function setALevel(?bool $aLevel): self
    {
        $this->aLevel = $aLevel;

        return $this;
    }

    public function getRequiereGuardian(): ?bool
    {
        return $this->requiereGuardian;
    }

    public function setRequiereGuardian(?bool $requiereGuardian): self
    {
        $this->requiereGuardian = $requiereGuardian;

        return $this;
    }

    public function getApplicationDeadline(): ?bool
    {
        return $this->applicationDeadline;
    }

    public function setApplicationDeadline(?bool $applicationDeadline): self
    {
        $this->applicationDeadline = $applicationDeadline;

        return $this;
    }

    public function getRollingAdmission(): ?bool
    {
        return $this->rollingAdmission;
    }

    public function setRollingAdmission(?bool $rollingAdmission): self
    {
        $this->rollingAdmission = $rollingAdmission;

        return $this;
    }

    public function getTotalMexicans(): ?int
    {
        return $this->totalMexicans;
    }

    public function setTotalMexicans(int $totalMexicans): self
    {
        $this->totalMexicans = $totalMexicans;

        return $this;
    }

    public function getClosestCity(): ?string
    {
        return $this->closestCity;
    }

    public function setClosestCity(?string $closestCity): self
    {
        $this->closestCity = $closestCity;

        return $this;
    }
}
