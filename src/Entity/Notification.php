<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Notifications
 *
 * @ORM\Table(name="notification")
 * @ORM\Entity
 * @ApiResource(
 *     collectionOperations={
 *     "get"
 * },
 *     itemOperations={
 *     "get"
 *     },
 *     normalizationContext={"groups"={"read"}},
 *     denormalizationContext={"groups"={"write"}},
 * )
 */
class Notification
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"read"})
     */
    private $id;

    /**
     * @ORM\Column(name="title", type="string", length=45, nullable=true)
     * @Groups({"read"})
     */
    private $title;

    /**
     * @ORM\Column(name="body", type="string", length=255, nullable=true)
     * @Groups({"read"})
     */
    private $body;

    /**
     * @ORM\Column(name="type", type="integer", nullable=true)
     * @Groups({"read"})
     */
    private $type;

    /**
     * @ORM\Column(name="shipping_date", type="datetime", nullable=true)
     * @Groups({"read"})
     */
    private $shippingDate;

    /**
     * @ORM\Column(name="status_id", type="integer", nullable=true, options={"default"="1"})
     * @Groups({"read"})
     */
    private $statusId = '1';

    

    /**
     *
     * @ORM\Column(name="created", type="datetime", nullable=false)
     * @Gedmo\Timestampable(on="create")
     * @Groups({"read"})
     */
    private $created;


    /**
     * @ORM\ManyToMany(targetEntity="User", mappedBy="notification")
     * @Groups({"read"})
     */
    private $user;

    /**
     * @ORM\ManyToMany(targetEntity="Candidate", mappedBy="notification")
     */
    private $candidate;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->user = new ArrayCollection();
        $this->candidate = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(?string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getBody(): ?string
    {
        return $this->body;
    }

    public function setBody(?string $body): self
    {
        $this->body = $body;

        return $this;
    }

    public function getType(): ?int
    {
        return $this->type;
    }

    public function setType(?int $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getShippingDate(): ?\DateTimeInterface
    {
        return $this->shippingDate;
    }

    public function setShippingDate(?\DateTimeInterface $shippingDate): self
    {
        $this->shippingDate = $shippingDate;

        return $this;
    }

    public function getStatusId(): ?int
    {
        return $this->statusId;
    }

    public function setStatusId(?int $statusId): self
    {
        $this->statusId = $statusId;

        return $this;
    }


    public function getCreated(): ?\DateTimeInterface
    {
        return $this->created;
    }

    public function setCreated(\DateTimeInterface $created): self
    {
        $this->created = $created;

        return $this;
    }

    /**
     * @return Collection|User[]
     */
    public function getUser(): Collection
    {
        return $this->user;
    }

    public function addUser(User $user): self
    {
        if (!$this->user->contains($user)) {
            $this->user[] = $user;
            $user->addNotification($this);
        }

        return $this;
    }

    public function removeUser(User $user): self
    {
        if ($this->user->contains($user)) {
            $this->user->removeElement($user);
            $user->removeNotification($this);
        }

        return $this;
    }

    /**
     * @return Collection|Candidate[]
     */
    public function getCandidate(): Collection
    {
        return $this->candidate;
    }

    public function addCandidate(Candidate $candidate): self
    {
        if (!$this->candidate->contains($candidate)) {
            $this->candidate[] = $candidate;
            $candidate->addNotification($this);
        }

        return $this;
    }

    public function removeCandidate(Candidate $candidate): self
    {
        if ($this->candidate->contains($candidate)) {
            $this->candidate->removeElement($candidate);
            $candidate->removeNotification($this);
        }

        return $this;
    }

    public function __toString()
    {
        return sprintf('%s',$this->getTitle());
    }
}
