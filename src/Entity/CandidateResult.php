<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CandidateResultRepository")
 * @ApiResource(
 *     collectionOperations={
 *     "get",
 *     "post",
 * },
 *     itemOperations={
 *     "get",
 *     "put",
 *     "delete"
 *     },
 *     normalizationContext={"groups"={"read"}},
 *     denormalizationContext={"groups"={"write"}},
 * )
 */
class CandidateResult
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"read"})
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Candidate", inversedBy="results")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"read","write"})
     */
    private $candidate;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\School", inversedBy="results")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"read","write"})
     */
    private $school;

    /**
     * @ORM\Column(type="boolean")
     * @Groups({"read","write"})
     */
    private $status;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCandidate(): ?Candidate
    {
        return $this->candidate;
    }

    public function setCandidate(?Candidate $candidate): self
    {
        $this->candidate = $candidate;

        return $this;
    }

    public function getSchool(): ?School
    {
        return $this->school;
    }

    public function setSchool(?School $school): self
    {
        $this->school = $school;

        return $this;
    }

    public function getStatus(): ?bool
    {
        return $this->status;
    }

    public function setStatus(bool $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function __toString()
    {
        return sprintf('%s - %s',$this->getSchool()->getName(),$this->getCandidate()->getName());
    }
}
