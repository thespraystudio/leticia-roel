<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CandidateAnswerRepository")
 * @ApiResource(
 *     collectionOperations={
 *     "get",
 *     "post",
 * },
 *     itemOperations={
 *     "get",
 *     "put",
 *     "delete"
 *     },
 *     normalizationContext={"groups"={"read"}},
 *     denormalizationContext={"groups"={"write"}},
 * )
 */
class CandidateAnswer
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"read"})
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Candidate", inversedBy="candidateAnswers")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"read","write"})
     */
    private $candidate;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Category", inversedBy="candidateAnswers")
     * @Groups({"read","write"})
     */
    private $category;

    /**
     * @ORM\Column(type="string", length=100)
     * @Groups({"read","write"})
     */
    private $value;

    use TimestampableEntity;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCandidate(): ?Candidate
    {
        return $this->candidate;
    }

    public function setCandidate(?Candidate $candidate): self
    {
        $this->candidate = $candidate;

        return $this;
    }

    public function getCategory(): ?Category
    {
        return $this->category;
    }

    public function setCategory(?Category $category): self
    {
        $this->category = $category;

        return $this;
    }

    public function getValue(): ?string
    {
        return $this->value;
    }

    public function setValue(string $value): self
    {
        $this->value = $value;

        return $this;
    }

    public function __toString()
    {
        return sprintf('%s  - %s',$this->getCandidate()->getName(),$this->getCategory());
    }
}
