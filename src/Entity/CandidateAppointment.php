<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\Timestampable;
use Symfony\Component\Serializer\Annotation\Groups;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Core\Annotation\ApiFilter;

/**
 * @ApiResource(
 *     normalizationContext={"groups"={"candidate-appointment:read","candidate:read"}},
 *     denormalizationContext={"groups"={"candidate-appointment:write","candidate:write"}},
 * )
 * @ApiFilter(SearchFilter::class, properties={"candidate": "exact","office": "exact"})
 * @ORM\Entity(repositoryClass="App\Repository\CandidateAppointmentRepository")
 */
class CandidateAppointment
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"candidate-appointment:read","candidate:read"})
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Candidate", inversedBy="appointments")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"candidate-appointment:read","candidate-appointment:write","candidate:read","candidate:write"})
     */
    private $candidate;

    /**
     * @ORM\Column(type="date")
     * @Groups({"candidate-appointment:read","candidate-appointment:write","candidate:read","candidate:write"})
     */
    private $date;

    /**
     * @Groups({"candidate-appointment:read","candidate-appointment:write","candidate:read","candidate:write"})
     * @ORM\Column(type="time")
     */
    private $time;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"candidate-appointment:read","candidate-appointment:write","candidate:read","candidate:write"})
     */
    private $url;

    use Timestampable;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Appointment", inversedBy="candidates")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"candidate-appointment:read","candidate-appointment:write","candidate:read","candidate:write"})
     */
    private $appointment;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Office", inversedBy="appointments")
     * @Groups({"candidate-appointment:read","candidate-appointment:write","candidate:read","candidate:write"})
     */
    private $office;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="appointments")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"candidate-appointment:read","candidate-appointment:write"})
     */
    private $operator;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCandidate(): ?Candidate
    {
        return $this->candidate;
    }

    public function setCandidate(?Candidate $candidate): self
    {
        $this->candidate = $candidate;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getTime(): ?\DateTimeInterface
    {
        return $this->time;
    }

    public function setTime(\DateTimeInterface $time): self
    {
        $this->time = $time;

        return $this;
    }

    public function getUrl(): ?string
    {
        return $this->url;
    }

    public function setUrl(?string $url): self
    {
        $this->url = $url;

        return $this;
    }

    public function getAppointment(): ?Appointment
    {
        return $this->appointment;
    }

    public function setAppointment(?Appointment $appointment): self
    {
        $this->appointment = $appointment;

        return $this;
    }

    public function getOffice(): ?Office
    {
        return $this->office;
    }

    public function setOffice(?Office $office): self
    {
        $this->office = $office;

        return $this;
    }
    public function __toString()
    {
        return sprintf('%s %s',$this->getCandidate()->getName(),$this->getDate()->format('d-m-Y'));
    }

    public function getOperator(): ?User
    {
        return $this->operator;
    }

    public function setOperator(?User $operator): self
    {
        $this->operator = $operator;

        return $this;
    }
}
