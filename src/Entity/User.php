<?php

namespace App\Entity;

use App\Repository\UserRepository;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use App\Controller\Api\UserController;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\SerializedName;
use Symfony\Component\Validator\Constraints as Assert;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;

/**
 * User
 *
 * @ORM\Table(name="user")
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Entity
 * @ApiResource(
 *     collectionOperations={
 *     "post" = { "controller"=UserController::class },
 *      "get" = {}
 * },
 *     itemOperations={
 *     "get"= {
 *     },
 *     "put" = {
 *     "controller"=UserController::class,
 *     },
 *     },
 *     normalizationContext={"groups"={"user:read","read","candidate:read","candidate-appointment:read"}},
 *     denormalizationContext={"groups"={"write","candidate:write","candidate-appointment:write"}},
 * )
 * @Vich\Uploadable
 * @ApiFilter(SearchFilter::class, properties={"roles":"partial"})
 * @UniqueEntity(fields={"email"}, message="Ya existe este usuario")
 */
class User implements UserInterface
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"read","user:read","candidate:read","candidate-appointment:read"})
     */
    private $id;

    /**
     * @ORM\Column(name="username", type="string", length=100, nullable=false,unique=true)
     * @Groups({"user:read","candidate:read","candidate-appointment:read"})
     */
    private $username;

    /**
     * @ORM\Column(name="name", type="string", length=100, nullable=false)
     * @Groups({"read","user:read","write"})
     */
    private $name;

    /**
     * @ORM\Column(name="lastname", type="string", length=100, nullable=false)
     * @Groups({"read","user:read","write"})
     */
    private $lastname;

    /**
     * @ORM\Column(name="email", type="string", length=100, nullable=false, unique=true)
     * @Groups({"read","write","candidate:read"})
     * @Assert\Email()
     */
    private $email;

    /**
     * @ORM\Column(name="phone_number", type="string", length=20, nullable=false)
     * @Groups({"read","write"})
     */
    private $phoneNumber;

    /**
     * @ORM\Column(name="password", type="string", length=255, nullable=false)
     */
    private $password;

    /**
     * @Groups({"write"})
     * @SerializedName("password")
     * @Assert\NotBlank(groups="create")
     */
    private $plainPassword;

    /**
     * @ORM\Column(type="text",nullable=true)
     * @Groups({"read","write"})
     * @SerializedName("file")
     * @var string
     */
    private $image;

    /**
     * @Vich\UploadableField(mapping="media", fileNameProperty="image")
     * @var File
     */
    private $imageFile;

    /**
     *
     * @ORM\Column(name="created", type="datetime", nullable=false)
     * @Gedmo\Timestampable(on="create")
     */
    private $created;

    /**
     *
     * @ORM\Column(name="updated", type="datetime", nullable=false)
     * @Gedmo\Timestampable(on="update")
     */
    private $updated;

    /**
     * @ORM\Column(name="player_id", type="string", length=100, nullable=true)
     * @Groups({"read","write"})
     */
    private $playerId;

    /**
     * @ORM\ManyToMany(targetEntity="Notification", inversedBy="user")
     * @ORM\JoinTable(name="log_notification",
     *   joinColumns={
     *     @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="notification_id", referencedColumnName="id")
     *   }
     * )
     */
    private $notification;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Candidate", mappedBy="operators")
     */
    private $operatorCandidates;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Candidate", mappedBy="owner", cascade={"persist"})
     * @Groups({"read","write","user:read"})
     */
    private $candidates;

    /**
     * @ORM\Column(type="json")
     * @Groups({"read","write"})
     */
    private $roles = [];

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\SchoolComment", mappedBy="user", orphanRemoval=true)
     */
    private $comments;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\CandidateAppointment", mappedBy="operator", orphanRemoval=true)
     */
    private $appointments;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $birthday;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $status;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->notification = new ArrayCollection();
        $this->operatorCandidates = new ArrayCollection();
        $this->comments = new ArrayCollection();
        $this->appointments = new ArrayCollection();
        $this->candidates = new ArrayCollection();
        $this->roles = array('ROLE_USER');
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUsername(): ?string
    {
        return $this->username;
    }

    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getLastname(): ?string
    {
        return $this->lastname;
    }

    public function setLastname(string $lastname): self
    {
        $this->lastname = $lastname;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getPhoneNumber(): ?string
    {
        return $this->phoneNumber;
    }

    public function setPhoneNumber(string $phoneNumber): self
    {
        $this->phoneNumber = $phoneNumber;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage(?string $image): self
    {
        $this->image = $image;

        return $this;
    }

    public function getCreated(): ?\DateTimeInterface
    {
        return $this->created;
    }

    public function setCreated(\DateTimeInterface $created): self
    {
        $this->created = $created;

        return $this;
    }

    public function getUpdated(): ?\DateTimeInterface
    {
        return $this->updated;
    }

    public function setUpdated(\DateTimeInterface $updated): self
    {
        $this->updated = $updated;

        return $this;
    }

    public function getPlayerId(): ?string
    {
        return $this->playerId;
    }

    public function setPlayerId(?string $playerId): self
    {
        $this->playerId = $playerId;

        return $this;
    }

    /**
     * @return Collection|Notification[]
     */
    public function getNotification(): Collection
    {
        return $this->notification;
    }

    public function addNotification(Notification $notification): self
    {
        if (!$this->notification->contains($notification)) {
            $this->notification[] = $notification;
        }

        return $this;
    }

    public function removeNotification(Notification $notification): self
    {
        if ($this->notification->contains($notification)) {
            $this->notification->removeElement($notification);
        }

        return $this;
    }

    public function __toString()
    {
        return $this->getName();
    }


    public function setImageFile(File $image = null)
    {
        $this->imageFile = $image;

        if ($image) {
            $this->updated = new \DateTime('now');
        }
    }

    public function getImageFile()
    {
        return $this->imageFile;
    }


    /**
     * @return Collection|Candidate[]
     */
    public function getOperatorCandidates(): Collection
    {
        return $this->operatorCandidates;
    }

    public function addOperatorCandidate(Candidate $operatorCandidate): self
    {
        if (!$this->operatorCandidates->contains($operatorCandidate)) {
            $this->operatorCandidates[] = $operatorCandidate;
            $operatorCandidate->addOperator($this);
        }

        return $this;
    }

    public function removeOperatorCandidate(Candidate $operatorCandidate): self
    {
        if ($this->operatorCandidates->contains($operatorCandidate)) {
            $this->operatorCandidates->removeElement($operatorCandidate);
            $operatorCandidate->removeOperator($this);
        }

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        $roles[] = 'ROLE_USER';
        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getSalt()
    {
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        $this->plainPassword = null;
    }

    /**
     * @ORM\PrePersist
     */
    public function prePersist()
    {
        $this->username = $this->email;
    }

    /**
     * @ORM\PreUpdate()
     */
    public function preUpdate()
    {
        $this->username = $this->email;
    }

    public function getPlainPassword(): ?string
    {
        return $this->plainPassword;
    }

    public function setPlainPassword($plainPassword): self
    {
        $this->plainPassword = $plainPassword;
        return $this;
    }

    /**
     * @return Collection|SchoolComment[]
     */
    public function getComments(): Collection
    {
        return $this->comments;
    }

    public function addComment(SchoolComment $comment): self
    {
        if (!$this->comments->contains($comment)) {
            $this->comments[] = $comment;
            $comment->setUser($this);
        }

        return $this;
    }

    public function removeComment(SchoolComment $comment): self
    {
        if ($this->comments->contains($comment)) {
            $this->comments->removeElement($comment);
            // set the owning side to null (unless already changed)
            if ($comment->getUser() === $this) {
                $comment->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|CandidateAppointment[]
     */
    public function getAppointments(): Collection
    {
        return $this->appointments;
    }

    public function addAppointment(CandidateAppointment $appointment): self
    {
        if (!$this->appointments->contains($appointment)) {
            $this->appointments[] = $appointment;
            $appointment->setOperator($this);
        }

        return $this;
    }

    public function removeAppointment(CandidateAppointment $appointment): self
    {
        if ($this->appointments->contains($appointment)) {
            $this->appointments->removeElement($appointment);
            // set the owning side to null (unless already changed)
            if ($appointment->getOperator() === $this) {
                $appointment->setOperator(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Candidate[]
     */
    public function getCandidates(): Collection
    {
        return $this->candidates;
    }

    public function addCandidate(Candidate $candidate): self
    {
        if (!$this->candidates->contains($candidate)) {
            $this->candidates[] = $candidate;
            $candidate->setOwner($this);
        }

        return $this;
    }

    public function removeCandidate(Candidate $candidate): self
    {
        if ($this->candidates->contains($candidate)) {
            $this->candidates->removeElement($candidate);
            // set the owning side to null (unless already changed)
            if ($candidate->getOwner() === $this) {
                $candidate->setOwner(null);
            }
        }

        return $this;
    }

    public function getBirthday(): ?\DateTimeInterface
    {
        return $this->birthday;
    }

    public function setBirthday(?\DateTimeInterface $birthday): self
    {
        $this->birthday = $birthday;

        return $this;
    }

    public function getStatus(): ?int
    {
        return $this->status;
    }

    public function setStatus(?int $status): self
    {
        $this->status = $status;

        return $this;
    }
}
