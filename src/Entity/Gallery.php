<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\Serializer\Annotation\SerializedName;

/**
 * Gallery
 *
 * @ORM\Table(name="gallery")
 * @ORM\Entity
 * @ApiResource(
 *     collectionOperations={
 *     "get"
 * },
 *     itemOperations={
 *     "get"
 *     },
 *     normalizationContext={"groups"={"read"}},
 *     denormalizationContext={"groups"={"write"}},
 * )
 * @Vich\Uploadable
 */
class Gallery
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"read","candidate:read"})
     */
    private $id;

    /**
     * @ORM\Column(name="name", type="string", length=100, nullable=false)
     * @Groups({"read","candidate:read"})
     */
    private $name;

    /**
     * @ORM\Column(type="text",nullable=true)
     * @Groups({"read","school:write","school:read","candidate:read"})
     * @SerializedName("file")
     * @var string
     */
    private $file;
 
     /**
     * @Assert\File(
     *     mimeTypes = {"image/png", "image/jpg", "image/jpeg"},
     *     mimeTypesMessage = "Formatos validos JPEG, JPG y PNG"
     * )
     * @Vich\UploadableField(mapping="media", fileNameProperty="file")
     * @var File
     */
    private $imageFile;

    /**
     * @ORM\Column(name="type", type="string", length=50)
     * @Groups({"read","candidate:read"})
     */
    private $type=0;

    /**
     * @ORM\Column(name="date_video", type="date", nullable=true)
     * @Groups({"read"})
     */
    private $dateVideo;

    /**
     * @ORM\Column(name="duration", type="string", length=45, nullable=true)
     * @Groups({"read"})
     */
    private $duration;

    /**
     *
     * @ORM\Column(name="created", type="datetime", nullable=false)
     * @Gedmo\Timestampable(on="create")
     * @Groups({"read"})
     */
    private $created;

    /**
     *
     * @ORM\Column(name="updated", type="datetime", nullable=false)
     * @Gedmo\Timestampable(on="update")
     * @Groups({"read"})
     */
    private $updated;

    /**
     * @ORM\ManyToMany(targetEntity="School", mappedBy="gallery")
     */
    private $school;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Groups({"read","candidate:read"})
     */
    private $url;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->school = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getFile(): ?string
    {
        return $this->file;
    }

    public function setFile(?string $file): self
    {
        $this->file = $file;
    
        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getDuration(): ?string
    {
        return $this->duration;
    }

    public function setDuration(?string $duration): self
    {
        $this->duration = $duration;

        return $this;
    }

    public function getCreated(): ?\DateTimeInterface
    {
        return $this->created;
    }

    public function setCreated(\DateTimeInterface $created): self
    {
        $this->created = $created;

        return $this;
    }

    public function getUpdated(): ?\DateTimeInterface
    {
        return $this->updated;
    }

    public function setUpdated(\DateTimeInterface $updated): self
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * @return Collection|School[]
     */
    public function getSchool(): Collection
    {
        return $this->school;
    }

    public function addSchool(School $school): self
    {
        if (!$this->school->contains($school)) {
            $this->school[] = $school;
            $school->addGallery($this);
        }

        return $this;
    }

    public function removeSchool(School $school): self
    {
        if ($this->school->contains($school)) {
            $this->school->removeElement($school);
            $school->removeGallery($this);
        }

        return $this;
    }

    public function setImageFile(File $file = null)
    {
        $this->imageFile = $file;

        if ($file) {
            $this->updated = new \DateTime('now');
        }
    }

    public function getImageFile()
    {
        return $this->imageFile;
    }

    public function getDateVideo(): ?\DateTimeInterface
    {
        return $this->dateVideo;
    }

    public function setDateVideo(?\DateTimeInterface $dateVideo): self
    {
        $this->dateVideo = $dateVideo;

        return $this;
    }

    public function __toString()
    {
      return $this->getName();
    }

    public function getUrl(): ?string
    {
        return $this->url;
    }

    public function setUrl(?string $url): self
    {
        $this->url = $url;

        return $this;
    }

}
