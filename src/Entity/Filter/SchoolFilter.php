<?php

namespace App\Entity\Filter;

use App\Entity\Activity;
use App\Entity\Country;
use App\Entity\Level;
use App\Entity\SchoolSkill;
use Doctrine\Common\Collections\ArrayCollection;

class SchoolFilter
{

    protected ?ArrayCollection $country = null;
    protected ?int $capacity = null;
    protected ?int $costMin = null;
    protected ?string $costMax = null;
    protected ?bool $military = null;
    protected ?bool $religion = null;
    //protected ?string $dresscode = null;
    protected ?bool $english = null;
    protected ?ArrayCollection $activities = null;
    protected ?ArrayCollection $levels = null;
    protected ?ArrayCollection $skills = null;
    protected ?Array $typeSchool = null;
    protected ?string $dresscode = null;
    protected ?Array $others = null;
    protected ?ArrayCollection $sport = null;

    /**
     * @return ArrayCollection<Country>|null
     */
    public function getCountry(): ?ArrayCollection
    {
        return $this->country;
    }

    /**
     * @param ArrayCollection<Country> $country
     */
    public function setCountry(?ArrayCollection $country): void
    {
        $this->country = $country;
    }

    /**
     * @return ArrayCollection<Sport>|null
     */
    public function getSports(): ?ArrayCollection
    {
        return $this->sport;
    }

    /**
     * @param ArrayCollection<Sport> $Sport
     */
    public function setSports(?ArrayCollection $sport): void
    {
        $this->sport = $sport;
    }

    /**
     * @return int|null
     */
    public function getCapacity(): ?int
    {
        return $this->capacity;
    }

    /**
     * @param int $capacity
     */
    public function setCapacity(?int $capacity): void
    {
        $this->capacity = $capacity;
    }

    /**
     * @return ArrayCollection<Activity>|null
     */
    public function getActivities(): ?ArrayCollection
    {
        return $this->activities;
    }

    /**
     * @param ArrayCollection<Activity> $activities
     */
    public function setActivities(?ArrayCollection $activities): void
    {
        $this->activities = $activities;
    }

    /**
     * @return ArrayCollection<Level>|null
     */
    public function getLevels(): ?ArrayCollection
    {
        return $this->levels;
    }

    /**
     * @param ArrayCollection<Level>|null $levels
     */
    public function setLevels(?ArrayCollection $levels): void
    {
        $this->levels = $levels;
    }

    /**
     * @return ArrayCollection<SchoolSkill>|null
     */
    public function getSkills(): ?ArrayCollection
    {
        return $this->skills;
    }

    /**
     * @param ArrayCollection<SchoolSkill>|null $skills
     */
    public function setSkills(?ArrayCollection $skills): void
    {
        $this->skills = $skills;
    }

    /**
     * @return bool
     */
    public function getMilitary(): ?bool
    {
        return $this->military;
    }

    /**
     * @param bool  $military
     */
    public function setMilitary(?bool $military): void
    {
        $this->military = $military;
    }

    /**
     * @return bool
     */
    public function getReligion(): ?bool
    {
        return $this->religion;
    }

    /**
     * @param bool $religion
     */
    public function setReligion(?bool $religion): void
    {
        $this->religion = $religion;
    }

    /**
     * @return string
     */
    public function getDresscode(): ?string
    {
        return $this->dresscode;
    }

    /**
     * @param string $dresscode
     */
    public function setDresscode(?string $dresscode): void
    {
        $this->dresscode = $dresscode;
    }
   

    /**
     * @return bool
     */
    public function getEnglish(): ?bool
    {
        return $this->english;
    }

    /**
     * @param bool $english
     */
    public function setEnglish(?bool $english): void
    {
        $this->english = $english;
    }

    /**
     * @return int|null
     */
    public function getCostMin(): ?int
    {
        return $this->costMin;
    }

    /**
     * @param int $costMin
     */
    public function setCostMin(?int $costMin): void
    {
        $this->costMin = $costMin;
    }

    /**
     * @return string|null
     */
    public function getCostMax(): ?string
    {
        return $this->costMax;
    }

    /**
     * @param string $costMax
     */
    public function setCostMax(?string $costMax): void
    {
        $this->costMax = $costMax;
    }

    /**
     * @return Array|null
     */
    public function getOthers(): ?Array
    {
        return $this->others;
    }

    /**
     * @param Array|null $others
     */
    public function setOthers(?Array $others): void
    {
        $this->others = $others;
    }

    /**
     * @param Array|null
     */
    public function getTypeSchool(): ?Array
    {
        return $this->typeSchool;
    }

    /**
     * @param Array|null $schoolsType
     */
    public function setTypeSchool(?Array $typeSchool): void
    {
        $this->typeSchool = $typeSchool;
    }
}