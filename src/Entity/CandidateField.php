<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Behavior\Timestampable;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Core\Annotation\ApiFilter;

/**
 * @ApiResource(
 *  normalizationContext={"groups"={"candidate-field:read"}},
 *  denormalizationContext={"groups"={"candidate-field:write"}},
 * )
 * @ORM\Entity(repositoryClass="App\Repository\CandidateFieldRepository")
 * @ApiFilter(SearchFilter::class, properties={"candidate": "exact"})
 */
class CandidateField
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Candidate", inversedBy="fields", cascade={"persist"})
     * @ORM\JoinColumn(nullable=true)
     * @Groups({"candidate-field:read","candidate-field:write","candidate:write","candidate:read"})
     */
    private $candidate;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\ProcessField", inversedBy="candidateFields", cascade={"persist","remove"})
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"candidate-field:read","candidate-field:write","candidate:write","candidate:read"})
     */
    private $field;

    use Timestampable;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"candidate-field:read","candidate-field:write","candidate:write","candidate:read"})
     */
    private $answer;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $accept;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCandidate(): ?Candidate
    {
        return $this->candidate;
    }

    public function setCandidate(?Candidate $candidate): self
    {
        $this->candidate = $candidate;

        return $this;
    }

    public function getField(): ?ProcessField
    {
        return $this->field;
    }

    public function setField(?ProcessField $field): self
    {
        $this->field = $field;

        return $this;
    }

    public function getAnswer(): ?string
    {
        return $this->answer;
    }

    public function setAnswer(?string $answer): self
    {
        $this->answer = $answer;

        return $this;
    }

    public function __toString()
    {
        return sprintf('%s - %s', $this->getCandidate()->getName(), $this->getField()->getName());
    }

    public function getAccept(): ?bool
    {
        return $this->accept;
    }

    public function setAccept(?bool $accept): self
    {
        $this->accept = $accept;

        return $this;
    }
}
