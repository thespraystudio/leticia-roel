<?php

namespace App\Entity;

use App\Repository\CandidateRepository;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use App\Behavior\Timestampable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\SerializedName;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use App\Controller\Api\CandidateController;
use App\Controller\Api\CandidatePutController;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Candidates
 *
 * @ORM\Table(name="candidate")
 * @Vich\Uploadable
 * @ORM\Entity
 * @ApiFilter(SearchFilter::class, properties={
 *  "operators": "exact"
 *     })
 * @ApiResource(
 *     collectionOperations={
 *     "get",
 *     "post" = { "controller"=CandidateController::class },
 * },
 *     itemOperations={
 *     "get",
 *     "put" = { "controller"=CandidatePutController::class },
 *     "delete"
 *     },
 *     normalizationContext={"groups"={"user:read","candidate:read","candidate-appointment:read","candidate-field:read"}},
 *     denormalizationContext={"groups"={"candidate:write","candidate-appointment:write","candidate-field:wirte"}},
 * )
 */
class Candidate
{

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"candidate:read","user:read","candidate-appointment:read","candidate-field:read","read"})
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="name", type="string", length=100, nullable=true)
     * @Groups({"candidate:read","user:read","candidate:write","candidate-appointment:read","candidate-field:read","read"})
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="lastname", type="string", length=100, nullable=true)
     * @Groups({"candidate:read","user:read","candidate:write","candidate-appointment:read","read"})
     */
    private $lastname;

    /**
     * @var string|null
     *
     * @ORM\Column(type="date", nullable=true)
     * @Groups({"candidate:read","candidate:write","read","write"})
     */
    private $birthday;

    /**
     * @var string|null
     *
     * @ORM\Column(name="street", type="string", length=150, nullable=true)
     * @Groups({"candidate:read","candidate:write","read"})
     */
    private $street;



    /**
     * @var string|null
     *
     * @ORM\Column(name="in_number", type="string", length=10, nullable=true)
     * @Groups({"candidate:read","candidate:write","read"})
     */
    private $inNumber;

    /**
     * @var string|null
     *
     * @ORM\Column(name="out_number", type="string", length=10, nullable=true)
     * @Groups({"candidate:read","candidate:write","read"})
     */
    private $outNumber;

    /**
     * @var string
     *
     * @ORM\Column(name="phone_number", type="string", length=20, nullable=true)
     * @Groups({"candidate:read","candidate:write","read"})
     */
    private $phoneNumber;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255, nullable=true)
     * @Groups({"candidate:read","candidate:write","read"})
     */
    private $email;

    /**
     * @var string|null
     *
     * @ORM\Column(name="current_school", type="string", length=150, nullable=true)
     * @Groups({"candidate:read","candidate:write","read"})
     */
    private $currentSchool;

    /**
     * @var string|null
     *
     * @ORM\Column(name="current_grade", type="string", length=150, nullable=true)
     * @Groups({"candidate:read","candidate:write","read"})
     */
    private $currentGrade;

    /**
     *
     *
     * /**
     * @var int|null
     *
     * @ORM\Column(name="status", type="integer", options={"default": 1})
     * @Groups({"candidate:read","candidate:write","read"})
     */
    private $status = 1;

    use Timestampable;

    /**
     * @ORM\ManyToMany(targetEntity="School", inversedBy="canditates")
     * @Groups({"candidate:read","candidate:write"})
     */
    private $school;

    /**
     * @ORM\ManyToMany(targetEntity="Notification", inversedBy="candidate")
     * @Groups({"candidate:read","candidate:write"})
     */
    private $notification;

    /**
     * @ORM\ManyToOne(targetEntity="State", inversedBy="candidates")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"candidate:read","candidate:write","read","write","read"})
     */
    private $state;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Candidate", cascade={"persist"})
     * @Groups({"candidate:read","candidate:write"})
     */
    private $candidate;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\CandidateAppointment", mappedBy="candidate", orphanRemoval=true)
     * @Groups({"candidate:read","candidate:write"})
     */
    private $appointments;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\CandidateResult", mappedBy="candidate", orphanRemoval=true)
     */
    private $results;

    /**
     * @ORM\OneToMany(targetEntity="CandidateDocument", mappedBy="candidate", orphanRemoval=true, cascade={"persist"})
     * @Groups({"read","write","candidate:read"})
     */
    private $documents;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\CandidateAnswer", mappedBy="candidate", orphanRemoval=true)
     */
    private $candidateAnswers;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="candidates", cascade={"persist"})
     * @Groups({"candidate:write","candidate:read"})
     */
    private $owner;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\User", inversedBy="operatorCandidates")
     * @Groups({"candidate:write","candidate:read"})
     */
    private $operators;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\CandidateField", mappedBy="candidate", orphanRemoval=true, cascade={"persist"})
     * @Groups({"candidate:write","candidate:read"})
     */
    private $fields;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Process", mappedBy="candidate")
     * @Groups({"candidate:read"})
     */
    private $processes;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ProcessResult", mappedBy="candidate")
     */
    private $processResults;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Stage", mappedBy="candidate")
     * @Groups({"candidate:read"})
     */
    private $stages;

    /**
     * @ORM\Column(type="text",nullable=true)
     * @Groups({"user:read","candidate:write","candidate:read"})
     * @SerializedName("file")
     * @Groups({"read"})
     * @var string
     */
    private $image;

    /**
     * @Assert\File(
     *     maxSize = "6072k",
     *     mimeTypes = {"image/png", "image/jpg", "image/jpeg", "image/jpg"},
     *     mimeTypesMessage = "Formatos validos JPEG, JPG y PNG"
     * )
     * @Vich\UploadableField(mapping="media", fileNameProperty="image")
     * @var File
     */
    private $imageFile;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Payment", mappedBy="candidate")
     */
    private $payments;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     * @Groups({"candidate:write","candidate:read","read","write"})
     */
    private $gender;


    /**
     * @ORM\Column(type="integer")
     * @Groups({"candidate:write","candidate:read","read","write"})
     */
    private $type = 1;


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->school = new ArrayCollection();
        $this->notification = new ArrayCollection();
        $this->appointments = new ArrayCollection();
        $this->results = new ArrayCollection();
        $this->documents = new ArrayCollection();
        $this->candidateAnswers = new ArrayCollection();
        $this->operators = new ArrayCollection();
        $this->fields = new ArrayCollection();
        $this->processes = new ArrayCollection();
        $this->processResults = new ArrayCollection();
        $this->stages = new ArrayCollection();
        $this->payments = new ArrayCollection();
    }

    public function getState(): ?State
    {
        return $this->state;
    }

    public function setState(?State $state): self
    {
        $this->state = $state;

        return $this;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getLastname(): ?string
    {
        return $this->lastname;
    }

    public function setLastname(string $lastname): self
    {
        $this->lastname = $lastname;

        return $this;
    }

    public function getBirthday(): ?\DateTimeInterface
    {
        return $this->birthday;
    }

    public function setBirthday(?\DateTimeInterface $birthday): self
    {
        $this->birthday = $birthday;

        return $this;
    }

    public function getStreet(): ?string
    {
        return $this->street;
    }

    public function setStreet(?string $street): self
    {
        $this->street = $street;

        return $this;
    }

    public function getInNumber(): ?string
    {
        return $this->inNumber;
    }

    public function setInNumber(?string $inNumber): self
    {
        $this->inNumber = $inNumber;

        return $this;
    }

    public function getOutNumber(): ?string
    {
        return $this->outNumber;
    }

    public function setOutNumber(?string $outNumber): self
    {
        $this->outNumber = $outNumber;

        return $this;
    }

    public function getPhoneNumber(): ?string
    {
        return $this->phoneNumber;
    }

    public function setPhoneNumber(string $phoneNumber): self
    {
        $this->phoneNumber = $phoneNumber;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getStatus(): ?int
    {
        return $this->status;
    }

    public function setStatus(?int $status): self
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return Collection|School[]
     */
    public function getSchool(): Collection
    {
        return $this->school;
    }

    public function addSchool(School $school): self
    {
        if (!$this->school->contains($school)) {
            $this->school[] = $school;
        }

        return $this;
    }

    public function removeSchool(School $school): self
    {
        if ($this->school->contains($school)) {
            $this->school->removeElement($school);
        }

        return $this;
    }

    /**
     * @return Collection|Notification[]
     */
    public function getNotification(): Collection
    {
        return $this->notification;
    }

    public function addNotification(Notification $notification): self
    {
        if (!$this->notification->contains($notification)) {
            $this->notification[] = $notification;
        }

        return $this;
    }

    public function removeNotification(Notification $notification): self
    {
        if ($this->notification->contains($notification)) {
            $this->notification->removeElement($notification);
        }

        return $this;
    }

    public function getCandidate(): ?self
    {
        return $this->candidate;
    }

    public function setCandidate(?self $candidate): self
    {
        $this->candidate = $candidate;

        return $this;
    }

    public function __toString()
    {
        if (is_null($this->getName())) {
            return 'NULL';
        }
        return sprintf('%s %s', $this->getName(), $this->getLastname());
    }

    /**
     * @return Collection|CandidateAppointment[]
     */
    public function getAppointments(): Collection
    {
        return $this->appointments;
    }

    public function addAppointment(CandidateAppointment $appointment): self
    {
        if (!$this->appointments->contains($appointment)) {
            $this->appointments[] = $appointment;
            $appointment->setCandidate($this);
        }

        return $this;
    }

    public function removeAppointment(CandidateAppointment $appointment): self
    {
        if ($this->appointments->contains($appointment)) {
            $this->appointments->removeElement($appointment);
            // set the owning side to null (unless already changed)
            if ($appointment->getCandidate() === $this) {
                $appointment->setCandidate(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|CandidateResult[]
     */
    public function getResults(): Collection
    {
        return $this->results;
    }

    public function addResult(CandidateResult $result): self
    {
        if (!$this->results->contains($result)) {
            $this->results[] = $result;
            $result->setCandidate($this);
        }

        return $this;
    }

    public function removeResult(CandidateResult $result): self
    {
        if ($this->results->contains($result)) {
            $this->results->removeElement($result);
            // set the owning side to null (unless already changed)
            if ($result->getCandidate() === $this) {
                $result->setCandidate(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|CandidateDocument[]
     */
    public function getDocuments(): Collection
    {
        return $this->documents;
    }

    public function addDocument(CandidateDocument $document): self
    {
        if (!$this->documents->contains($document)) {
            $this->documents[] = $document;
            $document->setCandidate($this);
        }

        return $this;
    }

    public function removeDocument(CandidateDocument $document): self
    {
        if ($this->documents->contains($document)) {
            $this->documents->removeElement($document);
            // set the owning side to null (unless already changed)
            if ($document->getCandidate() === $this) {
                $document->setCandidate(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|CandidateAnswer[]
     */
    public function getCandidateAnswers(): Collection
    {
        return $this->candidateAnswers;
    }

    public function addCandidateAnswer(CandidateAnswer $candidateAnswer): self
    {
        if (!$this->candidateAnswers->contains($candidateAnswer)) {
            $this->candidateAnswers[] = $candidateAnswer;
            $candidateAnswer->setCandidate($this);
        }

        return $this;
    }

    public function removeCandidateAnswer(CandidateAnswer $candidateAnswer): self
    {
        if ($this->candidateAnswers->contains($candidateAnswer)) {
            $this->candidateAnswers->removeElement($candidateAnswer);
            // set the owning side to null (unless already changed)
            if ($candidateAnswer->getCandidate() === $this) {
                $candidateAnswer->setCandidate(null);
            }
        }

        return $this;
    }

    public function getOwner(): ?User
    {
        return $this->owner;
    }

    public function setOwner(?User $owner): self
    {
        $this->owner = $owner;

        return $this;
    }

    /**
     * @return Collection|User[]
     */
    public function getOperators(): Collection
    {
        return $this->operators;
    }

    public function addOperator(User $operator): self
    {
        if (!$this->operators->contains($operator)) {
            $this->operators[] = $operator;
        }

        return $this;
    }

    public function removeOperator(User $operator): self
    {
        if ($this->operators->contains($operator)) {
            $this->operators->removeElement($operator);
        }

        return $this;
    }

    /**
     * @return Collection|CandidateField[]
     */
    public function getFields(): Collection
    {
        return $this->fields;
    }

    public function addField(CandidateField $field): self
    {
        if (!$this->fields->contains($field)) {
            $this->fields[] = $field;
            $field->setCandidate($this);
        }

        return $this;
    }

    public function removeField(CandidateField $field): self
    {
        if ($this->fields->contains($field)) {
            $this->fields->removeElement($field);
            // set the owning side to null (unless already changed)
            if ($field->getCandidate() === $this) {
                $field->setCandidate(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Process[]
     */
    public function getProcesses(): Collection
    {
        return $this->processes;
    }

    public function addProcess(Process $process): self
    {
        if (!$this->processes->contains($process)) {
            $this->processes[] = $process;
            $process->setCandidate($this);
        }

        return $this;
    }

    public function removeProcess(Process $process): self
    {
        if ($this->processes->contains($process)) {
            $this->processes->removeElement($process);
            // set the owning side to null (unless already changed)
            if ($process->getCandidate() === $this) {
                $process->setCandidate(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|ProcessResult[]
     */
    public function getProcessResults(): Collection
    {
        return $this->processResults;
    }

    public function addProcessResult(ProcessResult $processResult): self
    {
        if (!$this->processResults->contains($processResult)) {
            $this->processResults[] = $processResult;
            $processResult->setCandidate($this);
        }

        return $this;
    }

    public function removeProcessResult(ProcessResult $processResult): self
    {
        if ($this->processResults->contains($processResult)) {
            $this->processResults->removeElement($processResult);
            // set the owning side to null (unless already changed)
            if ($processResult->getCandidate() === $this) {
                $processResult->setCandidate(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Stage[]
     */
    public function getStages(): Collection
    {
        return $this->stages;
    }

    public function addStage(Stage $stage): self
    {
        if (!$this->stages->contains($stage)) {
            $this->stages[] = $stage;
            $stage->setCandidate($this);
        }

        return $this;
    }

    public function removeStage(Stage $stage): self
    {
        if ($this->stages->contains($stage)) {
            $this->stages->removeElement($stage);
            // set the owning side to null (unless already changed)
            if ($stage->getCandidate() === $this) {
                $stage->setCandidate(null);
            }
        }

        return $this;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage(?string $image): self
    {
        $this->image = $image;

        return $this;
    }

    public function setImageFile(File $image = null)
    {
        $this->imageFile = $image;

        if ($image) {
            $this->updated = new \DateTime('now');
        }
    }

    public function getImageFile()
    {
        return $this->imageFile;
    }

    /**
     * @return Collection|Payment[]
     */
    public function getPayments(): Collection
    {
        return $this->payments;
    }

    public function addPayment(Payment $payment): self
    {
        if (!$this->payments->contains($payment)) {
            $this->payments[] = $payment;
            $payment->setCandidate($this);
        }

        return $this;
    }

    public function removePayment(Payment $payment): self
    {
        if ($this->payments->contains($payment)) {
            $this->payments->removeElement($payment);
            // set the owning side to null (unless already changed)
            if ($payment->getCandidate() === $this) {
                $payment->setCandidate(null);
            }
        }

        return $this;
    }

    public function getGender(): ?string
    {
        return $this->gender;
    }

    public function setGender(?string $gender): self
    {
        $this->gender = $gender;

        return $this;
    }

    /**
     * @Groups({"candidate:read"})
     */
    public function getInitialStage(): ?Stage
    {
        $options = $this->stages->filter(
            function (Stage $stage) {
                return $stage->getOrder() == 1;
            }
        );

        return $options->count() ? $options->last() : null;
    }

    /**
     * @Groups({"candidate:read"})
     */
    public
    function getInitialProcess(): ?Process
    {
        $options = $this->processes->filter(
            function (Process $process) {
                return $process->getPosition() == 1;
            }
        );
        return $options->count() ? $options->last() : null;
    }

    public function getType(): ?int
    {
        return $this->type;
    }

    public function setType(int $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getCurrentSchool(): ?string
    {
        return $this->currentSchool;
    }

    public function setCurrentSchool(?string $currentSchool): self
    {
        $this->currentSchool = $currentSchool;

        return $this;
    }

    public function getCurrentGrade(): ?string
    {
        return $this->currentGrade;
    }

    public function setCurrentGrade(?string $currentGrade): self
    {
        $this->currentGrade = $currentGrade;

        return $this;
    }

}
