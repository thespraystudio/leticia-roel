<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Serializer\Annotation\Groups;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * Categories
 *
 * @ORM\Table(name="category")
 * @ORM\Entity
 * @ApiResource(
 *     collectionOperations={
 *     "get"
 * },
 *     itemOperations={
 *     "get"
 *     },
 *     normalizationContext={"groups"={"read"}},
 *     denormalizationContext={"groups"={"write"}},
 * )
 * @Vich\Uploadable
 */
class Category
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"read"})
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=100, nullable=false)
     * @Groups({"read"})
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"read"})
     * @var string
     */
    private $image;

    /**
     * @Vich\UploadableField(mapping="media", fileNameProperty="image")
     * @var File
     */
    private $imageFile;

    /**
     * @var int|null
     *
     * @ORM\Column(name="type_id", type="string", length=50, nullable=true)
     * @Groups({"read"})
     */
    private $type;

    /**
     *
     * @ORM\Column(name="created", type="datetime", nullable=false)
     * @Gedmo\Timestampable(on="create")
     * @Groups({"read"})
     */
    private $created;

    /**
     *
     * @ORM\Column(name="updated", type="datetime", nullable=false)
     * @Gedmo\Timestampable(on="update")
     * @Groups({"read"})
     */
    private $updated;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Category", inversedBy="categories")
     * @ORM\JoinColumn(onDelete="CASCADE")
     * @Groups({"read"})
     */
    private $category;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Category", mappedBy="category")
     * @ORM\JoinColumn(onDelete="CASCADE")
     * @Groups({"read"})
     */
    private $categories;

    /**
     * @ORM\Column(type="boolean")
     * @Groups({"read"})
     */
    private $visible = false;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\CandidateAnswer", mappedBy="category")
     */
    private $candidateAnswers;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\CategoryGroupLevel")
     * @Groups({"read"})
     */
    private $groupLevel;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->categories = new ArrayCollection();
        $this->candidateAnswers = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage(?string $image): self
    {
        $this->image = $image;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(?string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getCreated(): ?\DateTimeInterface
    {
        return $this->created;
    }

    public function setCreated(\DateTimeInterface $created): self
    {
        $this->created = $created;

        return $this;
    }

    public function getUpdated(): ?\DateTimeInterface
    {
        return $this->updated;
    }

    public function setUpdated(\DateTimeInterface $updated): self
    {
        $this->updated = $updated;

        return $this;
    }

    public function setImageFile(File $image = null)
    {
        $this->imageFile = $image;

        if ($image) {
            $this->updated = new \DateTime('now');
        }
    }

    public function getImageFile()
    {
        return $this->imageFile;
    }

    public function __toString()
    {
        return $this->getName();
    }

    public function getCategory(): ?self
    {
        return $this->category;
    }

    public function setCategory(?self $category): self
    {
        $this->category = $category;

        return $this;
    }

    /**
     * @return Collection|self[]
     */
    public function getCategories(): Collection
    {
        return $this->categories;
    }

    public function addCategory(self $category): self
    {
        if (!$this->categories->contains($category)) {
            $this->categories[] = $category;
            $category->setCategory($this);
        }

        return $this;
    }

    public function removeCategory(self $category): self
    {
        if ($this->categories->contains($category)) {
            $this->categories->removeElement($category);
            // set the owning side to null (unless already changed)
            if ($category->getCategory() === $this) {
                $category->setCategory(null);
            }
        }

        return $this;
    }

    public function getVisible(): ?bool
    {
        return $this->visible;
    }

    public function setVisible(?bool $visible): self
    {
        $this->visible = $visible;

        return $this;
    }

    /**
     * @return Collection|CandidateAnswer[]
     */
    public function getCandidateAnswers(): Collection
    {
        return $this->candidateAnswers;
    }

    public function addCandidateAnswer(CandidateAnswer $candidateAnswer): self
    {
        if (!$this->candidateAnswers->contains($candidateAnswer)) {
            $this->candidateAnswers[] = $candidateAnswer;
            $candidateAnswer->setCategory($this);
        }

        return $this;
    }

    public function removeCandidateAnswer(CandidateAnswer $candidateAnswer): self
    {
        if ($this->candidateAnswers->contains($candidateAnswer)) {
            $this->candidateAnswers->removeElement($candidateAnswer);
            // set the owning side to null (unless already changed)
            if ($candidateAnswer->getCategory() === $this) {
                $candidateAnswer->setCategory(null);
            }
        }

        return $this;
    }

    public function getGroupLevel(): ?CategoryGroupLevel
    {
        return $this->groupLevel;
    }

    public function setGroupLevel(?CategoryGroupLevel $groupLevel): self
    {
        $this->groupLevel = $groupLevel;

        return $this;
    }


}
