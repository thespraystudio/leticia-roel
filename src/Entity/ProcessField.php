<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Behavior\Timestampable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ApiResource(
 *  normalizationContext={"groups"={"read","candidate-field:read"}},
 *  denormalizationContext={"groups"={"write","candidate-field:write"}},
 * )
 * @ORM\Entity(repositoryClass="App\Repository\ProcessFieldRepository")
 */
class ProcessField
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @Groups({"read","candidate-field:read"})
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100)
     * @Groups({"read","candidate-field:read","candidate-field:write"})
     */
    private $name;

    /**
     * @Groups({"read","write"})
     * @ORM\Column(type="string", length=255)
     */
    private $description;

    /**
     * @Groups({"read","write"})
     * @ORM\ManyToOne(targetEntity="App\Entity\Process", inversedBy="fields", cascade={"persist","remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $process;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\CandidateField", mappedBy="field", cascade={"remove"}, orphanRemoval=true)
     */
    private $candidateFields;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Groups({"read","write","candidate-field:read","candidate-field:write"})
     */
    private $type = 0;

    /**
     * @ORM\Column(type="integer")
     */
    private $status = 1;

    public function __construct()
    {
        $this->candidateFields = new ArrayCollection();
    }

    use Timestampable;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getProcess(): ?Process
    {
        return $this->process;
    }

    public function setProcess(?Process $process): self
    {
        $this->process = $process;

        return $this;
    }

    public function getType(): ?int
    {
        return $this->type;
    }

    public function setType(int $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function __toString()
    {
        return sprintf('%s', $this->getName());
    }

    public function getStatus(): ?int
    {
        return $this->status;
    }

    public function setStatus(int $status): self
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return Collection<int, CandidateField>
     */
    public function getCandidateFields(): Collection
    {
        return $this->candidateFields;
    }

    public function addCandidateField(CandidateField $candidateField): self
    {
        if (!$this->candidateFields->contains($candidateField)) {
            $this->candidateFields[] = $candidateField;
            $candidateField->setField($this);
        }

        return $this;
    }

    public function removeCandidateField(CandidateField $candidateField): self
    {
        if ($this->candidateFields->removeElement($candidateField)) {
            // set the owning side to null (unless already changed)
            if ($candidateField->getField() === $this) {
                $candidateField->setField(null);
            }
        }

        return $this;
    }
}
