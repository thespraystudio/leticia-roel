<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ApiResource(
 *  normalizationContext={"groups"={"read","candidate:read"}},
 *  denormalizationContext={"groups"={"write","candidate:write"}},
 * )
 * @ApiFilter(SearchFilter::class, properties={"candidate": "exact","school": "exact"})
 * @ORM\Entity(repositoryClass="App\Repository\ProcessRepository")
 */
class Process
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @Groups({"read","candidate:read"})
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @Groups({"read","write","candidate:read"})
     * @ORM\Column(type="string", length=100)
     */
    private $name;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Groups({"read","write"})
     */
    private $position;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Stage", inversedBy="processes")
     * @Groups({"read","write","candidate:read"})
     */
    private $stage;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Process", inversedBy="children")
     * @Groups({"read","write"})
     */
    private $process;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Process", mappedBy="process", cascade={"persist","remove"})
     * @Groups({"read","write","candidate:read"})
     */
    private $children;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Document", mappedBy="process", cascade={"persist","remove"})
     * @Groups({"read","write","candidate:read"})
     */
    private $documents;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ProcessField", mappedBy="process", orphanRemoval=true, cascade={"persist","remove"})
     * @Groups({"read","write","candidate:read"})
     */
    private $fields;

    /**
     * @ORM\Column(type="integer", nullable=true, options={"default": 1})
     * @Groups({"read","write"})
     */
    private $status = 1;


    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Candidate", inversedBy="processes")
     * @Groups({"read","write"})
     */
    private $candidate;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\School", inversedBy="processes", cascade={"persist"})
     * @ORM\JoinColumn(nullable=true)  
     */
    private ?School $school = null;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Groups({"read","write","candidate:read"})
     */
    private $type = 1;

    /**
     * @ORM\Column(type="integer")
     */
    private $enabled = 1;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     * @Groups({"read","write","candidate:read"})
     */
    private $approved;

    public function __construct()
    {
        $this->children = new ArrayCollection();
        $this->documents = new ArrayCollection();
        $this->fields = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getPosition(): ?int
    {
        return $this->position;
    }

    public function setPosition(?int $position): self
    {
        $this->position = $position;

        return $this;
    }

    public function getType(): ?int
    {
        return $this->type;
    }

    public function setType(?int $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getEnabled(): ?int
    {
        return $this->enabled;
    }

    public function setEnabled(int $enabled): self
    {
        $this->enabled = $enabled;

        return $this;
    }

    public function getStage(): ?Stage
    {
        return $this->stage;
    }

    public function setStage(?Stage $stage): self
    {
        $this->stage = $stage;

        return $this;
    }

    public function getProcess(): ?self
    {
        return $this->process;
    }

    public function setProcess(?self $process): self
    {
        $this->process = $process;

        return $this;
    }

    /**
     * @return Collection<int, Process>
     */
    public function getChildren(): Collection
    {
        return $this->children;
    }

    public function addChild(Process $child): self
    {
        if (!$this->children->contains($child)) {
            $this->children[] = $child;
            $child->setProcess($this);
        }

        return $this;
    }

    public function removeChild(Process $child): self
    {
        if ($this->children->removeElement($child)) {
            // set the owning side to null (unless already changed)
            if ($child->getProcess() === $this) {
                $child->setProcess(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Document>
     */
    public function getDocuments(): Collection
    {
        return $this->documents;
    }

    public function addDocument(Document $document): self
    {
        if (!$this->documents->contains($document)) {
            $this->documents[] = $document;
            $document->setProcess($this);
        }

        return $this;
    }

    public function removeDocument(Document $document): self
    {
        if ($this->documents->removeElement($document)) {
            // set the owning side to null (unless already changed)
            if ($document->getProcess() === $this) {
                $document->setProcess(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, ProcessField>
     */
    public function getFields(): Collection
    {
        return $this->fields;
    }

    public function addField(ProcessField $field): self
    {
        if (!$this->fields->contains($field)) {
            $this->fields[] = $field;
            $field->setProcess($this);
        }

        return $this;
    }

    public function removeField(ProcessField $field): self
    {
        if ($this->fields->removeElement($field)) {
            // set the owning side to null (unless already changed)
            if ($field->getProcess() === $this) {
                $field->setProcess(null);
            }
        }

        return $this;
    }

    public function getCandidate(): ?Candidate
    {
        return $this->candidate;
    }

    public function setCandidate(?Candidate $candidate): self
    {
        $this->candidate = $candidate;

        return $this;
    }

    public function getSchool(): ?School
    {
        return $this->school;
    }

    public function setSchool(?School $school): self
    {
        $this->school = $school;

        return $this;
    }

    public function getStatus(): ?int
    {
        return $this->status;
    }

    public function setStatus(?int $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getApproved(): ?bool
    {
        return $this->approved;
    }

    public function setApproved(?bool $approved): self
    {
        $this->approved = $approved;

        return $this;
    }
}
