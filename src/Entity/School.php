<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Behavior\Timestampable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\SerializedName;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use App\Controller\Api\CandidateController;
use App\Controller\Api\CandidatePutController;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * School
 * @Vich\Uploadable
 * @ORM\Table(name="school", indexes={@ORM\Index(name="fk_country_idx", columns={"country_id"})})
 * @ORM\Entity
 * @ApiResource(
 *     collectionOperations={
 *     "get"
 * },
 *     itemOperations={
 *     "get"
 *     },
 *     normalizationContext={"groups"={"read"}},
 *     denormalizationContext={"groups"={"write"}},
 * )
 */
class School
{

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"read","candidate:read"})
     */
    private $id;

    /**
     * @ORM\Column(name="name", type="string", length=100, nullable=false)
     * @Groups({"read","candidate:read"})
     */
    private $name;

    /**
     * @ORM\Column(name="description", type="text", nullable=true)
     * @Groups({"read","candidate:read"})
     */
    private $description;

    /**
     * @ORM\Column(name="latitud", type="string", length=45, nullable=true)
     * @Groups({"read"})
     */
    private $latitud;

    /**
     * @ORM\Column(name="longitud", type="string", length=45, nullable=true)
     * @Groups({"read"})
     */
    private $longitud;

    /**
     * @ORM\Column(name="address", type="string", length=200, nullable=true)
     * @Groups({"read","candidate:read"})
     */
    private $address;

    /**
     * @Groups({"read","candidate:read"})
     * @ORM\ManyToOne(targetEntity="Country")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="country_id", referencedColumnName="id")
     * })
     */
    private $country;

    /**
     * @ORM\ManyToMany(targetEntity="Candidate", mappedBy="school")
     */
    private $canditates;

    /**
     * @ORM\ManyToMany(targetEntity="Activity", inversedBy="school")
     * @Groups({"read","candidate:read"})
     */
    private $activities;


    /**
     * @ORM\ManyToMany(targetEntity="Sport", inversedBy="school")
     * @Groups({"read","candidate:read"})
     */
    private $sports;

    /**
     * @ORM\ManyToMany(targetEntity="Gallery", inversedBy="school", cascade={"persist"})
     * @Groups({"read","candidate:read"})
     */
    private $gallery;

    /**
     * @ORM\ManyToMany(targetEntity="Level", inversedBy="school")
     * @Groups({"read"})
     */
    private $levels;

    /**
     * @ORM\ManyToMany(targetEntity="Outstanding", inversedBy="school")
     *
     */
    private $outstanding;

    /**
     * @ORM\ManyToMany(targetEntity="SchoolSkill", inversedBy="school")
     * @Groups({"read"})
     */
    private $skill;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\CandidateResult", mappedBy="school", orphanRemoval=true)
     */
    private $results;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\SchoolContact", mappedBy="school", orphanRemoval=true, cascade={"persist"})     *
     */
    private $contacts;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\SchoolInformation", mappedBy="school", orphanRemoval=true, cascade={"persist"})
     * @Groups({"read"})
     */
    private $descriptions;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\SchoolComment", mappedBy="school", orphanRemoval=true)
     */
    private $comments;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"read"})
     */
    private $website;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Process", mappedBy="school", cascade={"persist"})
     * @Groups({"candidate:read"})
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $processes;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Stage", mappedBy="school", cascade={"persist"})
     * @Groups({"candidate:read"})
     */
    private $stages;

    /**
     * @ORM\Column(type="text",nullable=true)
     * @Groups({"school:write","school:read","candidate:read"})
     * @SerializedName("file")
     * @var string
     */
    private $image;

    /**
     * @Assert\File(
     *     maxSize = "6072k",
     *     mimeTypes = {"image/png", "image/jpg", "image/jpeg", "image/jpg"},
     *     mimeTypesMessage = "Formatos validos JPEG, JPG y PNG"
     * )
     * @Vich\UploadableField(mapping="media", fileNameProperty="image")
     * @var File
     */
    private $imageFile;


    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"read","candidate:read"})
     */
    private $state;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"read","candidate:read"})
     */
    private $city;

    /**
     * @ORM\OneToMany(targetEntity=CandidateStage::class, mappedBy="school")
     */
    private $candidateStages;

    /**
     * @ORM\Column(type="string", length=80, nullable=true)
     * @Groups({"read"})
     */
    private $academics;

    /**
     * @ORM\Column(type="string", length=80, nullable=true)
     * @Groups({"read"})
     */
    private $deportes;

    /**
     * @ORM\Column(type="string", length=80, nullable=true)
     * @Groups({"read"})
     */
    private $artes;

    /**
     * @ORM\Column(type="string", length=80, nullable=true)
     * @Groups({"read"})
     */
    private $boarding;

    use Timestampable;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->canditates = new ArrayCollection();
        $this->activities = new ArrayCollection();
        $this->gallery = new ArrayCollection();
        $this->levels = new ArrayCollection();
        $this->outstanding = new ArrayCollection();
        $this->skill = new ArrayCollection();
        $this->results = new ArrayCollection();
        $this->contacts = new ArrayCollection();
        $this->descriptions = new ArrayCollection();
        $this->comments = new ArrayCollection();
        // $this->processes = new ArrayCollection();
        $this->stages = new ArrayCollection();
        $this->candidateStages = new ArrayCollection();
        $this->sports = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getLatitud(): ?string
    {
        return $this->latitud;
    }

    public function setLatitud(?string $latitud): self
    {
        $this->latitud = $latitud;

        return $this;
    }

    public function getLongitud(): ?string
    {
        return $this->longitud;
    }

    public function setLongitud(?string $longitud): self
    {
        $this->longitud = $longitud;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(?string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getCountry(): ?Country
    {
        return $this->country;
    }

    public function setCountry(?Country $country): self
    {
        $this->country = $country;

        return $this;
    }

    /**
     * @return Collection|Candidate[]
     */
    public function getCanditates(): Collection
    {
        return $this->canditates;
    }

    public function addCanditate(Candidate $canditate): self
    {
        if (!$this->canditates->contains($canditate)) {
            $this->canditates[] = $canditate;
            $canditate->addSchool($this);
        }

        return $this;
    }

    public function removeCanditate(Candidate $canditate): self
    {
        if ($this->canditates->contains($canditate)) {
            $this->canditates->removeElement($canditate);
            $canditate->removeSchool($this);
        }

        return $this;
    }

    /**
     * @return Collection|Activity[]
     */
    public function getActivities(): Collection
    {
        return $this->activities;
    }

    public function addActivity(Activity $activity): self
    {
        if (!$this->activities->contains($activity)) {
            $this->activities[] = $activity;
        }

        return $this;
    }

    public function removeActivity(Activity $activity): self
    {
        if ($this->activities->contains($activity)) {
            $this->activities->removeElement($activity);
        }

        return $this;
    }

    /**
     * @return Collection|Gallery[]
     */
    public function getGallery(): Collection
    {
        return $this->gallery;
    }

    public function addGallery(Gallery $gallery): self
    {
        if (!$this->gallery->contains($gallery)) {
            $this->gallery[] = $gallery;
        }

        return $this;
    }

    public function removeGallery(Gallery $gallery): self
    {
        if ($this->gallery->contains($gallery)) {
            $this->gallery->removeElement($gallery);
        }

        return $this;
    }

    /**
     * @return Collection|Level[]
     */
    public function getLevels(): Collection
    {
        return $this->levels;
    }

    public function addLevel(Level $level): self
    {
        if (!$this->levels->contains($level)) {
            $this->levels[] = $level;
        }

        return $this;
    }

    public function removeLevel(Level $level): self
    {
        if ($this->levels->contains($level)) {
            $this->levels->removeElement($level);
        }

        return $this;
    }

    /**
     * @return Collection|Outstanding[]
     */
    public function getOutstanding(): Collection
    {
        return $this->outstanding;
    }

    public function addOutstanding(Outstanding $outstanding): self
    {
        if (!$this->outstanding->contains($outstanding)) {
            $this->outstanding[] = $outstanding;
        }

        return $this;
    }

    public function removeOutstanding(Outstanding $outstanding): self
    {
        if ($this->outstanding->contains($outstanding)) {
            $this->outstanding->removeElement($outstanding);
        }

        return $this;
    }

    /**
     * @return Collection|SchoolSkill[]
     */
    public function getSkill(): Collection
    {
        return $this->skill;
    }

    public function addSkill(SchoolSkill $skill): self
    {
        if (!$this->skill->contains($skill)) {
            $this->skill[] = $skill;
        }

        return $this;
    }

    public function removeSkill(SchoolSkill $skill): self
    {
        if ($this->skill->contains($skill)) {
            $this->skill->removeElement($skill);
        }

        return $this;
    }

    public function __toString()
    {
        return $this->getName();
    }

    /**
     * @return Collection|CandidateResult[]
     */
    public function getResults(): Collection
    {
        return $this->results;
    }

    public function addResult(CandidateResult $result): self
    {
        if (!$this->results->contains($result)) {
            $this->results[] = $result;
            $result->setSchool($this);
        }

        return $this;
    }

    public function removeResult(CandidateResult $result): self
    {
        if ($this->results->contains($result)) {
            $this->results->removeElement($result);
            // set the owning side to null (unless already changed)
            if ($result->getSchool() === $this) {
                $result->setSchool(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|SchoolContact[]
     */
    public function getContacts(): Collection
    {
        return $this->contacts;
    }

    public function addContact(SchoolContact $contact): self
    {
        if (!$this->contacts->contains($contact)) {
            $this->contacts[] = $contact;
            $contact->setSchool($this);
        }

        return $this;
    }

    public function removeContact(SchoolContact $contact): self
    {
        if ($this->contacts->contains($contact)) {
            $this->contacts->removeElement($contact);
            // set the owning side to null (unless already changed)
            if ($contact->getSchool() === $this) {
                $contact->setSchool(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|SchoolInformation[]
     */
    public function getDescriptions(): Collection
    {
        return $this->descriptions;
    }

    public function addDescription(SchoolInformation $description): self
    {
        if (!$this->descriptions->contains($description)) {
            $this->descriptions[] = $description;
            $description->setSchool($this);
        }

        return $this;
    }

    public function removeDescription(SchoolInformation $description): self
    {
        if ($this->descriptions->contains($description)) {
            $this->descriptions->removeElement($description);
            // set the owning side to null (unless already changed)
            if ($description->getSchool() === $this) {
                $description->setSchool(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|SchoolComment[]
     */
    public function getComments(): Collection
    {
        return $this->comments;
    }

    public function addComment(SchoolComment $comment): self
    {
        if (!$this->comments->contains($comment)) {
            $this->comments[] = $comment;
            $comment->setSchool($this);
        }

        return $this;
    }

    public function removeComment(SchoolComment $comment): self
    {
        if ($this->comments->contains($comment)) {
            $this->comments->removeElement($comment);
            // set the owning side to null (unless already changed)
            if ($comment->getSchool() === $this) {
                $comment->setSchool(null);
            }
        }

        return $this;
    }

    public function getWebsite(): ?string
    {
        return $this->website;
    }

    public function setWebsite(?string $website): self
    {
        $this->website = $website;

        return $this;
    }

    /**
     * @return Collection<int, Process>
     */
    public function getProcesses(): Collection
    {
        return $this->processes;
    }

    public function addProcess(Process $process): self
    {
        if (!$this->processes->contains($process)) {
            $this->processes[] = $process;
            $process->setSchool($this);
        }

        return $this;
    }

    public function removeProcess(Process $process): self
    {
        if ($this->processes->removeElement($process)) {
            // set the owning side to null (unless already changed)
            if ($process->getSchool() === $this) {
                $process->setSchool(null);
            }
        }

        return $this;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage(?string $image): self
    {
        $this->image = $image;

        return $this;
    }

    public function setImageFile(File $image = null)
    {
        $this->imageFile = $image;

        if ($image) {
            $this->updated = new \DateTime('now');
        }
    }

    public function getImageFile()
    {
        return $this->imageFile;
    }

    /**
     * @return Collection<int, Stage>
     */
    public function getStages(): Collection
    {
        return $this->stages;
    }

    public function addStage(Stage $stage): self
    {
        if (!$this->stages->contains($stage)) {
            $this->stages[] = $stage;
            $stage->setSchool($this);
        }

        return $this;
    }

    public function removeStage(Stage $stage): self
    {
        if ($this->stages->removeElement($stage)) {
            // set the owning side to null (unless already changed)
            if ($stage->getSchool() === $this) {
                $stage->setSchool(null);
            }
        }

        return $this;
    }


    public function getState(): ?string
    {
        return $this->state;
    }

    public function setState(?string $state): self
    {
        $this->state = $state;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(?string $city): self
    {
        $this->city = $city;

        return $this;
    }

    /**
     * @return Collection<int, CandidateStage>
     */
    public function getCandidateStages(): Collection
    {
        return $this->candidateStages;
    }

    public function addCandidateStage(CandidateStage $candidateStage): self
    {
        if (!$this->candidateStages->contains($candidateStage)) {
            $this->candidateStages[] = $candidateStage;
            $candidateStage->setSchool($this);
        }

        return $this;
    }

    public function removeCandidateStage(CandidateStage $candidateStage): self
    {
        if ($this->candidateStages->removeElement($candidateStage)) {
            // set the owning side to null (unless already changed)
            if ($candidateStage->getSchool() === $this) {
                $candidateStage->setSchool(null);
            }
        }

        return $this;
    }

    public function getAcademics(): ?string
    {
        return $this->academics;
    }

    public function setAcademics(?string $academics): self
    {
        $this->academics = $academics;

        return $this;
    }

    public function getDeportes(): ?string
    {
        return $this->deportes;
    }

    public function setDeportes(?string $deportes): self
    {
        $this->deportes = $deportes;

        return $this;
    }

    public function getArtes(): ?string
    {
        return $this->artes;
    }

    public function setArtes(?string $artes): self
    {
        $this->artes = $artes;

        return $this;
    }

    public function getBoarding(): ?string
    {
        return $this->boarding;
    }

    public function setBoarding(?string $boarding): self
    {
        $this->boarding = $boarding;

        return $this;
    }

    /**
     * @return Collection|Sport[]
     */
    public function getSports(): Collection
    {
        return $this->sports;
    }

    public function addSport(Sport $sport): self
    {
        if (!$this->sports->contains($sport)) {
            $this->sports[] = $sport;
        }

        return $this;
    }

    public function removeSport(Sport $sport): self
    {
        if ($this->sports->contains($sport)) {
            $this->sports->removeElement($sport);
        }

        return $this;
    }


    public function fullAddress()
    {
        $piecesAdress = [];
        if ($this->getCity()) {
            $piecesAdress[] = $this->getCity();
        }

        if ($this->getState()) {
            $piecesAdress[] = $this->getState();
        }

        if ($this->getCountry()) {
            $piecesAdress[] = $this->getCountry()->getName();
        }
        return implode(", ", $piecesAdress);
    }
}
