<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\Timestampable;
use Symfony\Component\Serializer\Annotation\Groups;


/**
 * @ORM\Entity(repositoryClass="App\Repository\AppointmentRepository")
 * @ApiResource(
 *      collectionOperations={
 *     "get",
 * },
 *     itemOperations={
 *     "get",
 *     },
 *     normalizationContext={"groups"={"read","candidate:read"}},
 *     denormalizationContext={"groups"={"write","candidate:write"}},
 * )
 */
class Appointment
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"read","candidate:read"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100)
     * @Groups({"read","candidate:read"})
     */
    private $name;

    use Timestampable;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\CandidateAppointment", mappedBy="appointment", orphanRemoval=true)
     */
    private $candidates;

    public function __construct()
    {
        $this->candidates = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }


    /**
     * @return Collection|CandidateAppointment[]
     */
    public function getCandidates(): Collection
    {
        return $this->candidates;
    }

    public function addCandidate(CandidateAppointment $candidate): self
    {
        if (!$this->candidates->contains($candidate)) {
            $this->candidates[] = $candidate;
            $candidate->setAppointment($this);
        }

        return $this;
    }

    public function removeCandidate(CandidateAppointment $candidate): self
    {
        if ($this->candidates->contains($candidate)) {
            $this->candidates->removeElement($candidate);
            // set the owning side to null (unless already changed)
            if ($candidate->getAppointment() === $this) {
                $candidate->setAppointment(null);
            }
        }

        return $this;
    }

    public function __toString()
    {
        return $this->getName();
    }
}
