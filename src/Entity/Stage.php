<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Stages
 *
 * @ORM\Table(name="stage")
 * @ORM\Entity(repositoryClass="App\Repository\StageRepository")
 * @ApiResource(
 *     collectionOperations={
 *     "get",
 *     "post",
 * },
 *     itemOperations={
 *     "get",
 *     "put",
 *     "delete"
 *     },
 *     normalizationContext={"groups"={"read"}},
 *     denormalizationContext={"groups"={"write"}},
 * )
 */
class Stage
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"read","candidate:read"})
     */
    private $id;

    /**
     * @ORM\Column(name="name", type="string", length=100, nullable=false)
     * @Groups({"read","write","candidate:read"})
     */
    private $name;

    /**
     * @ORM\Column(name="position", type="integer", nullable=false)
     * @Groups({"read","write"})
     */
    private $order = '0';

    /**
     *
     * @ORM\Column(name="created", type="datetime", nullable=false)
     * @Gedmo\Timestampable(on="create")
     * @Groups({"read"})
     */
    private $created;

    /**
     *
     * @ORM\Column(name="updated", type="datetime", nullable=false)
     * @Gedmo\Timestampable(on="update")
     * @Groups({"read"})
     */
    private $updated;

    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     * @Groups({"read","write","candidate:read"})
     */
    private $color;
    
    


    // /**
    //  * @ORM\ManyToOne(targetEntity="App\Entity\Stage", inversedBy="stages")
    //  * @Groups({"read","write"})
    //  */
    // private $stage;

    // /**
    //  * @ORM\OneToMany(targetEntity="App\Entity\Stage", mappedBy="stage")
    //  * @Groups({"read","write"})
    //  */
    // private $stages;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Process", mappedBy="stage", orphanRemoval=true, cascade={"persist", "remove"})
     */
    private $processes;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     * @Groups({"read","write"})
     */
    private $template;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Candidate", inversedBy="stages")
     * @Groups({"read","write"})
     */
    private $candidate;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\School" , inversedBy="stages")
     * @Groups({"read","write","candidate:read"})
     */
    private $school;

    /**
     * @ORM\Column(type="integer")
     * @Groups({"read","write"})
     */
    private $status = 1;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     * @Groups({"read","write","candidate:read"})
     */
    private $approved;

    /**
     * Constructor
     */
    public function __construct()
    {
        // $this->stages = new ArrayCollection();
        $this->processes = new ArrayCollection();
        // $this->school = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getOrder(): ?int
    {
        return $this->order;
    }

    public function setOrder(int $order): self
    {
        $this->order = $order;

        return $this;
    }

    public function getCreated(): ?\DateTimeInterface
    {
        return $this->created;
    }

    public function setCreated(?\DateTimeInterface $created): self
    {
        $this->created = $created;

        return $this;
    }

    public function getUpdated(): ?\DateTimeInterface
    {
        return $this->updated;
    }

    public function setUpdated(?\DateTimeInterface $updated): self
    {
        $this->updated = $updated;

        return $this;
    }

    public function getColor(): ?string
    {
        return $this->color;
    }

    public function setColor(?string $color): self
    {
        $this->color = $color;

        return $this;
    }

    public function getStage(): ?self
    {
        return $this->stage;
    }

    public function setStage(?self $stage): self
    {
        $this->stage = $stage;

        return $this;
    }

    /**
     * @return Collection|self[]
     */
    public function getStages(): Collection
    {
        return $this->stages;
    }

    public function addStage(self $stage): self
    {
        if (!$this->stages->contains($stage)) {
            $this->stages[] = $stage;
            $stage->setStage($this);
        }

        return $this;
    }

    public function removeStage(self $stage): self
    {
        if ($this->stages->contains($stage)) {
            $this->stages->removeElement($stage);
            // set the owning side to null (unless already changed)
            if ($stage->getStage() === $this) {
                $stage->setStage(null);
            }
        }

        return $this;
    }

    public function __toString()
    {
        return $this->getName();
    }

    /**
     * @return Collection|Process[]
     */
    public function getProcesses(): Collection
    {
        return $this->processes;
    }

    public function addProcess(Process $process): self
    {
        if (!$this->processes->contains($process)) {
            $this->processes[] = $process;
            $process->setStage($this);
        }

        return $this;
    }

    public function removeProcess(Process $process): self
    {
        if ($this->processes->contains($process)) {
            $this->processes->removeElement($process);
            // set the owning side to null (unless already changed)
            if ($process->getStage() === $this) {
                $process->setStage(null);
            }
        }

        return $this;
    }

    public function getTemplate(): ?bool
    {
        return $this->template;
    }

    public function setTemplate(bool $template): self
    {
        $this->template = $template;

        return $this;
    }

    public function getCandidate(): ?Candidate
    {
        return $this->candidate;
    }

    public function setCandidate(?Candidate $candidate): self
    {
        $this->candidate = $candidate;

        return $this;
    }

    public function getSchool(): ?School
    {
        return $this->school;
    }

    public function setSchool(?School $school): self
    {
        $this->school = $school;

        return $this;
    }

    public function getStatus(): ?int
    {
        return $this->status;
    }

    public function setStatus(int $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getApproved(): ?bool
    {
        return $this->approved;
    }

    public function setApproved(?bool $approved): self
    {
        $this->approved = $approved;

        return $this;
    }

    

   

}
