<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * CanditeExam
 *
 * @ORM\Table(name="candidate_exam")
 * @ORM\Entity
 * @ApiResource(
 *     collectionOperations={
 *     "get"
 * },
 *     itemOperations={
 *     "get"
 *     },
 *     normalizationContext={"groups"={"read"}},
 *     denormalizationContext={"groups"={"write"}},
 * )
 */
class CandidateExam
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"read"})
     */
    private $id;

    /**
     * @var \ScheduledExam
     * @Groups({"read"})
     * @ORM\ManyToOne(targetEntity="ScheduledExam")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="scheduled_id", referencedColumnName="id")
     * })
     */
    private $scheduled;

    /**
     * @var \Candidates
     * @Groups({"read"})
     * @ORM\ManyToOne(targetEntity="Candidate")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="candidate_id", referencedColumnName="id")
     * })
     */
    private $candidate;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getScheduled(): ?ScheduledExam
    {
        return $this->scheduled;
    }

    public function setScheduled(?ScheduledExam $scheduled): self
    {
        $this->scheduled = $scheduled;

        return $this;
    }

    public function getCandidate(): ?Candidate
    {
        return $this->candidate;
    }

    public function setCandidate(?Candidate $candidate): self
    {
        $this->candidate = $candidate;

        return $this;
    }


}
