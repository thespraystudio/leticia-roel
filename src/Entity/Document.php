<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Documents
 *
 * @ORM\Table(name="document")
 * @ORM\Entity
 * @ApiResource(
 *     collectionOperations={
 *     "get",
 * },
 *     itemOperations={
 *     "get",
 *     },
 *     normalizationContext={"groups"={"read","candidate:read"}},
 *     denormalizationContext={"groups"={"write","candidate:write"}},
 * )
 */
class Document
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"read","candidate:read"})
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="name", type="string", length=100, nullable=false)
     * @Groups({"read","write","candidate:read"})
     */
    private $name;

    /**
     * @var int
     *
     * @ORM\Column(name="type_document", type="integer", nullable=true)
     * @Groups({"read","write","candidate:read"})
     */
    private $typeDocument = 0;

    /**
     *
     * @ORM\Column(name="created", type="datetime", nullable=false)
     * @Gedmo\Timestampable(on="create")
     * @Groups({"read","write"})
     */
    private $created;

    /**
     *
     * @ORM\Column(name="updated", type="datetime", nullable=false)
     * @Gedmo\Timestampable(on="update")
     * @Groups({"read","write"})
     */
    private $updated;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Process", inversedBy="documents", cascade={"persist", "remove"})
     */
    private $process;

    /**
     * @ORM\Column(type="integer")
     */
    private $status = 1;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $description;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\CandidateDocument", mappedBy="document")
     */
    private $candidateDocuments;


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->candidateDocuments = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getTypeDocument(): ?int
    {
        return $this->typeDocument;
    }

    public function setTypeDocument(int $typeDocument): self
    {
        $this->typeDocument = $typeDocument;

        return $this;
    }

    public function getCreated(): ?\DateTimeInterface
    {
        return $this->created;
    }

    public function setCreated(\DateTimeInterface $created): self
    {
        $this->created = $created;

        return $this;
    }

    public function getUpdated(): ?\DateTimeInterface
    {
        return $this->updated;
    }

    public function setUpdated(\DateTimeInterface $updated): self
    {
        $this->updated = $updated;

        return $this;
    }


    public function __toString()
    {
        return $this->getName();
    }


    public function getProcess(): ?Process
    {
        return $this->process;
    }

    public function setProcess(?Process $process): self
    {
        $this->process = $process;

        return $this;
    }

    public function getStatus(): ?int
    {
        return $this->status;
    }

    public function setStatus(int $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return Collection<int, CandidateDocument>
     */
    public function getCandidateDocuments(): Collection
    {
        return $this->candidateDocuments;
    }

    public function addCandidateDocument(CandidateDocument $candidateDocument): self
    {
        if (!$this->candidateDocuments->contains($candidateDocument)) {
            $this->candidateDocuments[] = $candidateDocument;
            $candidateDocument->setDocument($this);
        }

        return $this;
    }

    public function removeCandidateDocument(CandidateDocument $candidateDocument): self
    {
        if ($this->candidateDocuments->removeElement($candidateDocument)) {
            // set the owning side to null (unless already changed)
            if ($candidateDocument->getDocument() === $this) {
                $candidateDocument->setDocument(null);
            }
        }

        return $this;
    }
}
