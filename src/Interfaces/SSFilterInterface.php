<?php


namespace App\Interfaces;


use Doctrine\ORM\QueryBuilder;
use Knp\Bundle\PaginatorBundle\Pagination\SlidingPagination;

interface SSFilterInterface
{
    public function getFormClass(): string ;
    public function getFilterEntity();
    public function getName(): string;
}