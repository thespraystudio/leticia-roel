<?php

namespace App\Repository;

use App\Entity\CandidateField;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method CandidateField|null find($id, $lockMode = null, $lockVersion = null)
 * @method CandidateField|null findOneBy(array $criteria, array $orderBy = null)
 * @method CandidateField[]    findAll()
 * @method CandidateField[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CandidateFieldRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CandidateField::class);
    }

    // /**
    //  * @return CandidateField[] Returns an array of CandidateField objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CandidateField
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
