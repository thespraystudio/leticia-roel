<?php

namespace App\Repository;

use App\Entity\CandidateAnswer;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;

use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method CandidateAnswer|null find($id, $lockMode = null, $lockVersion = null)
 * @method CandidateAnswer|null findOneBy(array $criteria, array $orderBy = null)
 * @method CandidateAnswer[]    findAll()
 * @method CandidateAnswer[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CandidateAnswerRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CandidateAnswer::class);
    }

    // /**
    //  * @return CandidateAnswer[] Returns an array of CandidateAnswer objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CandidateAnswer
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
