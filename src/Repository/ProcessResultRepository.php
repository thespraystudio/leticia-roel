<?php

namespace App\Repository;

use App\Entity\ProcessResult;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ProcessResult|null find($id, $lockMode = null, $lockVersion = null)
 * @method ProcessResult|null findOneBy(array $criteria, array $orderBy = null)
 * @method ProcessResult[]    findAll()
 * @method ProcessResult[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProcessResultRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ProcessResult::class);
    }

    // /**
    //  * @return ProcessResult[] Returns an array of ProcessResult objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ProcessResult
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
