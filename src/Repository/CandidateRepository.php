<?php

namespace App\Repository;

use App\Entity\Candidate;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;

use Doctrine\ORM\Query\Expr;

use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;


class CandidateRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Candidate::class);
    }

    /**
     *
     *
     * @return Candidate[] Devuelve un array de objetos Candidate.
     */
    public function findActiveCandidates(int $user = null): array
    {
        $queryBuilder = $this->createQueryBuilder('c');
        $queryBuilder
            ->andWhere('c.status != :status')
            ->setParameter('status', 2)
            ->andWhere('c.type != :type')
            ->setParameter('type', 2)
            ->orderBy('c.id', 'DESC');
        if ($user) {
            $queryBuilder->leftJoin('c.operators','operators');
            $queryBuilder->andWhere('operators = :user')
                ->setParameter('user', $user);
        }
        return $queryBuilder
            ->getQuery()
            ->getResult();
    }

    public function findTutorCandidates(int $user = null): array
    {
        $queryBuilder = $this->createQueryBuilder('c');
        $subqueryBuilder = $this->_em->createQueryBuilder();

        $subquery = $subqueryBuilder
            ->select('candidate.id')
            ->from('App\Entity\Candidate', 'candidate')
            ->andWhere('candidate.owner = :user')
            ->setParameter('user', $user)
            ->getQuery()
            ->getResult();
        if (empty($subquery)) {
            return [];
        }
        $candidateIds = array_column($subquery, 'id');

        $queryBuilder
            ->andWhere('c.status != :status')
            ->andWhere('c.type != :status')
            ->setParameter('status', 2)
            ->andWhere(
                $queryBuilder->expr()->in('c.candidate', $candidateIds)
            );

        $query = $queryBuilder->getQuery();
        $sql = $query->getSQL();
        dd($sql);
        return $queryBuilder
            ->getQuery()
            ->getResult();
    }
}
