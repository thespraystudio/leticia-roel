<?php

namespace App\Repository;

use App\Entity\ProcessField;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
/**
 * @method ProcessField|null find($id, $lockMode = null, $lockVersion = null)
 * @method ProcessField|null findOneBy(array $criteria, array $orderBy = null)
 * @method ProcessField[]    findAll()
 * @method ProcessField[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProcessFieldRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ProcessField::class);
    }

    // /**
    //  * @return ProcessField[] Returns an array of ProcessField objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ProcessField
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
