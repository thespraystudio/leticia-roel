<?php

namespace App\Repository;

use App\Entity\SchoolComment;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method SchoolComment|null find($id, $lockMode = null, $lockVersion = null)
 * @method SchoolComment|null findOneBy(array $criteria, array $orderBy = null)
 * @method SchoolComment[]    findAll()
 * @method SchoolComment[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SchoolCommentRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SchoolComment::class);
    }

    // /**
    //  * @return SchoolComment[] Returns an array of SchoolComment objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?SchoolComment
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
