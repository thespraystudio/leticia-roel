<?php

namespace App\Repository;


use App\Entity\School;
use App\Entity\Candidate;
use App\Entity\Stage;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use App\Entity\Filter\MainFilter;


/**
 * @method Stage|null find($id, $lockMode = null, $lockVersion = null)
 * @method Stage|null findOneBy(array $criteria, array $orderBy = null)
 * @method Stage[]    findAll()
 * @method Stage[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class StageRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Stage::class);
    }

    public function findStagesByCandidateSchools($schoolIds)
    {
        return $this->createQueryBuilder('s')
            ->leftJoin('s.school', 'sc')
            ->where('sc.id IN (:schoolIds) and s.template = true')
            ->setParameter('schoolIds', $schoolIds)
            ->distinct()
            ->getQuery()
            ->getResult();
    }

    public function findApprovedStagesBySchoolAndCandidate(School $school, Candidate $candidate): array
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.school = :school')
            ->andWhere('s.candidate = :candidate')
            ->andWhere('s.approved = :approved')
            ->setParameter('school', $school)
            ->setParameter('candidate', $candidate)
            ->setParameter('approved', true)
            ->getQuery()
            ->getResult();
    }

    // /**
    //  * @return Stage[] Returns an array of Stage objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */


    /*  public function findOneBySomeField($value): ?Stage
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

    public function findAllWithProcess($value = null)
    {


        return $this->createQueryBuilder('c')
            ->select('c,p')
            ->leftjoin('c.processes', 'p')
            // ->leftjoin('p.children','z')
            // ->andWhere('c.processes = :id')
            ->andWhere('p.enabled = :enabled')
            ->andWhere('c.status = :status_enabled')
            ->andWhere('c.school = :val')
            // ->andWhere('z.enabled = :enabledz')
            ->orderBy('c.id', 'ASC')
            ->setParameter('enabled', 1)
            // ->setParameter('enabledz', 1)
            ->setParameter('status_enabled', 1)
            ->setParameter('val', $value)
            // ->setParameter('id', 1)
            ->getQuery()->getResult();
    }
}
