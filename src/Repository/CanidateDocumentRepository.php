<?php

namespace App\Repository;

use App\Entity\CandidateDocument;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method CandidateDocument|null find($id, $lockMode = null, $lockVersion = null)
 * @method CandidateDocument|null findOneBy(array $criteria, array $orderBy = null)
 * @method CandidateDocument[]    findAll()
 * @method CandidateDocument[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CanidateDocumentRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CandidateDocument::class);
    }

    // /**
    //  * @return CanidateDocument[] Returns an array of CanidateDocument objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CanidateDocument
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
