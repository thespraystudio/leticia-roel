<?php

namespace App\Repository;

use App\Entity\SchoolInformation;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;

use Doctrine\Persistence\ManagerRegistry;

/**
 * @method SchoolInformation|null find($id, $lockMode = null, $lockVersion = null)
 * @method SchoolInformation|null findOneBy(array $criteria, array $orderBy = null)
 * @method SchoolInformation[]    findAll()
 * @method SchoolInformation[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SchoolInformationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SchoolInformation::class);
    }

    // /**
    //  * @return SchoolInformation[] Returns an array of SchoolInformation objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?SchoolInformation
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
