<?php

namespace App\Repository;

use App\Entity\Filter\SchoolFilter;
use App\Entity\School;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method School|null find($id, $lockMode = null, $lockVersion = null)
 * @method School|null findOneBy(array $criteria, array $orderBy = null)
 * @method School[]    findAll()
 * @method School[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SchoolRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, School::class);
    }

    public function filterSchools(SchoolFilter $schoolFilter = null)
    {
        $conn = $this->getEntityManager()->getConnection();
        $where = "";

        if ($schoolFilter && $schoolFilter->getCountry() && $schoolFilter->getCountry()->count()) {
            $ids = [];

            foreach ($schoolFilter->getCountry() as $level) {
                $ids[] = $level->getId();
            }

            $where .= " AND C.id IN (" . implode(',', $ids) . ")";
        }

        if ($schoolFilter && $schoolFilter->getActivities() && $schoolFilter->getActivities()->count()) {
            $ids = [];

            foreach ($schoolFilter->getActivities() as $activity) {
                $ids[] = $activity->getId();
            }

            $where .= " AND Y.id IN (" . implode(',', $ids) . ")";
        }

        if ($schoolFilter && $schoolFilter->getSports() && $schoolFilter->getSports()->count()) {
            $ids = [];

            foreach ($schoolFilter->getSports() as $sport) {
                $ids[] = $sport->getId();
            }

            $where .= " AND Y.id IN (" . implode(',', $ids) . ")";
        }


        if ($schoolFilter && !is_null($schoolFilter->getReligion())) {
            $where .= " AND I.religion = " . ($schoolFilter->getReligion() ? 1 : 0);
        }

        if ($schoolFilter && $schoolFilter->getMilitary()) {
            $where .= " AND I.military = " . $schoolFilter->getMilitary();
        }

        if ($schoolFilter && $schoolFilter->getDresscode()) {
            $where .= " AND I.dresscode = '" . $schoolFilter->getDresscode() . "' ";
        }

        if ($schoolFilter && $schoolFilter->getEnglish()) {
            $where .= " AND I.esl = " . $schoolFilter->getEnglish();
        }

        if ($schoolFilter && $schoolFilter->getLevels() && $schoolFilter->getLevels()->count()) {
            $ids = [];

            foreach ($schoolFilter->getLevels() as $level) {
                $ids[] = $level->getId();
            }

            $where .= " AND E.id IN (" . implode(',', $ids) . ")";
        }

        if ($schoolFilter && $schoolFilter->getCapacity()) {
            $where .= " AND I.capacity = " . $schoolFilter->getEnglish();
        }

        if ($schoolFilter && $schoolFilter->getTypeSchool() && $schoolFilter->getTypeSchool()) {
            $ids = [];

            foreach ($schoolFilter->getTypeSchool() as $level) {

                $ids[] = $level;
            }

            $where .= " AND I.type_school IN ('" . implode(',', $ids) . "')";
        }

        // if (!is_null($schoolFilter->getCostMin()) && !is_null($schoolFilter->getCostMax())) {
        //     $where .= " AND I.cost between " . $schoolFilter->getCostMin() . " and " . $schoolFilter->getCostMax();
        // }

        if ($schoolFilter && $schoolFilter->getOthers()) {

            $ids = [];

            foreach ($schoolFilter->getOthers() as $currency) {

                switch ($currency) {
                    case 1:
                        $where .= " AND I.advances_classes IN (" . $currency . ")";
                        break;
                    case 2:
                        $where .= " AND I.learning_differences IN (" . $currency . ")";
                        break;
                    case 3:
                        $where .= " AND I.requiere_guardian IN (" . $currency . ")";
                        break;
                    case 4:
                        $where .= " AND I.a_level IN (" . $currency . ")";
                        break;
                    case 5:
                        $where .= " AND I.ib IN (" . $currency . ")";
                        break;
                    case 6:
                        $where .= " AND I.saturday_classes IN (" . $currency . ")";
                        break;
                    case 7:
                        $where .= " AND I.rolling_admission IN (" . $currency . ")";
                        break;
                    case 8:
                        $where .= " AND I.application_deadline IN (" . $currency . ")";
                        break;
                }
            }
        }


        $sql = 'select distinct S.id, S.name, C.name as country, S.address, C.id as id_country, I.cost, S.image as image,
    S.website, 0 as stages
            FROM school S
            LEFT JOIN country C ON S.country_id = C.id
            LEFT JOIN school_level L ON L.school_id = S.id
            LEFT JOIN level E ON L.level_id = E.id
            LEFT JOIN school_activity A ON A.school_id = S.id
            LEFT JOIN school_school_skill K ON K.school_id = S.id
            LEFT JOIN activity Y ON Y.id = A.activity_id
            LEFT JOIN school_information I ON I.school_id = S.id
            LEFT JOIN school_sport O ON O.school_id = S.id
            LEFT JOIN sport P ON P.id = O.sport_id

            WHERE S.id > 0   ' . $where . '
            GROUP BY S.id, S.name, C.name, S.address, C.id, I.cost, S.image
            Order by S.name ASC';

        $stmt = $conn->prepare($sql);
        $result = $stmt->executeQuery();
        return $result->fetchAllAssociative();
    }

    public function searchIds($ids): array
    {
        $queryBuilder = $this->createQueryBuilder('s')
            ->andWhere('s.id IN (:ids)')
            ->setParameter('ids', $ids);

        return $queryBuilder
            ->getQuery()
            ->getResult();
    }
}
