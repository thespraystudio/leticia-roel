<?php

namespace App\Repository;

use App\Entity\CandidateAppointment;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method CandidateAppointment|null find($id, $lockMode = null, $lockVersion = null)
 * @method CandidateAppointment|null findOneBy(array $criteria, array $orderBy = null)
 * @method CandidateAppointment[]    findAll()
 * @method CandidateAppointment[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CandidateAppointmentRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CandidateAppointment::class);
    }

    // /**
    //  * @return CandidateAppointment[] Returns an array of CandidateAppointment objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CandidateAppointment
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
