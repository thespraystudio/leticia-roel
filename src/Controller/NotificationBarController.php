<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class NotificationBarController extends AbstractController
{
    /**
     * @Route("/notification/bar", name="app_notification_bar")
     */
    public function index(): Response
    {
        return $this->render('notification_bar/index.html.twig', [
            'controller_name' => 'NotificationBarController',
        ]);
    }
}
