<?php


namespace App\Controller\Api;


use App\Entity\Candidate;
use App\Entity\CandidateDocument;
use App\Entity\Document;
use App\Entity\Report;
use App\Utils\Base64FileExtractor;
use App\Utils\UploadedBase64File;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Security\Core\Security;

class DocumentController
{
    public function __invoke(EntityManagerInterface $em, Request $request, Security $security, Base64FileExtractor $base64FileExtractor): CandidateDocument
    {

        $data = json_decode($request->getContent(), true);

        $base64Image = $data['file'];
        if (!$base64Image) {
            throw new BadRequestHttpException('file" is required');
        }

        $doc = $em->getRepository(Document::class)->find($this->extractNumbers($data['document']));
        $candidate = $em->getRepository(Candidate::class)->find($this->extractNumbers($data['candidate']));
        $imageName = $base64FileExtractor->extractImageName($base64Image);
        $base64Image = $base64FileExtractor->extractBase64String($base64Image);
        $imageFile = new UploadedBase64File($base64Image, $imageName);
        $document = new CandidateDocument();
        $document->setDocumentFile($imageFile);
        if ($candidate && $doc) {
            $document->setDocument($doc);
            $document->setCandidate($candidate);
        }
        return $document;
    }

    protected function extractNumbers($string)
    {
        return filter_var($string, FILTER_SANITIZE_NUMBER_INT);
    }
}