<?php


namespace App\Controller\Api;


use App\Entity\Candidate;
use App\Entity\Report;
use App\Utils\Base64FileExtractor;
use App\Utils\UploadedBase64File;
use Symfony\Component\HttpFoundation\Request;

class CandidateController
{
    public function __invoke(Candidate $data, Request $request, Base64FileExtractor $base64FileExtractor): Candidate
    {
        $post = json_decode($request->getContent(), true);

        $base64Image = @$post['file'];
        if ($base64Image) {
            $imageName = $base64FileExtractor->extractImageName($base64Image);
            $base64Image = $base64FileExtractor->extractBase64String($base64Image);
            $imageFile = new UploadedBase64File($base64Image, $imageName);
            $data->setImageFile($imageFile);
        }


        return $data;
    }

    protected function extractNumbers($string)
    {
        return filter_var($string, FILTER_SANITIZE_NUMBER_INT);
    }
}