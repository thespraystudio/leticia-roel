<?php


namespace App\Controller\Api;


use App\Entity\Report;
use App\Entity\User;
use App\Utils\Base64FileExtractor;
use App\Utils\UploadedBase64File;
use Symfony\Component\HttpFoundation\Request;

class UserController
{
    public function __invoke(User $data, Request $request, Base64FileExtractor $base64FileExtractor): User
    {
        $post = json_decode($request->getContent(), true);

        $base64Image = @$post['file'];
        if ($base64Image) {
            $imageName = $base64FileExtractor->extractImageName($base64Image);
            $base64Image = $base64FileExtractor->extractBase64String($base64Image);
            $imageFile = new UploadedBase64File($base64Image, $imageName);
            $data->setImageFile($imageFile);
        }

        if ($data->getCandidates()->count() > 0) {
            $loopIndex = 0;
            foreach ($data->getCandidates() as $candidate) {
                $base64Image = @$post['candidates'][$loopIndex]['file'];
                if ($base64Image) {
                    $imageName = $base64FileExtractor->extractImageName($base64Image);
                    $base64Image = $base64FileExtractor->extractBase64String($base64Image);
                    $imageFile = new UploadedBase64File($base64Image, $imageName);
                    $candidate->setImageFile($imageFile);
                }
                $loopIndex++;
            }
        }


        return $data;
    }

    protected function extractNumbers($string)
    {
        return filter_var($string, FILTER_SANITIZE_NUMBER_INT);
    }
}