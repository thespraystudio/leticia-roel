<?php

namespace App\Controller;


use App\Entity\Candidate;
use App\Entity\CandidateField;
use App\Entity\CandidateDocument;
use App\Entity\Document;
use App\Entity\CandidateStage;
use App\Entity\ProcessField;
use App\Entity\Stage;


use App\Form\CandidateEditType;
use App\Repository\CandidateFieldRepository;
use App\Repository\CanidateDocumentRepository;
use App\Repository\CandidateStageRepository;
use App\Repository\ProcessRepository;
use App\Repository\CandidateRepository;
use App\Repository\ProcessFieldRepository;

use App\Entity\School;
use App\Repository\StageRepository;
use App\Repository\SchoolRepository;


use App\Form\CandidateType;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Services\StageManager;
use App\Services\ProcessManager;
use App\Services\StageService;

use Symfony\Component\HttpFoundation\JsonResponse;
use Twig\Environment;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\HttpFoundation\File\UploadedFile;


/**
 * @Route("/candidate")
 */
class CandidateController extends AbstractController
{

    private $twig;
    private $security;


    public function __construct(Environment $twig, Security $security)
    {
        $this->twig = $twig;
        $this->security = $security;
    }

    /**
     * @Route("/", name="app_candidate_index", methods={"GET"})
     */
    public function index(EntityManagerInterface $entityManager, CandidateRepository $candidateRepository): Response
    {
        $activeCandidates = $candidateRepository->findActiveCandidates();
        $user = $this->security->getUser();
        if ($this->security->isGranted('ROLE_ADMIN')) {
            $candidates = $candidateRepository->findActiveCandidates();
        } elseif ($this->security->isGranted('ROLE_OPERADOR')) {
            $candidates = $candidateRepository->findActiveCandidates($user->getId());
        } elseif ($this->security->isGranted('ROLE_TUTOR')) {
            $candidates = $candidateRepository->findBy(['owner' => $user->getId(), 'type' => 1]);
        } else {
            throw $this->createAccessDeniedException();
        }
        return $this->render('candidate/index.html.twig', [
            'candidates' => $candidates,
        ]);
    }

    /**
     * @Route("/new", name="app_candidate_new", methods={"GET", "POST"})
     */
    public function new(Request $request, EntityManagerInterface $entityManager, StageManager $stageManager): Response
    {
        $candidate = new Candidate();
        $form = $this->createForm(CandidateType::class, $candidate);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $isNewCandidateField = $form->get('isNewCandidate');
            $isNewCandidateValue = $isNewCandidateField->getData();
            $newCandidateForm = $form->get('newCandidate');
            if ($newCandidateForm->isSubmitted() && $newCandidateForm->isValid() && $isNewCandidateValue) {
                $newCandidateData = $newCandidateForm->getData();
                $newCandidate = new Candidate();
                $newCandidate->setName($newCandidateData->getName());
                $newCandidate->setLastName($newCandidateData->getLastName());
                $newCandidate->setBirthday($newCandidateData->getBirthday());
                $newCandidate->setStreet($newCandidateData->getStreet());
                $newCandidate->setInNumber($newCandidateData->getInNumber());
                $newCandidate->setOutNumber($newCandidateData->getOutNumber());
                $newCandidate->setState($newCandidateData->getState());
                $newCandidate->setPhoneNumber($newCandidateData->getPhoneNumber());
                $newCandidate->setEmail($newCandidateData->getEmail());
                $newCandidate->setType(2);
                $entityManager->persist($newCandidate);
                $entityManager->flush();
                $candidate->setCandidate($newCandidate);
            } else {
            }
            $entityManager->persist($candidate);
            $entityManager->flush();
            $this->addFlash('success', 'Aspirante agregado correctamente.');
            return $this->redirectToRoute('app_candidate_index', [], Response::HTTP_SEE_OTHER);
        }
        return $this->render('candidate/new.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="app_candidate_show", methods={"GET"})
     */
    public function show(Candidate $candidate): Response
    {
        $form = $this->createForm(CandidateType::class, $candidate, ['readonly' => true]);
        return $this->render('candidate/show.html.twig', [
            'candidate' => $candidate,
            'form' => $form->createView(),
        ]);
    }


    /**
     * @Route("/{id}/edit", name="app_candidate_edit", methods={"GET", "POST"})
     */
    public function edit(Request $request, Candidate $candidate, EntityManagerInterface $entityManager, StageManager $stageManager): Response
    {

        $form = $this->createForm(CandidateEditType::class, $candidate);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->addFlash('success', 'Aspirante actualizado correctamente.');
            $stageManager->updateStages($candidate);
            $entityManager->flush();
            return $this->redirectToRoute('app_candidate_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('candidate/edit.html.twig', [
            'candidate' => $candidate,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}/process", name="app_candidate_process", methods={"GET", "POST"})
     */
    public function cadidateProcess(Request $request, Candidate $candidate, EntityManagerInterface $entityManager, StageRepository $stageRepository, StageService $stageService): Response
    {
        $schools = $candidate->getSchool();
        $schoolsInformation = [];
        foreach ($schools as $school) {
            $stageInfo = $stageService->calculateApprovedStagesPercentage($school, $candidate);
            $stages = $stageRepository->findBy(['candidate' => $candidate, 'school' => $school]);
            $schoolInfo = [
                'school' => $school,
                'stage' => $stages,
                'approvedStagesCount' => $stageInfo['approvedStagesCount'],
                'approvedStagesPercentage' => $stageInfo['approvedStagesPercentage']
            ];
            $schoolsInformation[] = $schoolInfo;
        }
        return $this->render('candidate/process.html.twig', [
            'candidate' => $candidate,
            'schools' => $schoolsInformation,
        ]);
    }

    /**
     * @Route("/{id}/process/{school}", name="app_candidate_school_process", methods={"GET", "POST"})
     */
    public function candidateSchoolProcess(
        Request                  $request,
        Candidate                $candidate,
        School                   $school,
        EntityManagerInterface   $entityManager,
        CandidateStageRepository $candidateStageRepository,
        StageRepository          $stageRepository,
        SchoolRepository         $schoolRepository,
        StageService             $stageService
    ): Response
    {
        $stagesCandidate = $stageRepository->findBy(['candidate' => $candidate->getId(), 'school' => $school->getId()]);
        $stageInfo = $stageService->calculateApprovedStagesPercentage($school, $candidate);
        $stages = $stageRepository->findBy(['candidate' => $candidate, 'school' => $school]);
        return $this->render('candidate/process-school.html.twig', [
            'candidate' => $candidate,
            'escuela' => $school,
            'stage' => $stages,
            'process' => $stagesCandidate,
            'info' => $stageInfo
        ]);
    }


    /**
     * @Route("/{id}", name="app_candidate_delete", methods={"POST"})
     */
    public function delete(Request $request, Candidate $candidate, EntityManagerInterface $entityManager): Response
    {
        if ($this->isCsrfTokenValid('delete' . $candidate->getId(), $request->request->get('_token'))) {
            $entityManager->remove($candidate);
            $entityManager->flush();
        }

        return $this->redirectToRoute('app_candidate_index', [], Response::HTTP_SEE_OTHER);
    }

    /**
     * @Route("/{id}/update-status", name="app_candidate_update_delete", methods={"GET", "POST"})
     */
    public function updateStatus(Request $request, Candidate $candidate, EntityManagerInterface $entityManager): Response
    {

        $candidate->setStatus(2);
        $entityManager->flush();
        return $this->redirectToRoute('app_candidate_index', [], Response::HTTP_SEE_OTHER);
    }

    /**
     * @Route("/consulta-process/{processId}", name="consulta_process", methods={"GET"})
     */
    public function consultaAjax(Request $request, $processId, ProcessRepository $processRepository, EntityManagerInterface $entityManager, ProcessManager $processManager): Response
    {

        $process = $processRepository->find($processId);
        if (!$process) {
            throw $this->createNotFoundException('El proceso no fue encontrado.');
        }
        $html = $this->twig->render('candidate/process-school-result.html.twig', [
            'process' => $process,
        ]);
        return new JsonResponse(['html' => $html]);
    }

    /**
     * @Route("/{id}/proceso/{fieldId}", name="app_candidate_process_answer", methods={"POST"})
     */
    public function candidateSchoolProcessWithProcessId(
        Request                  $request,
        Candidate                $candidate,
                                 $fieldId,
        EntityManagerInterface   $entityManager,
        CandidateStageRepository $candidateStageRepository,
        StageRepository          $stageRepository,
        SchoolRepository         $schoolRepository,
        ProcessFieldRepository   $processFieldRepository,
        ProcessRepository        $processRepository,
        CandidateFieldRepository $candidateFieldRepository
    ): Response
    {
        $candidateField = $candidateFieldRepository->findOneBy([
            'candidate' => $candidate,
            'field' => $fieldId
        ]);
        if ($candidateField) {
            $candidateField->setAnswer($request->request->get('answer'));
            $entityManager->flush();
        } else {
            $candidateField = new CandidateField();
            $candidateField->setCandidate($candidate);
            $fiel = $processFieldRepository->find($fieldId);
            $candidateField->setField($fiel);
            $candidateField->setAnswer($request->request->get('answer'));
            $entityManager->persist($candidateField);
            $entityManager->flush();
        }
        return new JsonResponse(['success' => true]);
    }


    /**
     * @Route("/{id}/answer/{fieldId}", name="update_answer_approved", methods={"POST"})
     */
    public function updateApproved(
        Request                  $request,
        Candidate                $candidate,
                                 $fieldId,
        EntityManagerInterface   $entityManager,
        CandidateStageRepository $candidateStageRepository,
        StageRepository          $stageRepository,
        SchoolRepository         $schoolRepository,
        ProcessRepository        $processRepository,
        ProcessFieldRepository   $processFieldRepository,
        CandidateFieldRepository $candidateFieldRepository
    ): Response
    {

        $candidateField = $candidateFieldRepository->findOneBy([
            'candidate' => $candidate,
            'field' => $fieldId
        ]);


        if ($candidateField) {

            $candidateField->setAccept($request->request->get('approved'));
            $entityManager->flush();
        } else {
            $candidateField = new CandidateField();
            $candidateField->setCandidate($candidate);
            $fiel = $processFieldRepository->find($fieldId);
            $candidateField->setField($fiel);
            $candidateField->setAccept($request->request->get('approved'));
            $entityManager->persist($candidateField);
            $entityManager->flush();
        }
        return new Response('La etapa ha sido actualizada correctamente', Response::HTTP_OK);
    }


    /**
     * @Route("/{id}/document/{documentId}", name="upload_document", methods={"POST"})
     */
    public function uploadDocument(
        Request                    $request,
        Candidate                  $candidate,
                                   $documentId,
        EntityManagerInterface     $entityManager,
        CanidateDocumentRepository $canidateDocumentRepository
    ): Response
    {
        $candidateDocument = $canidateDocumentRepository->findOneBy([
            'candidate' => $candidate,
            'document' => $documentId
        ]);

        if (!$candidateDocument) {
            $doc = $entityManager->getRepository(Document::class)->find($documentId);
            $candidateDocument = new CandidateDocument();
            $candidateDocument->setCandidate($candidate);
            $candidateDocument->setDocument($doc);
        }

        $file = $request->files->get('file');
        if ($file instanceof UploadedFile) {
            $candidateDocument->setDocumentFile($file);
            $entityManager->persist($candidateDocument);
            $entityManager->flush();

            return new JsonResponse(['message' => 'Documento actualizado correctamente'], JsonResponse::HTTP_OK);
        }

        return new JsonResponse(['message' => 'No se proporcionó ningún archivo'], JsonResponse::HTTP_BAD_REQUEST);
    }

    /**
     * @Route("/{id}/candidate/{schoolId}", name="candidate_delete_record", methods={"GET", "POST"})
     */
    public function deleteRecordCandidate(
        Request                  $request,
        Candidate                $candidate,
        SchoolRepository         $schoolRepository,
        StageRepository          $stageRepository,
        CandidateStageRepository $candidateStageRepository,
        EntityManagerInterface   $entityManager,
                                 $schoolId
    ): Response
    {
        $school = $schoolRepository->find($schoolId);
        if ($candidate->getSchool()->contains($school)) {

            $candidate->removeSchool($school);
            $candidateStages = $candidateStageRepository->findBy([
                'candidate' => $candidate,
                'school' => $school
            ]);
            $stages = $stageRepository->findBy([
                'candidate' => $candidate,
                'school' => $school
            ]);
            foreach ($candidateStages as $candidateStage) {
                $entityManager->remove($candidateStage);
            }
            foreach ($stages as $stage) {

                $entityManager->remove($stage);
            }
            $entityManager->flush();
        }
        return $this->redirectToRoute('app_candidate_index');
    }
}
