<?php

namespace App\Controller;

use App\Entity\ProcessField;
use App\Form\ProcessFieldType;
use App\Repository\ProcessFieldRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/process/field")
 */
class ProcessFieldController extends AbstractController
{
    /**
     * @Route("/", name="app_process_field_index", methods={"GET"})
     */
    public function index(ProcessFieldRepository $processFieldRepository): Response
    {
        return $this->render('process_field/index.html.twig', [
            'process_fields' => $processFieldRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="app_process_field_new", methods={"GET", "POST"})
     */
    public function new(Request $request, EntityManagerInterface $entityManager): Response
    {
        $processField = new ProcessField();
        $form = $this->createForm(ProcessFieldType::class, $processField);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($processField);
            $entityManager->flush();

            return $this->redirectToRoute('app_process_field_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('process_field/new.html.twig', [
            'process_field' => $processField,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="app_process_field_show", methods={"GET"})
     */
    public function show(ProcessField $processField): Response
    {
        return $this->render('process_field/show.html.twig', [
            'process_field' => $processField,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="app_process_field_edit", methods={"GET", "POST"})
     */
    public function edit(Request $request, ProcessField $processField, EntityManagerInterface $entityManager): Response
    {
        $form = $this->createForm(ProcessFieldType::class, $processField);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();

            return $this->redirectToRoute('app_process_field_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('process_field/edit.html.twig', [
            'process_field' => $processField,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="app_process_field_delete", methods={"POST"})
     */
    public function delete(Request $request, ProcessField $processField, EntityManagerInterface $entityManager): Response
    {
        if ($this->isCsrfTokenValid('delete'.$processField->getId(), $request->request->get('_token'))) {
            $entityManager->remove($processField);
            $entityManager->flush();
        }

        return $this->redirectToRoute('app_process_field_index', [], Response::HTTP_SEE_OTHER);
    }
}
