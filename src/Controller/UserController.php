<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\UserType;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * @Route("/user")
 */
class UserController extends AbstractController
{

    /**
     * @Route("/", name="app_user_index", methods={"GET"})
     * @IsGranted("ROLE_ADMIN")
     */
    public function index(EntityManagerInterface $entityManager, UserRepository $userRepository): Response
    {
        $title = 'Operadores';
        $users = $userRepository->findAllByRole("ROLE_OPERADOR");
        return $this->render('user/index.html.twig', [
            'users' => $users,
            'title' => $title,
        ]);
    }

    /**
     * @Route("/new", name="app_user_new", methods={"GET", "POST"})
     * @IsGranted("ROLE_ADMIN")
     *
     */
    public function new(Request $request, EntityManagerInterface $entityManager, UserPasswordEncoderInterface $passwordEncoder): Response
    {
        $user = new User();
        $user->setStatus(1);
        $user->setRoles(['ROLE_OPERADOR']);
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $user->setPassword(
                $passwordEncoder->encodePassword(
                    $user,
                    $form->get('plainPassword')->getData()
                )
            );
            $this->addFlash('success', 'Operador agregado correctamente.');
            $entityManager->persist($user);
            $entityManager->flush();
            return $this->redirectToRoute('app_user_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('user/new.html.twig', [
            'user' => $user,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="app_user_show", methods={"GET"})
     * @IsGranted("ROLE_ADMIN")
     *
     */
    public function show(User $user): Response
    {
        $form = $this->createForm(UserType::class, $user, ['readonly' => true]);
        return $this->render('user/show.html.twig', [
            'user' => $user,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}/edit", name="app_user_edit", methods={"GET", "POST"})
     * @IsGranted("ROLE_ADMIN")
     *
     */
    public function edit(Request $request, User $user, EntityManagerInterface $entityManager,  UserPasswordEncoderInterface $passwordEncoder): Response
    {
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->addFlash('success', 'Operador actualizado correctamente.');
            if ($user instanceof User && $user->getPlainPassword()) {
                $user->setPassword(
                    $passwordEncoder->encodePassword(
                        $user,
                        $form->get('plainPassword')->getData()
                    )
                );
            }
            $entityManager->flush();

            return $this->redirectToRoute('app_user_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('user/edit.html.twig', [
            'user' => $user,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="app_user_delete", methods={"POST"})
     * @IsGranted("ROLE_ADMIN")
     *
     */
    public function delete(Request $request, User $user, EntityManagerInterface $entityManager): Response
    {
        if ($this->isCsrfTokenValid('delete' . $user->getId(), $request->request->get('_token'))) {
            $this->addFlash('success', 'Usuario eliminado correctamente.');
            $entityManager->remove($user);
            $entityManager->flush();
        }

        return $this->redirectToRoute('app_user_index', [], Response::HTTP_SEE_OTHER);
    }

    /**
     * @Route("/{id}/pausa", name="app_user_pause")
     * @IsGranted("ROLE_ADMIN")
     *
     */
    public function pause(Request $request, User $user, EntityManagerInterface $entityManager): Response
    {
        $this->denyAccessUnlessGranted('ROLE_USER');
        $user->setStatus(2);
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);
        $entityManager->flush();
        $this->addFlash('success', 'Usuario pausado correctamente.');
        return $this->redirectToRoute('app_user_index', [], Response::HTTP_SEE_OTHER);
    }

    /**
     * @Route("/{id}/suspendido", name="app_user_suspendido")
     * @IsGranted("ROLE_ADMIN")
     *
     */
    public function suspendido(Request $request, User $user, EntityManagerInterface $entityManager): Response
    {
        $this->denyAccessUnlessGranted('ROLE_USER');
        $user->setStatus(3);
        $form = $this->createForm(UserType::class, $user);
        $this->addFlash('success', 'Usuario suspendido correctamente.');
        $form->handleRequest($request);
        $entityManager->flush();
        return $this->redirectToRoute('app_user_index', [], Response::HTTP_SEE_OTHER);
    }

    /**
     * @Route("/{id}/activo", name="app_user_activo")
     * @IsGranted("ROLE_ADMIN")
     *
     */
    public function activo(Request $request, User $user, EntityManagerInterface $entityManager): Response
    {
        $this->denyAccessUnlessGranted('ROLE_USER');
        $user->setStatus(1);
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);
        $entityManager->flush();
        $this->addFlash('success', 'Usuario actualizado correctamente.');
        return $this->redirectToRoute('app_user_index', [], Response::HTTP_SEE_OTHER);
    }
}
