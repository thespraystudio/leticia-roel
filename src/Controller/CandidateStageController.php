<?php

namespace App\Controller;

use App\Entity\Stage;
use App\Entity\Candidate;
use App\Form\CandidateStageType;
use App\Form\StageType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Services\StageManager;


/**
 * @Route("/candidate-stage")
 */
class CandidateStageController extends AbstractController
{


    /**
     * @Route("/{id}/{candidate}", name="app_candidate_stage", methods={"GET", "POST"})
     */
    public function editStage(Request $request, Stage $stage, EntityManagerInterface $entityManager, StageManager $stageManager, Candidate $candidate): Response
    {


        $form = $this->createForm(StageType::class, $stage, [
            'attr' => ['id' => 'candidateStageFrom'],
        ]);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            foreach ($stage->getProcesses() as $process) {
                $process->setCandidate($candidate);
                foreach ($process->getChildren() as $subProcess) {
                    $subProcess->setCandidate($candidate);
                }
            }
            $entityManager->persist($stage);
            $entityManager->flush();
        }
        return $this->render('candidate_stage/index.html.twig', [
            'candidate' => $stage,
            'form' => $form->createView(),
        ]);
    }
}
