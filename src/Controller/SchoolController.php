<?php

namespace App\Controller;

use App\Entity\School;
use App\Entity\Task\SubTask;
use App\Entity\Task\Task;
use App\Entity\Process;
use App\Filters\SchoolFilter;
use App\Form\Filter\SchoolFilterType;
use App\Form\SchoolType;
use App\Entity\SchoolInformation;
use App\Form\StageType;
use App\Form\ProcessType;
use App\Form\Task\TaskType;
use App\Repository\SchoolRepository;
use App\Services\Exporter\SchoolGeneratorHelper;
use App\Services\FiltersHelper;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Services\StageManager;
use Symfony\UX\Turbo\TurboBundle;

use App\Entity\Stage;
use Twig\Environment;


/**
 * @Route("/school")
 */
class SchoolController extends AbstractController
{

    private $stageManager;

    public function __construct(StageManager $stageManager)
    {
        $this->stageManager = $stageManager;
    }

    /**
     * @Route("/", name="app_school_index", methods={"GET","POST"})
     */
    public function index(SchoolFilter $schoolFilter, SchoolRepository $schoolRepository, FiltersHelper $filtersHelper): Response
    {
        $filters = $filtersHelper->getParameters($schoolFilter->getName());
        $filterForm = $this->createForm(SchoolFilterType::class, $filters);
        $entities = $schoolRepository->filterSchools($filters);
        return $this->render('school/index.html.twig', [
            'filterForm' => $filterForm->createView(),
            'schools' => $entities,
        ]);
    }

    /**
     * @Route("/export", name="app_school_export", methods={"GET"})
     */
    public function export(FiltersHelper $filtersHelper, SchoolFilter $schoolFilter, SchoolRepository $schoolRepository, SchoolGeneratorHelper $schoolGeneratorHelper): Response
    {
        $filters = $filtersHelper->getParameters($schoolFilter->getName());
        $entitiesArray = $schoolRepository->filterSchools($filters);
        $ids = [];
        foreach ($entitiesArray as $entity) {
            $ids[] = $entity['id'];
        }
        $entities = $schoolRepository->searchIds($ids);
        return $schoolGeneratorHelper->generate($entities);
    }

    /**
     * @Route("/new", name="app_school_new", methods={"GET", "POST"})
     */
    public function new(Request $request, EntityManagerInterface $entityManager): Response
    {
        $school = new School();


        $SchoolInformation = new SchoolInformation();
        $school->addDescription($SchoolInformation);
        $form = $this->createForm(SchoolType::class, $school, [
            'entity_manager' => $entityManager,
        ]);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->addFlash('success', 'Oferta agregada correctamente.');
            if (!empty($school->getStages())) {
                foreach ($school->getStages() as $stage) {
                    $stage->setTemplate(true);
                }
            }
            $entityManager->persist($school);
            $entityManager->flush();
            return $this->redirectToRoute('app_school_index', [], Response::HTTP_SEE_OTHER);
        }
        return $this->render('school/new.html.twig', [
            'school' => $school,
            'form' => $form->createView(),
        ]);
    }


    /**
     * @Route("/{id}", name="app_school_show", methods={"GET"})
     */
    public function show(School $school): Response
    {


        return $this->render('school/oportunidad.html.twig', [
            'school' => $school,

        ]);
    }

    /**
     * @Route("/{id}/edit", name="app_school_edit", methods={"GET", "POST"})
     */
    public function edit(Request $request, School $school, EntityManagerInterface $entityManager): Response
    {
        $schoolInfo = $this->getDoctrine()->getRepository(SchoolInformation::class)->findBy(array('school' => $school->getId()));
        if (empty($schoolInfo)) {
            $SchoolInformation = new SchoolInformation();
            $school->addDescription($SchoolInformation);
        }

        $unfilteredStages = $school->getStages()->filter(function (Stage $stage) {
            return $stage->getTemplate() === false;
        });
        $form = $this->createForm(SchoolType::class, $school, [
            'attr' => ['id' => 'schoolFrom'],
            'entity_manager' => $entityManager,
            'school_id' => $school->getId(),
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->addFlash('success', 'Oferta actualizada correctamente.');

            if (!empty($school->getStages())) {
                foreach ($school->getStages() as $stage) {
                    $stage->setTemplate(true);
                }
                foreach ($unfilteredStages as $unfilteredStage) {
                    $school->addStage($unfilteredStage);
                }
            }
            $entityManager->persist($school);
            $entityManager->flush();
            $candidates = $school->getCanditates();
            return $this->redirectToRoute('app_school_edit', ['id' => $school->getId()]);
            // return new JsonResponse(['success' => true]);
        }
        $this->addFlash('error', 'Error al actualizar la información.');

        return $this->render('school/edit.html.twig', [
            'school' => $school,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="app_school_delete", methods={"POST"})
     */
    public function delete(Request $request, School $school, EntityManagerInterface $entityManager): Response
    {
        if ($this->isCsrfTokenValid('delete' . $school->getId(), $request->request->get('_token'))) {
            $entityManager->remove($school);
            $this->addFlash('success', 'Oferta eliminada correctamente.');

            $entityManager->flush();
        }
        return $this->redirectToRoute('app_school_index', [], Response::HTTP_SEE_OTHER);
    }

    /**
     * @Route("/{id}/add/stage", name="app_school_add_stage", methods={"GET", "POST"})
     */
    public function stage(Environment $twig, Request $request, School $school, EntityManagerInterface $entityManager): Response
    {
        $stage = new Stage();
        $form = $this->createForm(StageType::class, $stage);
        $form->handleRequest($request);
        $stage->setSchool($school);
        $stage->setTemplate(true);
        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($stage);
            $entityManager->flush();
            if (TurboBundle::STREAM_FORMAT === $request->getPreferredFormat()) {
                $request->setRequestFormat(TurboBundle::STREAM_FORMAT);
                $template = $twig->load('school/_form.html.twig');
                $block = $template->renderBlock(
                    'success_stream',
                    [
                        'stage' => $stage,
                        'school' => $school,
                    ]
                );
                return new Response($block);
            }
            return $this->redirectToRoute('app_school_edit', ['id' => $school->getId()]);
        }
        return $this->render('stage/_form.html.twig', [
            'form' => $form->createView(),
            'stage' => $stage,
            'school' => $school
        ]);
    }

    /**
     * @Route("/school/stage/{id}/edit", name="app_school_edit_stage", methods={"GET", "POST"})
     */
    public function editStage(Stage $stage, Request $request, EntityManagerInterface $em): Response
    {
        $form = $this->createForm(StageType::class, $stage);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->flush();
            return $this->redirectToRoute('app_some_route');
        }
        return $this->render('stage/_form_edit.html.twig', [
            'form' => $form->createView(),
            'stage' => $stage,
        ]);
    }


    /**
     * @Route("/school/stage/{stageId}/process/new", name="app_school_add_processe", methods={"GET", "POST"})
     */
    public function newProcess(int $stageId, Request $request, EntityManagerInterface $em): Response
    {

        $stage = $em->getRepository(Stage::class)->find($stageId);
        if (!$stage) {
            throw $this->createNotFoundException('Stage not found');
        }

        $process = new Process();
        $process->setStage($stage);

        $form = $this->createForm(ProcessType::class, $process);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $school = $stage->getSchool();
            $em->persist($process);
            $em->flush();
            return $this->redirectToRoute('app_school_edit', ['id' => $school->getId()]);
        }

        return $this->render('process/processe_form.html.twig', [
            'form' => $form->createView(),
            'stageId' => $stageId
        ]);
    }


    /**
     * @Route("/school/process/{id}/edit", name="app_school_edit_processe", methods={"GET", "POST"})
     */
    public function editProcess(Process $process, Request $request, EntityManagerInterface $em): Response
    {
        $form = $this->createForm(ProcessType::class, $process);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->flush();
            return $this->redirectToRoute('app_school_show', ['id' => $process->getStage()->getId()]);
        }

        return $this->render('process/processe_form.html.twig', [
            'form' => $form->createView(),
            'processe' => $process
        ]);
    }

    /**
     * @Route("/school/process/{processId}/subprocess/new", name="app_school_add_subprocess", methods={"GET", "POST"})
     */
    public function newSubProcess(int $processId, Request $request, EntityManagerInterface $em): Response
    {
        $parentProcess = $em->getRepository(Process::class)->find($processId);
        if (!$parentProcess) {
            throw $this->createNotFoundException('Process not found');
        }

        $subProcess = new Process();
        $subProcess->setProcess($parentProcess); // Establece el proceso padre correctamente

        $form = $this->createForm(ProcessType::class, $subProcess);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($subProcess);
            $em->flush();
            return $this->redirectToRoute('app_school_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('process/sub_processe_form.html.twig', [
            'form' => $form->createView(),
            'processe' => $subProcess,
            'processId' => $processId
        ]);
    }


    /**
     * @Route("/school/subprocess/{id}/edit", name="app_school_edit_subprocess", methods={"GET", "POST"})
     */
    public function editSubProcess(Process $subProcess, Request $request, EntityManagerInterface $em): Response
    {
        $form = $this->createForm(ProcessType::class, $subProcess);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->flush();
            return $this->redirectToRoute('app_school_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('process/edit_sub_processe_form.html.twig', [
            'form' => $form->createView(),
            'processe' => $subProcess
        ]);
    }
}
