<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\HttpFoundation\RequestStack;

class HomeController extends AbstractController
{
    private $security;
    private $requestStack;


    public function __construct(Security $security, RequestStack $requestStack)
    {
        $this->security = $security;
        $this->requestStack = $requestStack;
    }

    /**
     * @Route("/", name="app_home")
     */
    public function index(): Response
    {
        if ($this->security->isGranted('ROLE_ADMIN')) {
            return $this->redirectToRoute('app_user_index', [], Response::HTTP_SEE_OTHER);
        }
        return $this->redirectToRoute('app_candidate_index', [], Response::HTTP_SEE_OTHER);
    }

    /**
     * @Route("/menu", name="menu")
     */
    public function menu(Request $request): Response
    {
        // $currentPath = $this->requestStack->getCurrentRequest()->attributes->get('_route');
        // dd($currentPath);

        $menu = array( 
            "operadores" => array(
                'label' => 'Operadores',
                "icon" => "_otro",
                "route" => "app_user_index", 
                "required_role" => ["ROLE_ADMIN"]
            ),
            "aspirantes/Procesos" => array(
                'label' => 'Aspirantes/Procesos',
                "icon" => "_user",
                "route" => "app_candidate_index",
                "required_role" => ["ROLE_ADMIN", "ROLE_OPERADOR", "ROLE_TUTOR"]
            ),
            "ofertas" => array(
                'label' => 'Ofertas',
                "icon" => "_ofertas",
                "route" => "app_school_index",
                "required_role" => ["ROLE_ADMIN"]
            ),
        );
        $referer = $request->headers->get('referer');

        $menuHelper = array(
            "inicio" => array(
                'label' => 'Inicio',
                "icon" => "ini",
                "route" => "app_candidate_index",
                "required_role" => ["ROLE_ADMIN", "ROLE_OPERADOR", "ROLE_TUTOR"],
                "css" => ""
            ),
            "regresar" => array(
                'label' => 'Regresar',
                "icon" => "back",
                "routePath" => '#',
                "required_role" => ["ROLE_ADMIN", "ROLE_OPERADOR", "ROLE_TUTOR"],
                "css" => "btnRegresar-js"
            ),
        );


        $user = $this->security->getUser();
        $roles = $user ? $user->getRoles() : [];
        $filteredMenu = $this->filterMenuByRoles($menu, $roles);

        return $this->render('home/menu.html.twig', [
            'menu' => $filteredMenu,
            'menuHelper' => $menuHelper,
        ]);
    }

    private function filterMenuByRoles(array $menu, array $roles): array
    {
        $filteredMenu = [];
        foreach ($menu as $key => $item) {
            if (empty($item['required_role']) || array_intersect($item['required_role'], $roles)) {
                if ($key === "operadores" && in_array("ROLE_OPERADOR", $roles)) {
                    continue;
                }
                $filteredMenu[$key] = $item;
            }
        }

        return $filteredMenu;
    }
}
