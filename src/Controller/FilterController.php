<?php

namespace App\Controller;


use App\Services\FiltersHelper;
use App\Services\FiltersManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("filter")
 */
class FilterController extends AbstractController
{
    /**
     * @Route("/filters/{filterName}/{routeName}", name="filters")
     */
    public function filters(
        string         $filterName,
        Request        $request,
        FiltersHelper  $filtersHelper,
        FiltersManager $filtersManager,
        string         $routeName = null
    ): Response
    {
        $filter = $filtersManager->createFilter($filterName);
        $temporalData = $filter->getFilterEntity();
        $object = $filtersHelper->getParameters($filterName) ? $filtersHelper->getParameters($filterName) : $temporalData;
        $form = $this->createForm($filter->getFormClass(), $object, ['action' => $this->generateUrl('filters', ['filterName' => $filterName, 'routeName' => $routeName])]);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $filtersHelper->setParameters($form->getData(), $filterName);
            return $this->redirectToRoute($routeName);
        }
        return $this->render('filters/_filters.html.twig', [
            'filterForm' => $form->createView(),
            'filterName' => $filterName
        ]);
    }

}
