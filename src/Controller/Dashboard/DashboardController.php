<?php


namespace App\Controller\Dashboard;


use App\Entity\User;
use EasyCorp\Bundle\EasyAdminBundle\Controller\EasyAdminController as BaseController;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class DashboardController extends BaseController
{
    private UserPasswordEncoderInterface $encoder;

    public function createEntityForm($entity, array $entityProperties, $view)
    {

        $form = parent::createEntityForm($entity, $entityProperties, $view);
        $form->remove('created');
        $form->remove('updated');


        if($entity instanceof User){
            $form->add(
                'roles', ChoiceType::class, [
                'choices' => ['Administrador' => 'ROLE_ADMIN','Operador' => 'ROLE_OPERADOR','Tutor' => 'ROLE_TUTOR','Usuario' => 'ROLE_USER'],
                'expanded' => true,
                'multiple' => true,
            ]);
        }
        return $form;
    }


    private function setUserPlainPassword(User $user): void
    {
        if ($user->getPlainPassword()) {
            $user->setPassword($this->encoder->encodePassword($user, $user->getPlainPassword()));
        }
    }

    /**
     * @required
     */
    public function setEncoder(UserPasswordEncoderInterface $encoder): void
    {
        $this->encoder = $encoder;
    }

    public function persistUserEntity(User $user): void
    {
        $this->setUserPlainPassword($user);

        $this->persistEntity($user);
    }

    public function updateUserEntity(User $user): void
    {
        $this->setUserPlainPassword($user);

        $this->updateEntity($user);
    }

}