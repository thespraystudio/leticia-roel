<?php

namespace App\Controller;

use App\Entity\Stage;
use App\Form\Stage1Type;


use App\Entity\Process;
use App\Entity\School;


use App\Repository\StageRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\ORM\EntityManagerInterface;


/**
 * @Route("/stage/new")
 */
class StageNewController extends AbstractController
{
    /**
     * @Route("/", name="app_stage_new_index", methods={"GET"})
     */
    public function index(StageRepository $stageRepository): Response
    {
        return $this->render('stage_new/index.html.twig', [
            'stages' => $stageRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="app_stage_new_new", methods={"GET", "POST"})
     */
    public function new(Request $request, EntityManagerInterface $entityManager): Response
    {
        $stage = new Stage();
        $form = $this->createForm(StageType::class, $stage);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($stage);
            $entityManager->flush();

            return $this->redirectToRoute('app_stage_new_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('stage_new/new.html.twig', [
            'stage' => $stage,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="app_stage_new_show", methods={"GET"})
     */
    public function show(Stage $stage): Response
    {
        return $this->render('stage_new/show.html.twig', [
            'stage' => $stage,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="app_stage_new_edit", methods={"GET", "POST"})
     */
    public function edit(Request $request, Stage $stage, EntityManagerInterface $entityManager): Response
    {
        $form = $this->createForm(Stage1Type::class, $stage);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();

            return $this->redirectToRoute('app_stage_new_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('stage_new/edit.html.twig', [
            'stage' => $stage,
            'form' => $form->createView(),
        ]);
    }
//
//    /**
//     * @Route("/{id}", name="app_stage_new_delete", methods={"POST"})
//     */
//    public function delete(Request $request, Stage $stage, EntityManagerInterface $entityManager): Response
//    {
//        if ($this->isCsrfTokenValid('delete'.$stage->getId(), $request->request->get('_token'))) {
//            $entityManager->remove($stage);
//            $entityManager->flush();
//        }
//
//        return $this->redirectToRoute('app_stage_new_index', [], Response::HTTP_SEE_OTHER);
//    }
}
