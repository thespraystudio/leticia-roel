<?php


namespace App\EventListener;


use App\Controller\MobileController;
use App\Controller\SelectorController;
use App\Exception\RedirectException;
use App\Services\ConfigurationHelper;
use App\Services\UserHelper;
use Detection\MobileDetect;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Event\ControllerEvent;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Routing\Router;
use Symfony\Component\Routing\RouterInterface;

class MobileControllerListener implements EventSubscriberInterface
{
    /**
     * @var Router
     */
    private $router;
    private MobileDetect $mobileDetect;


    /**
     * @inheritDoc
     */
    public static function getSubscribedEvents()
    {
        return [KernelEvents::CONTROLLER => ['onKernelResponsePre'], KernelEvents::EXCEPTION => ['onKernelException']];
    }

    public function __construct(RouterInterface $router)
    {
        $this->router = $router;
        $this->mobileDetect = new MobileDetect();
    }

    public function onKernelException(ExceptionEvent $event)
    {
        $exception = $event->getThrowable();
        if ($exception instanceof RedirectException) {
            /** @var RedirectException $exception */
            $event->setResponse($exception->getRedirectResponse());
        }
    }

    public function onKernelResponsePre(ControllerEvent $event)
    {
        $controllerData = $event->getController();
        if (!is_array($controllerData)) {
            return;
        }

        if (!$controllerData[0] instanceof MobileController && $this->mobileDetect->isMobile()) {
            throw new RedirectException(new RedirectResponse($this->router->generate('app_mobile')));
        }
    }
}