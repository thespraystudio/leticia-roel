<?php


namespace App\EventListener;

use Doctrine\ORM\Event\LifecycleEventArgs;

use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Doctrine\ORM\Events;
use App\Entity\Process;
use App\Entity\ProcessField;
use App\Entity\Document;
use App\Entity\Stage;
use App\Entity\School;

class CustomEntityChangeListener implements EventSubscriber
{
    public function getSubscribedEvents()
    {
        return [
            Events::preUpdate,
            Events::preRemove,
            Events::prePersist,


        ];
    }

    public function prePersist(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();

        if ($entity instanceof Stage || $entity instanceof Process || $entity instanceof ProcessField || $entity instanceof Document) {
            
        }
    }

    public function preRemove(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();

        if ($entity instanceof Stage ||$entity instanceof Process || $entity instanceof ProcessField || $entity instanceof Document) {
            
        }
    }

    public function preUpdate(PreUpdateEventArgs $eventArgs)
    {
        $entity = $eventArgs->getEntity();
        $entityManager = $eventArgs->getEntityManager();
        $changeset = $eventArgs->getEntityChangeSet();

        if ($entity instanceof School || $entity instanceof Stage ||$entity instanceof Process ||$entity instanceof ProcessField ||$entity instanceof Document) {
            foreach ($changeset as $fieldName => $change) {
                
            }
        }
    }
}
