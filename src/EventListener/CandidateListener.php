<?php

namespace App\EventListener;

use App\Entity\Candidate;
use App\Entity\CandidateField;
use App\Entity\CandidateDocument;
use App\Entity\Notification;
use App\Entity\User;
use App\Services\StageManager;
use App\Utils\Base64FileExtractor;
use App\Utils\UploadedBase64File;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Events;
use Doctrine\Persistence\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class CandidateListener implements EventSubscriber
{
    protected StageManager $stageManager;
    private Base64FileExtractor $base64FileExtractor;
    private MailerInterface $mailer;
    private UserPasswordEncoderInterface $passwordEncoder;
    private EntityManagerInterface $entityManager;

    public function __construct(
        MailerInterface              $mailer,
        Base64FileExtractor          $base64FileExtractor,
        UserPasswordEncoderInterface $passwordEncoder,
        EntityManagerInterface       $entityManager,
        StageManager                 $stageManager
    )
    {
        $this->stageManager = $stageManager;
        $this->base64FileExtractor = $base64FileExtractor;
        $this->mailer = $mailer;
        $this->passwordEncoder = $passwordEncoder;
        $this->entityManager = $entityManager;
    }

    public function getSubscribedEvents()
    {
        return [
            Events::postPersist,
            Events::prePersist,
            Events::preUpdate,
            Events::postUpdate
        ];
    }

    public function prePersist(LifecycleEventArgs $args)
    {
        $entity = $args->getObject();

        if (!$entity instanceof Candidate) {
            return;
        }

        if ($entity->getImage() !== null) {
            $this->transformImage($entity);
        }
    }

    protected function transformImage(Candidate $candidate)
    {
        if ($this->base64FileExtractor->isValidToExtract($candidate->getImage())) {
            $imageName = $this->base64FileExtractor->extractImageName($candidate->getImage());
            $base64Image = $this->base64FileExtractor->extractBase64String($candidate->getImage());
            $imageFile = new UploadedBase64File($base64Image, $imageName);
            $candidate->setImageFile($imageFile);
        }
    }

    public function preUpdate(PreUpdateEventArgs $eventArgs)
    {
    }

    public function postPersist(LifecycleEventArgs $args)
    {
        $entity = $args->getObject();

        if ($entity instanceof CandidateField || $entity instanceof CandidateDocument) {
            $this->handleEvent($args);
        }

        if ($entity instanceof Candidate) {
            $this->createNewUserIfNotExists($entity);
        }
    }

    public function postUpdate(LifecycleEventArgs $eventArgs)
    {
    }

    private function handleEvent(LifecycleEventArgs $args)
    {
        $entityManager = $args->getObjectManager();
        $entity = $args->getObject();

        if ($entity instanceof CandidateField || $entity instanceof CandidateDocument) {
            $candidate = $entity->getCandidate();

            if ($candidate) {
                $candidateName = $candidate->getName();
                $candidateLastName = $candidate->getLastname();
                $notificationBody = sprintf('Actualizacion %s %s', $candidateName, $candidateLastName);
                $adminUsers = $this->entityManager->getRepository(User::class)->findByRoles('ROLE_ADMIN');
                $relatedUsers = $candidate->getOwner();

                foreach ($adminUsers as $adminUser) {
                    $notification = new Notification();
                    $notification->setTitle('Nueva notificación');
                    $notification->setBody($notificationBody);
                    $notification->addUser($adminUser);
                    $entityManager->persist($notification);
                }

                if ($relatedUsers) {
                    $notification = new Notification();
                    $notification->setTitle('Nueva notificación');
                    $notification->setBody($notificationBody);
                    $notification->addUser($relatedUsers);
                    $entityManager->persist($notification);

                    try {
                        $entityManager->flush();
                    } catch (\Exception $e) {
                        echo 'Error al guardar la notificación: ' . $e->getMessage();
                    }
                }
            }
        }
    }

    private function createNewUserIfNotExists(Candidate $candidate): User
    {
        if ($candidate->getType() == 2 || ($candidate->getType() == 1 && $candidate->getCandidate() === null)) {
            $email = $candidate->getEmail();
        } else {
            $email = $candidate->getCandidate()->getEmail();

            $this->addUserToCandidate($candidate, $email);
        }
        $existingUser = $this->entityManager->getRepository(User::class)->findOneBy(['email' => $email]);

        if (!$existingUser) {
            $newUser = new User();
            $newUser->setStatus(1);
            $newUser->setUsername($candidate->getEmail());
            $newUser->setEmail($candidate->getEmail());
            $newUser->setRoles(['ROLE_TUTOR']);
            $newUser->setName($candidate->getName());
            $newUser->setLastname($candidate->getLastname());
            $newUser->setPhoneNumber($candidate->getPhoneNumber());
            $newUser->setImage($candidate->getImage());
            $this->addCandidatesToUser($newUser, $candidate);
            $plainPassword = $this->generateRandomPassword();
            $newUser->setPlainPassword($plainPassword);
            $encodedPassword = $this->passwordEncoder->encodePassword($newUser, $plainPassword);
            $newUser->setPassword($encodedPassword);
            $this->entityManager->persist($newUser);
            $this->entityManager->flush();
            $this->sendWelcomeEmail($newUser);
            return $newUser;
        } else {
            /** @var User $existingUser */
            if (!$existingUser->getImage()) {
                $existingUser->setImage($candidate->getImage());
            }
            $this->addCandidatesToUser($existingUser, $candidate);
            return $existingUser;
        }


        return $existingUser;
    }

    private function sendWelcomeEmail(User $user): void
    {
        $email = (new Email())
            ->from('karim@spraystudio.com.mx')
            ->to($user->getEmail())
            ->cc('gerardo.179.cruz@gmail.com')
            ->bcc('mariokarim1109@gmail.com')
            ->subject('Accesos a la plataforma')
            ->text('Usuario: ' . $user->getEmail() . ' ' . ' Contrasesña: ' . $user->getPlainPassword());
        $this->mailer->send($email);
    }

    private function addCandidatesToUser(User $newUser, Candidate $candidate): void
    {
        $candidateType = $candidate->getType();
        if ($candidateType == 1) {
            $newUser->addCandidate($candidate);
        } elseif ($candidateType == 2) {
            $matchingCandidates = $this->entityManager->getRepository(Candidate::class)->findBy(['id' => $candidate->getId()]);
            foreach ($matchingCandidates as $matchingCandidate) {
                $newUser->addCandidate($matchingCandidate);
            }
        }
    }

    private function addUserToCandidate(Candidate $candidate, $tutor): void
    {
        $UserTutor = $this->entityManager->getRepository(User::class)->findOneBy(['email' => $tutor]);
        if ($UserTutor instanceof User) {
            $candidate->setOwner($UserTutor);
            $this->entityManager->persist($candidate);
            $this->entityManager->flush();
        }
    }

    private function generateRandomPassword(): string
    {
        return bin2hex(random_bytes(8));
    }
}
