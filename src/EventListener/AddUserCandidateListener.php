<?php


namespace App\EventListener;


use ApiPlatform\Core\EventListener\EventPriorities;
use App\Entity\Candidate;
use App\Entity\User;
use App\Services\StageManager;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

final class AddUserCandidateListener implements EventSubscriberInterface

{

    private $tokenStorage;
    private StageManager $stageManager;

    /**
     * AddUserToCrushSubscriber constructor.
     * @param $tokenStorage
     */
    public function __construct(TokenStorageInterface $tokenStorage,StageManager $stageManager)
    {
        $this->tokenStorage = $tokenStorage;
        $this->stageManager = $stageManager;
    }

    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::VIEW => ['attachUser', EventPriorities::PRE_WRITE],
        ];
    }

    public function attachUser(ViewEvent $event)
    {
        $entity = $event->getControllerResult();
        $method = $event->getRequest()->getMethod();

        if (!$entity instanceof Candidate || Request::METHOD_POST !== $method) {
            return;
        }

        $token = $this->tokenStorage->getToken();
        if (!$token) {
            return;
        }

        $user = $token->getUser();
        if (!$user instanceof User) {
            return;
        }


        $entity->setOwner($user);
    }

}