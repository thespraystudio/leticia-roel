<?php


namespace App\EventListener;
use Lexik\Bundle\JWTAuthenticationBundle\Event\AuthenticationSuccessEvent;
use Lexik\Bundle\JWTAuthenticationBundle\Events;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\SerializerInterface;

class JWTAuthentication implements EventSubscriberInterface
{

    /**
     * @var SerializerInterface
     */
    private $serializer;


    public function __construct(SerializerInterface $serializer)
    {
        $this->serializer = $serializer;
    }

    public function onAuthenticationSuccessResponse(AuthenticationSuccessEvent $event)
    {

        $data = $event->getData();
        $user = $event->getUser();


        if (!$user instanceof UserInterface) {
            return;
        }

        $data['user'] = json_decode($this->serializer->serialize( $user, 'json', ['groups' => ['read']] ));
        $event->setData($data);
    }

    /**
     * @inheritDoc
     */
    public static function getSubscribedEvents()
    {
        return [Events::AUTHENTICATION_SUCCESS => ['onAuthenticationSuccessResponse']];
    }
}