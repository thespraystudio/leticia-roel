/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you import will output into a single css file (app.css in this case)
import './styles/app.scss';

// start the Stimulus application
import './bootstrap';
import { Fancybox } from "@fancyapps/ui";
import "@fancyapps/ui/dist/fancybox/fancybox.css";

// main file js
import './javascripts/main';






//function initCollection() {
//    if ($(".collection-wrap").length) {
//        console.log("entra");
//        $(".collection-wrap").each(function () {
//            var $collectionWrap = $(this);
//            console.log($collectionWrap);
//            $.each($collectionWrap.find(".collection-item-wrap"), function (index) {
//                var $collectionItemWrap = $(this);
//
//                if (index == 0) {
//                    //solo en afianzadora se puede eliminar la primera persona de contacto
//                    if ($(this).find(".js-nohidden-remove").length <= 0 || $("form[name='bonding_company']").length <= 0) {
//                        $collectionItemWrap.find("[data-entry-action='remove']").addClass('d-none');
//                    }
//
//                    // var select = $collectionItemWrap.find('select');
//                    // select.attr('required', true);
//                    // select.attr('data-parsley-required', true);
//
//                    // if (select.hasClass('no-req')) {
//                    //     select.attr('required', false);
//                    //     select.attr('data-parsley-required', false);
//                    // }
//
//                    $collectionItemWrap.find('.slt-bonding-company-type').attr('data-parsley-required', true);
//                    $collectionItemWrap.find('.lbl-bonding-company-type').addClass('required');
//                }
//
//                if (index != 0) {
//                    $collectionItemWrap.find("[data-entry-action='remove']").removeClass('d-none');
//                }
//            });
//        });
//    }
//}

