const $ = require('jquery');
window.jQuery = $;
window.$ = $;
import '@popperjs/core';
import 'bootstrap';
import 'bootstrap-datepicker/js/bootstrap-datepicker';
import 'bootstrap-datepicker/js/locales/bootstrap-datepicker.es';
import 'parsleyjs';
import 'parsleyjs/dist/i18n/es';
import 'select2';
import '../javascripts/jscolor.min.js';
import '../js/rSlider.js'
import a2lix_lib from './collection';

require('slick-carousel');
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
//import a2lix_lib from '@a2lix/symfony-collection/dist/a2lix_sf_collection.min'
$.fn.select2.amd.define('select2/i18n/es', [], require("select2/src/js/select2/i18n/es"));
import './boostrap.bundle.min.js';
import { Collapse, Modal } from "bootstrap";

import { Fancybox } from '@fancyapps/ui';

let editors = {};
const editorConfig = {
    toolbar: ['link'],
    link: {
        addTargetToExternalLinks: true
    },
};
$(document).ready(function () {
    inita2lix();
    // Slider principal (galery-inner)
    $('.galery-inner').slick({
        infinite: true, // Asegura que el slider principal también sea infinito
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: false,
        arrows: true,
        prevArrow: $('.slick-prev-main'),
        nextArrow: $('.slick-next-main'),
        asNavFor: '.slider-nav'
    });
    // Slider de navegación (slider-nav)
    $('.slider-nav').slick({
        slidesToShow: 5, // Número de miniaturas visibles
        slidesToScroll: 1,
        dots: false,
        arrows: true,
        infinite: true, // Slider de navegación infinito
        asNavFor: '.galery-inner', // Sincroniza con el slider principal
        focusOnSelect: true // Permite seleccionar la imagen haciendo clic
    });
    $('.js-limpiar').on('click', function (event) {
        event.preventDefault();
        $('input[type="radio"], input[type="checkbox"]').prop('checked', false);
    });
    $('.collapsible').on('click', function () {
        var $content = $(this).next('.content');
        if ($content.is(':visible')) {
            $(this).removeClass('active');
            $content.slideUp();
        } else {
            $('.collapsible').not(this).removeClass('active').next('.content').slideUp();
            $(this).addClass('active');
            $content.slideDown();
        }
    });
    if ($(".cont-files")[0]) {
        var imgThis = $(this).find('a').find('img').parent().html();
        $(this).find('.fot').html(imgThis);
    }
    $('.js-example-basic-single').select2({});
    $(document).ready(function () {
        hideFlashMessage('flash-message-success');
        hideFlashMessage('flash-message-error');
    });
    $(document).ready(function () {
        var minCost = document.getElementById("school_filter_costMin");
        var maxCost = document.getElementById("school_filter_costMax");
        $('#school_filter_costMin').change(function () {
            var min = minCost.value;
            var max = maxCost.value;
            if (min > max) {
                alert("El costo mínimo debe ser igual o mayor al costo mayor. ");
                minCost.value = maxCost.value;
            }
        });
        $('#school_filter_costMax').change(function () {
            var min = minCost.value;
            var max = maxCost.value;
            if (min <= max) {
                alert("El costo máximo debe ser igual o menor al costo mínimo ");
                minCost.value = maxCost.value;
            }
        });
    });
    $('#schoolFrom1').on('submit', function (e) {
        e.preventDefault();
        var formData = $(this).serialize();
        var messageDiv = $('#message');
        $.ajax({
            url: $(this).attr('action'),
            type: $(this).attr('method'),
            data: formData,
            success: function (response) {
                if (response.success) {
                    messageDiv.html('<p style="color: green;">Los datos se guardaron correctamente</p>');
                } else {
                    messageDiv.html('<p style="color: red;">Hubo un error al guardar los datos</p>');
                }
                setTimeout(function () {
                    messageDiv.empty();
                }, 3000);
            },
            error: function () {
                alert('Se produjo un error en el servidor');
            }
        });
    });
    $('#toggle-password').click(function (e) {
        e.preventDefault();
        var passwordField = $('#inputPassword');
        var fieldType = passwordField.attr('type');
        console.log(passwordField);
        console.log(fieldType);
        if (fieldType === 'password') {
            passwordField.attr('type', 'text');
            $('#toggle-password i').removeClass('fa-eye').addClass('fa-eye-slash');
        } else {
            passwordField.attr('type', 'password');
            $('#toggle-password i').removeClass('fa-eye-slash').addClass('fa-eye');
        }
    });
    //subprocesos (nery)
    var substages = $('.collection-wrap-subprocess');
    $(document).on('click', '.js-genus-subprocess-add', function (e) {
        e.preventDefault();
        var prototype = substages.data('prototype');
        var index = substages.data('index');
        var newForm = prototype.replace(/__name__/g, index);
        substages.data('index', index + 1);
        $(this).after(newForm);
    });
    $(document).on('click', '.js-remove-children', function (e) {
        e.preventDefault();
        var id = $(this).attr("data-id");
        $(this).closest('.list-group-item')
            .fadeOut()
            .remove();
        $('.container-child-' + id)
            .fadeOut()
            .remove();
        return false;
    });
    //fields-campos
    $(document).on('click', '.js-genus-field-add', function (e) {
        e.preventDefault();
        var id = $(this).data("id");
        var fields = $('.collection-wrap-fields-' + id);
        console.log(fields);
        if (id == 0) {
            return;
        }
        var prototype = fields.data('prototype');
        var index = fields.data('index');
        var newForm = prototype.replace(/__name__/g, index);
        fields.data('index', index + 1);
        $(".container-child-" + id + " .collection-wrap-fields-" + id).before(newForm);
    });
    $(document).on('click', '.js-remove-fields', function (e) {
        e.preventDefault();
        $(this).closest('.field-group-item')
            .fadeOut()
            .remove();
        return false;
    });
    let isOperador = $('.roles-menu').data('is-operador');
    if ($('.menu-active').attr('data-path')) {
        var valor = $('.menu-active').data('path');
        $('.menu .active').removeClass('active');
        if (!isOperador || valor == "app_candidate_edit" || valor == "app_candidate_index" || valor == "app_candidate_new" || valor == "app_candidate_process" || valor == "app_candidate_school_process" || valor == "app_candidate_stage") {
            $(".app_candidate_index").addClass('active');
        } else if (valor == "app_user_edit" || valor == "app_user_index" || valor == "app_user_new") {
            $(".app_user_index").addClass('active');
        } else if (valor == "app_school_index" || valor == "app_school_edit" || valor == "app_school_show" || valor == "app_school_index" || valor == "app_school_new") {
            $(".app_school_index").addClass('active');
        }
    }
    $(document).on('click', '.js-genus-document-add', function (e) {
        e.preventDefault();
        var id = $(this).data("id");
        var doc = $('.collection-wrap-documents-' + id);
        if (id == 0) {
            return;
        }
        var prototype = doc.data('prototype');
        var index = doc.data('index');
        var newForm = prototype.replace(/__name__/g, index);
        doc.data('index', index + 1);
        $(".container-child-" + id + " .collection-wrap-fields-" + id).before(newForm);
    });
    $(document).on('click', '.js-remove-document', function (e) {
        e.preventDefault();
        $(this).closest('.document-group-item')
            .fadeOut()
            .remove();
        return false;
    });
    $('.accordion-wrap > a').click(function (event) {
        event.preventDefault();
        $(this).closest('.accordion-wrap').toggleClass('active');
    });
    initializeCKEditor();
    // $('#schoolFrom').on('submit', function (e) {
    //     e.preventDefault();
    //     console.log('submit');
    //     for (let id in editors) {
    //         editors[id].updateSourceElement();
    //     }
    // });
    $('.form-cked').on('click', function (e) {
        console.log('entro???');
        for (let id in editors) {
            editors[id].updateSourceElement();
        }
        $('#schoolForm').submit();
    });

    $('.card-item').on('click', function () {
        $(this).find('a.go-stage')[0].click();
    });

    if ($('.js-inical').length) {

        $('.js-inical').find('.card-item:not(.validate)').first().trigger('click');
    }

    $(".delete-btn").on("click", function () {
        $(this).parent().remove();
    });

    $('.btnRegresar-js').click(function () {

        window.history.back();
    });
    Fancybox.bind('[data-fancybox="gallery-pictures"]', {
        infinite: true,
        buttons: ["zoom", "slideShow", "thumbs", "close"],
        transitionEffect: "fade",
        caption: function (fancybox, slide) {
            return slide.$trigger ? slide.$trigger.dataset.caption || '' : '';
        }
    });
    
    Fancybox.bind('[data-fancybox="gallery-videos"]', {
        infinite: true,
        buttons: ["zoom", "slideShow", "thumbs", "close"],
        transitionEffect: "fade",
        caption: function (fancybox, slide) {
            return slide.$trigger ? slide.$trigger.dataset.caption || '' : '';
        }
    });

});
// $(document).on('DOMNodeInserted', function (e) {
//     $('.btn-ckeditor').on('click', function () {
//         console.log('______11111');
//         setTimeout(function () {
//             initializeCKEditor();
//         }, 100);
//     });
// });
// $(document).on('click', '.btn-ckeditor', function () {
//     console.log('Botón CKEditor presionado');
//     setTimeout(function () {
//         initializeCKEditor();
//     }, 100);
// });
$(document).on('click', '.js-select-file', function (e) {
    console.log('123');
    $(this).parent().find('.file-tr').trigger("click");
    return false;
});
$(document).on('click', '.js-edit-children', function () {
    var id = $(this).attr("data-id");
    $(".container-hidden").each(function () {
        if ($(this).hasClass('d-none')) {
        } else {
            $(this).addClass('d-none');
        }
    });
    $('.container-child-' + id).removeClass('d-none');
    $('.js-genus-field-add').data('id', id);
    $('.js-genus-document-add').data('id', id);
});
$(document).on('click', '.js-remove-new', function () {
    $(this).closest('.collection-item-wrap').remove();
});
$(document).on('click', '.js-remove-element', function () {
    $(this).closest('.collection-item-wrap').remove();
});
$(document).on('click', '.js-switch-active', function () {
    $('.cont-proces').find('.active').removeClass('active');
    $(this).closest('.card-item').addClass("active");
});
$(document).on('click', '.btn-delete-tt', function (event) {
    event.preventDefault()
    console.log('entro');
    var removeButton = $(this).closest('.accordion.accordion-flush');
    if (removeButton.length) {
        removeButton.remove();
    }
});
$(document).on('click', '.js-show-process', function () {
    var id = $(this).data('id');
    $(".each-hiden").each(function () {
        if ($(this).hasClass('d-none')) {
        } else {
            $(this).addClass('d-none');
        }
    });
    $('.content-process-' + id).removeClass('d-none');
    $(".card-show-process .btn-edit").attr('href', '/process/' + id + '/edit');
});
$(document).on('click', '.btn-link', function (e) {
    e.preventDefault();
    var card = $(this).closest('.card');
    var collapse = card.find('.collapse').first();
    collapse.collapse('toggle');
});
$(document).on('click', '.accion a', function (event) {
    event.preventDefault();
    var consultaAjaxUrl = $(this).attr('href');
    var $formStage = $('#form-stage');
    var $scrollContainer = $('.h-proces');
    $formStage.fadeTo("fast", 0.5);
    $.ajax({
        url: consultaAjaxUrl,
        type: 'GET',
        success: function (response) {
            $scrollContainer.animate({
                scrollTop: 0
            }, 800, function () {
                $formStage.fadeTo("fast", 1);
                $formStage.html(response.html);
            });
            
        },
        error: function (xhr, status, error) {
            console.log('tunas-error');
            console.error(xhr.responseText);
        }
    });
});
$(document).on('click', '.js-update-answer', function (event) {
    event.preventDefault();
    var storyContent = $('#story').val();
    $('#form-stage').fadeTo("fast", 0.5);
    $.ajax({
        url: $(this).attr('href'),
        type: 'POST',
        data: { answer: storyContent },
        success: function (response) {
            $('#form-stage').fadeTo("fast", 1);
            $('a.js-update-answer').hide();
            var successMessage = document.createElement('div');
            successMessage.textContent = '¡Actualización completa!';
            successMessage.style.color = 'green';
            $('a.js-update-answer').closest('.col-md-24').append(successMessage);
        },
        error: function (xhr, status, error) {
            $('#form-stage').fadeTo("fast", 1);
        }
    });
});
$('.no-close').on('click', function (event) {
    event.stopPropagation();
});
$('#candidate_isNewCandidate').on('change', function () {
    var isNewCandidateChecked = $(this).is(':checked');
    var $formNewCandidate = $('.js-form-new-candidate');
    var $candidateSelect = $('.js-candidate-select');
    var $formFields = $formNewCandidate.find('input, select, textarea');
    $formFields.prop('required', isNewCandidateChecked);
    $formNewCandidate.toggleClass('d-none', !isNewCandidateChecked);
    $candidateSelect.toggleClass('d-none', isNewCandidateChecked);
    $formFields.prop('disabled', !isNewCandidateChecked);
    var $fileField = $('.js-form-new-candidate').find('input[type="file"]');
    $fileField.prop('required', false);
});
$(document).on('change', '.js-update-answer-approved', function (event) {
    event.preventDefault();
    var $url = $(this).data("url");
    var $respuesta = $(this).is(":checked") ? 1 : 0;
    var storyContent = $('#story').val();
    console.log($url);
    $.ajax({
        url: $url,
        type: 'POST',
        data: { approved: $respuesta },
        success: function (response) {
            console.log('Respuesta del servidor:', response);
        },
        error: function (xhr, status, error) {
            console.error('Error en la solicitud AJAX:', error);
        }
    });
});
document.addEventListener('submit', function (event) {
    var form = event.target;

    var parent = $(form).closest('.response-management');
    parent.fadeTo("fast", 0.5);
    console.log(parent);
    if (event.target.matches('.form-fiele form')) {
        event.preventDefault();
        var fileInput = form.querySelector('input[type="file"]');
        var file = fileInput.files[0];
        var formData = new FormData();
        formData.append('file', file);
        fetch(form.action, {
            method: form.method,
            body: formData
        }).then(response => {
            parent.fadeTo("fast", 1);
            if (response.ok) {
                form.querySelector('button[type="submit"]').style.display = 'none';
                var successMessage = document.createElement('div');
                successMessage.textContent = '¡Actualización completa!';
                successMessage.style.color = 'green';
                form.parentNode.appendChild(successMessage);
            } else {
            }
        })
            .catch(error => {
            });
        return false;
    }
});
document.addEventListener('DOMContentLoaded', function () {
    document.addEventListener('change', function (event) {
        if (event.target.matches('#switchApproved')) {
            var id = event.target.dataset.id
            var route = event.target.dataset.route;
            var approved = event.target.checked ? 1 : 0;
            var type = event.target.dataset.type;
            var xhr = new XMLHttpRequest();
            xhr.open('POST', route);
            xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
            xhr.onload = function () {
                if (xhr.status === 200) {
                    console.log(xhr.responseText);
                } else {
                    console.error('Error al actualizar el estado de approved');
                }
            };
            xhr.send('id=' + id + '&approved=' + approved + '&type=' + type);
        }
    });
});

function inita2lix() {
    a2lix_lib.sfCollection.init({
        collectionsSelector: 'form div[data-prototype]',
        manageRemoveEntry: true,
        entry: {
            add: {
                prototype:
                    '<button class="__class__" data-entry-action="add">__label__</button>',
                class: 'btn btn-primary btn-sm m-y-2 add-process-btn',
                label: "Agregar otro elemento",
                customFn: null,
                onBeforeFn: null,
                onAfterFn: function (newElement) {
                    jscolor.install();
                    initializeCKEditor(newElement);
                }
            },
            remove: {
                prototype:
                    '<button class="__class__" data-entry-action="remove">__label__</button>',
                class: 'btn-delete',
                label: 'Eliminar elemento',
                customFn: null,
                onAfterFn: function (removedElement) {
                    console.log('Elemento eliminado');
                }
            }
        }
    });
}

function hideFlashMessage(id) {
    setTimeout(function () {
        $('#' + id).fadeOut();
    }, 5000); // 5000 milisegundos = 5 segundos
}

$(document).on('click', '#btn-tab-galeria', function () {
    $(window).trigger('resize');
});

function initializeCKEditor() {
    $('.ckeditor-description').each(function () {
        if (!$(this).hasClass('ckeditor-initialized')) {
            let elementId = $(this).attr('id');
            ClassicEditor
                .create(this, editorConfig)
                .then(editor => {
                    $(this).addClass('ckeditor-initialized');
                    editors[elementId] = editor;
                    editors[$(this).attr('id')] = editor;
                })
                .catch(error => {
                    console.error(error);
                });
        }
    });
}
