'use strict';
let __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (let s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (let p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
Object.defineProperty(exports, "__esModule", { value: true });
let a2lix_lib = {};
a2lix_lib.sfCollection = (function () {
    let ENTRY_ACTION;
    (function (ENTRY_ACTION) {
        ENTRY_ACTION["ADD"] = "add";
        ENTRY_ACTION["REMOVE"] = "remove";
    })(ENTRY_ACTION || (ENTRY_ACTION = {}));
    let CONFIG_DEFAULT = {
        collectionsSelector: 'div[data-prototype]',
        entry: {
            add: {
                enabled: true,
                prototype: "<button class=\"__class__\" data-entry-action=\"".concat(ENTRY_ACTION.ADD, "\">__label__</button>"),
                class: 'btn btn-primary btn-sm mt-2',
                label: 'Add',
                customFn: null,
                onBeforeFn: null,
                onAfterFn: null,
            },
            remove: {
                enabled: true,
                prototype: "<button class=\"__class__\" data-entry-action=\"".concat(ENTRY_ACTION.REMOVE, "\">__label__</button>"),
                class: 'btn btn-danger btn-sm mt-2',
                label: 'Remove',
                customFn: null,
                onAfterFn: null,
            },
        },
    };
    let init = function (config) {
        let _a, _b;
        if (config === void 0) { config = CONFIG_DEFAULT; }
        if (!('content' in document.createElement('template'))) {
            console.error('HTML template will not working...');
            return;
        }
        let cfg = __assign(__assign(__assign({}, CONFIG_DEFAULT), config), { entry: {
                add: __assign(__assign({}, CONFIG_DEFAULT.entry.add), (((_a = config.entry) === null || _a === void 0 ? void 0 : _a.add) || {})),
                remove: __assign(__assign({}, CONFIG_DEFAULT.entry.remove), (((_b = config.entry) === null || _b === void 0 ? void 0 : _b.remove) || {})),
            } });
        proceedCollectionElts(document.querySelectorAll(cfg.collectionsSelector), cfg);
    };
    let proceedCollectionElts = function (collectionElts, cfg) {
        if (!collectionElts.length) {
            return;
        }
        collectionElts.forEach(function (collectionElt) {
            proceedCollectionElt(collectionElt, cfg);
        });
    };
    let proceedCollectionElt = function (collectionElt, cfg) {
        let _a, _b;
        collectionElt.setAttribute('data-entry-index', collectionElt.children.length + '');
        if ((_a = collectionElt.getAttribute('data-entry-add-enabled')) !== null && _a !== void 0 ? _a : cfg.entry.add.enabled) {
            appendEntryAddElt(collectionElt, cfg.entry.add);
        }
        if ((_b = collectionElt.getAttribute('data-entry-remove-enabled')) !== null && _b !== void 0 ? _b : cfg.entry.remove.enabled) {
            appendEntryRemoveElts(collectionElt, cfg.entry.remove);
        }
        collectionElt.addEventListener('click', function (evt) {
            return handleEntryActionClick(evt, cfg);
        });
    };
    let appendEntryAddElt = function (collectionElt, entryAddCfg) {
        let _a, _b, _c;
        let entryAddHtml = ((_a = collectionElt.getAttribute('data-entry-add-prototype')) !== null && _a !== void 0 ? _a : entryAddCfg.prototype)
            .replace(/__class__/g, (_b = collectionElt.getAttribute('data-entry-add-class')) !== null && _b !== void 0 ? _b : entryAddCfg.class)
            .replace(/__label__/g, (_c = collectionElt.getAttribute('data-entry-add-label')) !== null && _c !== void 0 ? _c : entryAddCfg.label);
        collectionElt.appendChild(createTemplateContent(entryAddHtml));
    };
    let appendEntryRemoveElts = function (collectionElt, entryRemoveCfg) {
        let templateContentEntryRemove = getTemplateContentEntryRemove(collectionElt, entryRemoveCfg);
        Array.from(collectionElt.children)
            .filter(function (entryElt) { return !entryElt.hasAttribute('data-entry-action'); })
            .forEach(function (entryElt) {
                entryElt.appendChild(templateContentEntryRemove.cloneNode(true));
            });
    };
    let getTemplateContentEntryRemove = function (collectionElt, entryRemoveCfg) {
        let _a, _b, _c;
        let entryRemoveHtml = ((_a = collectionElt.getAttribute('data-entry-remove-prototype')) !== null && _a !== void 0 ? _a : entryRemoveCfg.prototype)
            .replace(/__class__/g, (_b = collectionElt.getAttribute('data-entry-remove-class')) !== null && _b !== void 0 ? _b : entryRemoveCfg.class)
            .replace(/__label__/g, (_c = collectionElt.getAttribute('data-entry-remove-label')) !== null && _c !== void 0 ? _c : entryRemoveCfg.label);
        return createTemplateContent(entryRemoveHtml);
    };
    let handleEntryActionClick = function (evt, cfg) {
        if (!evt.target.hasAttribute('data-entry-action')) {
            return;
        }
        evt.preventDefault();
        evt.stopPropagation();
        let collectionElt = evt.currentTarget.closest(cfg.collectionsSelector);
        switch (evt.target.getAttribute('data-entry-action')) {
            case ENTRY_ACTION.ADD:
                handleEntryActionAddClick(collectionElt, evt.target, cfg);
                break;
            case ENTRY_ACTION.REMOVE:
                handleEntryActionRemoveClick(collectionElt, evt.target, cfg);
                break;
        }
    };
    let handleEntryActionAddClick = function (collectionElt, entryAddElt, cfg) {
        let templateContentEntry = getTemplateContentEntry(collectionElt, cfg);
        if (cfg.entry.add.customFn) {
            cfg.entry.add.customFn(collectionElt, entryAddElt, templateContentEntry, cfg);
            return;
        }
        addEntry(collectionElt, entryAddElt, templateContentEntry, cfg);
    };
    let handleEntryActionRemoveClick = function (collectionElt, entryRemoveElt, cfg) {
        if (cfg.entry.remove.customFn) {
            cfg.entry.remove.customFn(collectionElt, entryRemoveElt, cfg);
            return;
        }
        removeEntry(collectionElt, entryRemoveElt, cfg);
    };
    let getTemplateContentEntry = function (collectionElt, cfg) {
        let _a, _b, _c;
        let entryIndex = (_a = collectionElt.getAttribute('data-entry-index')) !== null && _a !== void 0 ? _a : 0;
        collectionElt.setAttribute('data-entry-index', +entryIndex + 1 + '');
        let prototypeName = (_b = collectionElt.getAttribute('data-prototype-name')) !== null && _b !== void 0 ? _b : '__name__';
        let entryHtml = collectionElt
            .getAttribute('data-prototype')
            .replace(new RegExp("".concat(prototypeName, "label__"), 'g'), "!New! ".concat(entryIndex))
            .replace(new RegExp(prototypeName, 'g'), entryIndex + '');
        let templateContentEntry = createTemplateContent(entryHtml);
        if ((_c = collectionElt.getAttribute('data-entry-remove-enabled')) !== null && _c !== void 0 ? _c : cfg.entry.remove.enabled) {
            templateContentEntry.firstChild.appendChild(getTemplateContentEntryRemove(collectionElt, cfg.entry.remove));
        }
        return templateContentEntry;
    };
    let addEntry = function (collectionElt, entryAddElt, templateContentEntry, cfg) {
        let _a, _b, _c, _d;
        (_b = (_a = cfg.entry.add).onBeforeFn) === null || _b === void 0 ? void 0 : _b.call(_a, collectionElt, entryAddElt, templateContentEntry);
        entryAddElt.parentElement.insertBefore(templateContentEntry, entryAddElt);
        proceedCollectionElts(entryAddElt.previousElementSibling.querySelectorAll(cfg.collectionsSelector), cfg);
        (_d = (_c = cfg.entry.add).onAfterFn) === null || _d === void 0 ? void 0 : _d.call(_c, collectionElt, entryAddElt);
    };
    let removeEntry = function (collectionElt, entryRemoveElt, cfg) {
        let _a, _b;
        entryRemoveElt.parentElement.remove();
        (_b = (_a = cfg.entry.remove).onAfterFn) === null || _b === void 0 ? void 0 : _b.call(_a, collectionElt, entryRemoveElt);
    };
    /**
     * HELPERS
     */
    let createTemplateContent = function (html) {
        let template = document.createElement('template');
        template.innerHTML = html.trim();
        return template.content;
    };
    return {
        init: init,
    };
})();
exports.default = a2lix_lib;
