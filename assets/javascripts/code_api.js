
const API_URL = 'https://api.copomex.com/query';

const xhr = new XMLHttpRequest();
const xhr2 = new XMLHttpRequest();
const xhr3 = new XMLHttpRequest();

// Get the input field
var input = document.getElementsByClassName("client_zip_code");

$(document).on('blur','.client_zip_code',function(){
  findZipCode();
});

$(document).on('blur','.beneficiary_zip_code',function(){
  if($(this).closest(".js-collection-item-wrap").length > 0){
    //beneficiarios en polizas
    var div = $(this).closest(".js-collection-item-wrap");
    findZipCodeBeneficiary(div);  
  }else{
   //beneficiarios en catalogos
    findZipCodeBeneficiaryBack();
  }
  
});
  
  //clientes en polizas
function onRequestHandler(text) {

  let entity = document.getElementsByClassName("client_federal_entity");
  var municipality = document.getElementsByClassName("client_municipality");
  var city = document.getElementsByClassName("client_city");
  var suburb = document.getElementsByClassName("client_suburb");
  var zip = document.getElementsByClassName("client_zip_code");
  
    removeOptions(document.getElementsByClassName('client_federal_entity'));
    removeOptions(document.getElementsByClassName('client_municipality'));
    //removeOptions(document.getElementById('client_city'));
    removeOptions(document.getElementsByClassName('client_suburb'));

    if (this.status === 200) {
        const data = JSON.parse(this.response);
        
        var estado    = '';
        var ciudad    ='';
        var municipio = '';
        
        for (item of data) {
            estado    = item.response.estado;
            municipio = item.response.municipio;
            ciudad    = item.response.ciudad;
        }
        removeOptions(document.getElementsByClassName('client_federal_entity'));
        
            var option      = document.createElement("option");
            option.text     = estado;
            option.selected = true;
            var sel         = entity[0].options[entity[0].selectedIndex];
            entity[0].add(option, sel);
            $(".client_federal_entity-input").val(estado);
        
        removeOptions(document.getElementsByClassName('client_municipality'));
        
            var option      = document.createElement("option");
            option.text     = municipio;
            option.selected = true;
            var sel         = municipality[0].options[municipality[0].selectedIndex];
            municipality[0].add(option, sel);
            $(".client_municipality-input").val(municipio);
        
           //subur municipio  ciudad
        removeOptions(document.getElementsByClassName('client_suburb'));
        
        var option2 = document.createElement("option");
        option2.text = 'Seleccione una opción';
        option2.selected = true;
        option2.disabled = true;
        var sel = suburb[0].options[suburb[0].selectedIndex];
        suburb[0].add(option2, sel);
        for (item2 of data) {    
            var option = document.createElement("option");            
                option.text = item2.response.asentamiento;
                //option.selected = true;
                var sel = suburb[0].options[suburb[0].selectedIndex];
                suburb[0].add(option, sel);           
                $(".client_suburb-input").val(option.text);
        }   
        
      //  disabledSelect(false);     
        
    }
    else{
    
      $(".client_federal_entity-input").val('');
      $(".client_municipality-input").val('');
      $(".client_suburb-input").val('');
    
    }
}

//beneficiarios en polizas
function onRequestHandlerBeneficiary(div) {
  
    var slt_federal_entity  = div.find('.beneficiary_federal_entity');
    var slt_municipality    = div.find('.beneficiary_municipality');
    var slt_suburb          = div.find('.beneficiary_suburb');

    var entity_beneficiary        = div.find('.beneficiary_federal_entity');
    var municipality_beneficiary  = div.find('.beneficiary_municipality');
    var suburb_beneficiary        = div.find('.beneficiary_suburb');
    
    removeOptions(div.find('.beneficiary_federal_entity'));
    removeOptions(div.find('.beneficiary_municipality'));
    removeOptions(div.find('.beneficiary_suburb'));
    
    setTimeout(() => {
    
    if (xhr2.status === 200) {

        const data = JSON.parse(xhr2.response);
        var estado    = '';
        var ciudad    = '';
        var municipio = '';
        
        for (var item of data) {
            estado    = item.response.estado;
            municipio = item.response.municipio;
            ciudad    = item.response.ciudad;
        }
        removeOptions(slt_federal_entity);

            var option      = document.createElement("option");
            option.text     = estado;
            option.selected = true;
            var sel         = entity_beneficiary[0].options[entity_beneficiary[0].selectedIndex];
            entity_beneficiary[0].add(option, sel);
            slt_federal_entity.val(estado);
            div.find(".beneficiary_federal_entity-input").val(estado);
        
        removeOptions(slt_municipality);
        
            var option      = document.createElement("option");
            option.text     = municipio;
            option.selected = true;
            var sel         = municipality_beneficiary[0].options[municipality_beneficiary[0].selectedIndex];
            municipality_beneficiary[0].add(option, sel);
            slt_municipality.val(municipio);
            div.find(".beneficiary_municipality-input").val(municipio);
        
           //subur municipio  ciudad
        removeOptions(slt_suburb);
        
        for (item2 of data) {    
          var option = document.createElement("option");            
              option.text = item2.response.asentamiento;
              //option.selected = true;
              var sel = suburb_beneficiary[0].options[suburb_beneficiary[0].selectedIndex];
              suburb_beneficiary[0].add(option, sel);           
              slt_suburb.val(option.text);
        }

        var option2 = document.createElement("option");
        option2.text = 'Seleccione una opción';
        option2.selected = true;
        option2.disabled = true;
        var sel = suburb_beneficiary[0].options[suburb_beneficiary[0].selectedIndex];
        suburb_beneficiary[0].add(option2, sel);       
    }
    else{
      div.find(".beneficiary_federal_entity-input").val('');
      div.find(".beneficiary_municipality-input").val('');
      div.find(".beneficiary_suburb-input").val('');
    }
  }, 800);
}

//beneficiarios en catalogos
function onRequestHandlerBack(){

    var input_beneficiary         = document.getElementsByClassName("beneficiary_zip_code");
    let entity_beneficiary        = document.getElementsByClassName("beneficiary_federal_entity");
    var municipality_beneficiary  = document.getElementsByClassName("beneficiary_municipality");
    var city_beneficiary          = document.getElementsByClassName("beneficiary_city");
    var suburb_beneficiary        = document.getElementsByClassName("beneficiary_suburb");
    var zip_beneficiary           = document.getElementsByClassName("beneficiary_zip_code");

    removeOptions(document.getElementsByClassName('beneficiary_federal_entity'));
    removeOptions(document.getElementsByClassName('beneficiary_municipality'));
    removeOptions(document.getElementsByClassName('beneficiary_suburb'));

    if (this.status === 200) {
        const data = JSON.parse(this.response);
        
        var estado    = '';
        var ciudad    = '';
        var municipio = '';
        
        for (item of data) {
            estado    = item.response.estado;
            municipio = item.response.municipio;
            ciudad    = item.response.ciudad;
        }
        removeOptions(document.getElementsByClassName('beneficiary_federal_entity'));
        
            var option      = document.createElement("option");
            option.text     = estado;
            option.selected = true;
            var sel         = entity_beneficiary[0].options[entity_beneficiary[0].selectedIndex];
            entity_beneficiary[0].add(option, sel);
            $(".beneficiary_federal_entity-input").val(estado);
        
        removeOptions(document.getElementsByClassName('beneficiary_municipality'));
        
            var option      = document.createElement("option");
            option.text     = municipio;
            option.selected = true;
            var sel         = municipality_beneficiary[0].options[municipality_beneficiary[0].selectedIndex];
            municipality_beneficiary[0].add(option, sel);
            $(".beneficiary_municipality-input").val(ciudad);
        
           //subur municipio  ciudad
        removeOptions(document.getElementsByClassName('beneficiary_suburb'));
        var option2       = document.createElement("option");
        option2.text      = 'Seleccione una opción';
        option2.selected  = true;
        option2.disabled  = true;
        var sel           = suburb_beneficiary[0].options[suburb_beneficiary[0].selectedIndex];
        suburb_beneficiary[0].add(option2, sel);  
        for (item2 of data) {    
            var option = document.createElement("option");            
                option.text = item2.response.asentamiento;
                var sel = suburb_beneficiary[0].options[suburb_beneficiary[0].selectedIndex];
                suburb_beneficiary[0].add(option, sel);           
                $(".beneficiary_suburb-input").val(option.text);
        }   
        
    }
    else{
      $(".beneficiary_federal_entity-input").val('');
      $(".beneficiary_municipality-input").val('');
      $(".beneficiary_suburb-input").val('');
    }
}

//clientes en catalogos y poliza
function findZipCode() {
    xhr.addEventListener('load', onRequestHandler);
    var zipRequest = $(".client_zip_code").val();
    xhr.open('GET', `${API_URL}/info_cp/${zipRequest}?token=7d9966fd-18eb-4e72-8566-4d0bb63d58a7`);
    xhr.send();
}

//beneficiarios en poliza
function findZipCodeBeneficiary(div) {
    xhr2.addEventListener('load', onRequestHandlerBeneficiary(div));
    var zipRequest = div.find(".beneficiary_zip_code").val();
    xhr2.open('GET', `${API_URL}/info_cp/${zipRequest}?token=7d9966fd-18eb-4e72-8566-4d0bb63d58a7`);
    xhr2.send();
}

//beneficiarios en catalogos
function findZipCodeBeneficiaryBack() {
    xhr3.addEventListener('load', onRequestHandlerBack);
    var zipRequest = $(".beneficiary_zip_code").val();
    xhr3.open('GET', `${API_URL}/info_cp/${zipRequest}?token=7d9966fd-18eb-4e72-8566-4d0bb63d58a7`);
    xhr3.send();
}

function removeOptions(selectElement) {
    var select = selectElement[0];
    var i, L = select.options.length - 1;
    for(i = L; i >= 0; i--) {
       select.remove(i);
    }
}
