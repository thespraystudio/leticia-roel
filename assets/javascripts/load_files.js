// const $ = require("jquery");
const labels_es_ES = {
    labelIdle: 'Arrastra y suelta tus archivos o <span class = "filepond--label-action"> Examinar <span>',
    labelInvalidField: "El campo contiene archivos inválidos",
    labelFileWaitingForSize: "Esperando tamaño",
    labelFileSizeNotAvailable: "Tamaño no disponible",
    labelFileLoading: "Cargando",
    labelFileLoadError: "Error durante la carga",
    labelFileProcessing: "Cargando",
    labelFileProcessingComplete: "Carga completa",
    labelFileProcessingAborted: "Carga cancelada",
    labelFileProcessingError: "Error durante la carga",
    labelFileProcessingRevertError: "Error durante la reversión",
    labelFileRemoveError: "Error durante la eliminación",
    labelTapToCancel: "toca para cancelar",
    labelTapToRetry: "tocar para volver a intentar",
    labelTapToUndo: "tocar para deshacer",
    labelButtonRemoveItem: "Eliminar",
    labelButtonAbortItemLoad: "Abortar",
    labelButtonRetryItemLoad: "Reintentar",
    labelButtonAbortItemProcessing: "Cancelar",
    labelButtonUndoItemProcessing: "Deshacer",
    labelButtonRetryItemProcessing: "Reintentar",
    labelButtonProcessItem: "Cargar",
    labelMaxFileSizeExceeded: "El archivo es demasiado grande",
    labelMaxFileSize: "El tamaño máximo del archivo es {filesize}",
    labelMaxTotalFileSizeExceeded: "Tamaño total máximo excedido",
    labelMaxTotalFileSize: "El tamaño total máximo del archivo es {filesize}",
    labelFileTypeNotAllowed: "Archivo de tipo no válido",
    fileValidateTypeLabelExpectedTypes: "Espera {allButLastType} o {lastType}",
    imageValidateSizeLabelFormatError: "Tipo de imagen no compatible",
    imageValidateSizeLabelImageSizeTooSmall: "La imagen es demasiado pequeña",
    imageValidateSizeLabelImageSizeTooBig: "La imagen es demasiado grande",
    imageValidateSizeLabelExpectedMinSize: "El tamaño mínimo es {minWidth} × {minHeight}",
    imageValidateSizeLabelExpectedMaxSize: "El tamaño máximo es {maxWidth} × {maxHeight}",
    imageValidateSizeLabelImageResolutionTooLow: "La resolución es demasiado baja",
    imageValidateSizeLabelImageResolutionTooHigh: "La resolución es demasiado alta",
    imageValidateSizeLabelExpectedMinResolution: "La resolución mínima es {minResolution}",
    imageValidateSizeLabelExpectedMaxResolution: "La resolución máxima es {maxResolution}",
};

// // create a FilePond instance at the input element location
// FilePond.registerPlugin(
//     FilePondPluginFileValidateType,
//     FilePondPluginFileValidateSize
//     );
// FilePond.setOptions(labels_es_ES);

// $(document).ready(function () {
//     adminFilepond();
// });

// //carga de documentos para la parte administrativa
function adminFilepond(){
console.log("aplica");
var fields = document.querySelectorAll(".img-field");
fields.forEach(function (e) {

    let field = $(e);
    let type_image = field.attr('data-type-image');     //tipo de imagen a cargar
    var files = '';

    if (type_image == 'candidate') {

        var register_id = field.attr('data-candidate-id');
        var url_process = '/candidate/upload-file-candidate/'
        var url_revert = '/candidate/delete-doc-candidate/'
        var instant_Upload = true;

    } else if (type_image == 'afianzadora') {

        var register_id = field.attr('data-afianzadora-id');
        var url_process = '/afianzadoras/upload-file-afianzadora/';
        var url_revert = '/afianzadoras/delete-doc-afianzadora/';
        var instant_Upload = true;

    } else if (type_image == 'cliente') {

        var register_id = field.attr('data-client-id');
        var url_process = '/clientes/upload-file-client/';
        var url_revert = '/clientes/delete-doc-cliente/';
        var instant_Upload = true;

    } else if (type_image == 'beneficiary') {

        var register_id = field.attr('data-beneficiary-id');
        var url_process = '/beneficiarios/upload-file-beneficiary/';
        var url_revert = '/beneficiarios/delete-doc-beneficiary/';
        var instant_Upload = true;

    } else if (type_image == 'user') {

        var register_id = field.attr('data-user-id');
        var url_process = '/user/upload-file-profile/';
        var url_revert = '/user/delete-doc-profile/';
        var instant_Upload = true;

    }else if (type_image == 'fidelidad') {

        var register_id = field.attr('data-employee-id');
        var url_process = '/fidelidad/upload-file-femployee/';
        var url_revert = '/fidelidad/delete-doc-femployee/';
        var instant_Upload = true;
        files = 'application/pdf';

    } else if (type_image == 'batch-empleados') {
        var instant_Upload = true;
    }
    var select_id = field.attr('id');     //id del select
    var url_base = $("#data-url").val();
    var inputElement = document.querySelector("#" + select_id);
    var pond = FilePond.create(inputElement,
        {
            acceptedFileTypes: [
                'image/png',
                'image/jpg',
                'image/jpeg',
                files
            ],
            maxFiles: 1,
            maxFileSize: '5MB',
            allowBrowse: true,
            allowMultiple: false,
            instantUpload: instant_Upload,
            'server': {
                url: url_base,
                process: {
                    url: url_process + register_id,
                    method: 'POST',
                },
                revert: {
                    url: url_revert + register_id,
                    method: 'POST',
                }
            }
        }
    );
});
}

module.exports = {
    "adminFilepond": adminFilepond
}

 //carga de documento para Borrador
 $('#policy_documentos_draft_fileField_file').each(function (index, value) {
    var doc_id = $(this).attr('data-doc');
    var select_id = $(this).attr('id');
    var url_base = $("#data-url").val();
    var inputElement = document.querySelector("#policy_documentos_draft_fileField_file");
    var pond = FilePond.create(inputElement,
        {
            acceptedFileTypes: ['application/pdf'],
            maxFiles: 1,
            maxFileSize: '5MB',
            allowBrowse: true,
            allowMultiple: false,
            instantUpload: true,
            'server': {
                url: url_base,
                //process:'/poliza/upload-file',
                process: {
                    url: '/poliza/upload-file-draft/' + doc_id,
                    method: 'POST',
                },
                revert: {
                    url: '/poliza/delete-doc-draft/' + doc_id,
                    method: 'POST',

                }
            }
        }
    );
});

//CARGA DE DOCUMENTOS PASO 5 ,6 Y 7
//carga de archivo para paso 5
$('input.payment-file').each(function (index, value) {
    var doc_id = $(this).attr('data-poliza-id');   //id de la poliza
    var data_doc = $(this).attr('data-doc');   //tipo de documento
    var select_id = $(this).attr('id');
    var inputElement = document.getElementById(select_id);
    var url_base = $("#data-url").val();
    if (data_doc == 'xml') {

        var accept_file = 'text/xml';

    } else if (data_doc == 'pdf') {

        var accept_file = 'application/pdf';

    } else if (data_doc == 'img') {

        var accept_file = 'application/pdf';

    } else if (data_doc == 'poliza-pdf') {

        var accept_file = 'application/pdf';

    } else if (data_doc == 'poliza-xml') {

        var accept_file = 'text/xml';

    }
    var pond = FilePond.create(inputElement,
        {
            acceptedFileTypes: [accept_file],
            maxFiles: 1,
            maxFileSize: '5MB',
            allowBrowse: true,
            allowMultiple: false,
            instantUpload: true,
            'server': {
                url: url_base,
                process: {
                    url: '/poliza/upload-file-pago/' + doc_id + '/' + data_doc,
                    method: 'POST',
                },
                revert: {
                    url: '/poliza/delete-doc-pago/' + doc_id + '/' + data_doc,
                    method: 'POST',
                }
            }
        }
    );
});

//carga de documento para documentacion reglamentaria
$('input.reglamentaria-file').each(function (index, value) {
    var doc_id = $(this).attr('data-id');   //id de la poliza
    var select_id = $(this).attr('id');     //id del select
    var url_base = $("#data-url").val();
    var inputElement = document.querySelector("#" + select_id);
    var pond = FilePond.create(inputElement,
        {
            acceptedFileTypes: [
                'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
                'application/vnd.ms-excel',
                'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
                'application/msword',
                'application/pdf'
            ],
            maxFiles: 1,
            allowBrowse: true,
            allowMultiple: false,
            instantUpload: true,
            maxFileSize: '40MB',
            'server': {
                url: url_base,
                //process:'/poliza/upload-file',
                process: {
                    url: '/poliza/upload-file-docreglamentaria/' + doc_id,
                    method: 'POST',
                },
                revert: {
                    url: '/poliza/delete-doc/' + doc_id,
                    method: 'POST',

                }
            }
        }
    );
});

//carga de archivo para documento fuente
$('#policy_documentos_policyDocumentSource_fileField').each(function (index, value) {
    var doc_id = $(this).attr('data-id');
    var inputElement = document.querySelector('#policy_documentos_policyDocumentSource_fileField');
    var url_base = $("#data-url").val();
    var pond = FilePond.create(inputElement,
        {
            acceptedFileTypes: [
                'application/vnd.openxmlformats-officedocument.wordprocessingml.document', 
                'application/msword', 
                'application/pdf',
                'image/png', 
                'image/jpg', 
                'image/jpeg',
                ],
                
            maxFiles: 1,
            maxFileSize: '40MB',
            allowBrowse: true,
            allowMultiple: false,
            instantUpload: true,
            'server': {
                url: url_base,
                process: {
                    url: '/poliza/upload-file-docfuente/' + doc_id,
                    method: 'POST',
                },
                revert: {
                    url: '/poliza/delete-doc-fuente/' + doc_id,
                    method: 'POST',
                }
            }
        }
    );
});

$('.privacy_file').each(function (index, value) {
    var doc_id = $(this).attr('data-poliza-id');   //id de la poliza
    var select_id = $(this).attr('id');
    var inputElement = document.getElementById(select_id);
    var url_base = $("#data-url").val();
    
    var pond = FilePond.create(inputElement,
        {
            acceptedFileTypes: ['application/pdf'],
            maxFiles: 1,
            maxFileSize: '5MB',
            allowBrowse: true,
            allowMultiple: false,
            instantUpload: true,
            'server': {
                url: url_base,
                process: {
                    url: '/fidelidad/upload-file-fidelidad/' + doc_id,
                    method: 'POST',
                },
                revert: {
                    url: '/fidelidad/delete-doc-fidelidad/' + doc_id,
                    method: 'POST',
                }
            }
        }
    );
});
