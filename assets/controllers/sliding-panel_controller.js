import {Controller} from '@hotwired/stimulus';
import * as Turbo from "@hotwired/turbo";

export default class extends Controller {
    static targets = ['slidingFrame'];

    connect() {
        document.addEventListener('turbo:submit-end', (event) => {
            this.afterFormSubmit(event);
        });
    }

    afterFormSubmit(event) {
        if (event.detail.success) {
            $('.sliding-panel-wrap').removeClass('slide');
        }
    }

    async slideIn(event) {
        $('.sliding-panel-wrap').removeClass('slide');

        let frameSrc = $(event.currentTarget).attr('data-frame-src');
        let frameID = $(event.currentTarget).attr('data-frame-id');

        if (frameSrc) {
            $('#' + frameID).attr('src', frameSrc);
            Turbo.visit(frameSrc, {frame: frameID})
            $('#' + frameID).closest('.sliding-panel-wrap').addClass('slide');
        } else {
            $(this.slidingFrameTarget).addClass('slide');
        }
    }

    async slideOut(event) {
        $(this.slidingFrameTarget).removeClass('slide');
        return false;
    }

}
