import {Controller} from '@hotwired/stimulus';
import {Modal} from 'bootstrap';

export default class extends Controller {
    static targets = ['modal', 'alternativeModal'];
    static values = {
        formUrl: String,
    }
    modal = null;

    async openModal(event) {
        this.modal = new Modal(this.modalTarget);
        this.modal.show();
    }

    async openAlternativeModal(event) {
        this.modal = new Modal(this.alternativeModalTarget);
        this.modal.show();
    }

    closeModal(event) {
        this.modal.hide();
    }
}